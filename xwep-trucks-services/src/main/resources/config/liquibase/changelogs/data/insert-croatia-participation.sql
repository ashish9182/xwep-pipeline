DO $task$
    DECLARE
        insertQry VARCHAR := 'insert into country_participation (code,iso_code,name,country_vo,participation,primary_lang) ' ||
            'VALUES ($1, $2, $3, $4, $5, $6)';
    BEGIN
        EXECUTE insertQry USING 542,'HR','Croatia','EGVO','dealer','en';

END $task$;