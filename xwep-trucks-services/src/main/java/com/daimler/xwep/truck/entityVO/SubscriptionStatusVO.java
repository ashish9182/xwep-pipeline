package com.daimler.xwep.truck.entityVO;

import com.daimler.xwep.truck.entity.EquipmentType;


public class SubscriptionStatusVO {

    private Long id;
	
	private boolean isSubscribed;
	
	private EquipmentType equipmentType;
	
	private String divisionName;

	public SubscriptionStatusVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}

	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}



	public String getDivisionName() {
		return divisionName;
	}



	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	
	

}
