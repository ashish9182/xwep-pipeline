package com.daimler.xwep.truck.dto;

import java.util.List;
import java.util.Map;

public class OfferedServiceRequest {
	
	private List<String> countryIsoCodes;

    private List<String> distributionLevels;

    private List<Map<String, String>> offeredServices;
    
    private Boolean onlyActive;
    
    private Boolean includeApplicants;

    public List<String> getCountryIsoCodes() {
        return countryIsoCodes;
    }

    public void setCountryIsoCodes(List<String> countryIsoCodes) {
        this.countryIsoCodes = countryIsoCodes;
    }

    public List<String> getDistributionLevels() {
        return distributionLevels;
    }

    public void setDistributionLevels(List<String> distributionLevels) {
        this.distributionLevels = distributionLevels;
    }

    public List<Map<String, String>> getOfferedServices() {
        return offeredServices;
    }

    public void setOfferedServices(List<Map<String, String>> offeredServices) {
        this.offeredServices = offeredServices;
    }

    public Boolean isOnlyActive() {
        return onlyActive;
    }

    public void setOnlyActive(Boolean onlyActive) {
        this.onlyActive = onlyActive;
    }

    public Boolean isIncludeApplicants() {
        return includeApplicants;
    }

    public void setIncludeApplicants(Boolean includeApplicants) {
        this.includeApplicants = includeApplicants;
    }
}
