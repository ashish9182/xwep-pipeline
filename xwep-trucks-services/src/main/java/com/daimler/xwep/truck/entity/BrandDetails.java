package com.daimler.xwep.truck.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "brand_details")
public class BrandDetails {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "brand_id")
    private String brandId;
    
    @Column(name = "value")
    private String value; 
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manage_subscription_id")
    private ManageSubscription manageSubscription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ManageSubscription getManageSubscription() {
        return manageSubscription;
    }

    public void setManageSubscription(ManageSubscription manageSubscription) {
        this.manageSubscription = manageSubscription;
    }

}
