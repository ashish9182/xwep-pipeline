package com.daimler.xwep.truck.entityVO;

import java.util.List;

import com.daimler.xwep.truck.entity.SubscriptionStatus;


public class DivisionVO {
	

    private Long id;
	
	private String name;
	
	private String description;
	
	private List<SubscriptionStatusVO> subscriptionStatusVOs;

	public DivisionVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public List<SubscriptionStatusVO> getSubscriptionStatusVOs() {
		return subscriptionStatusVOs;
	}



	public void setSubscriptionStatusVOs(List<SubscriptionStatusVO> subscriptionStatusVOs) {
		this.subscriptionStatusVOs = subscriptionStatusVOs;
	}

	
	

}
