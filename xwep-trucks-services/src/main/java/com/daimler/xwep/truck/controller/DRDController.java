package com.daimler.xwep.truck.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.daimler.xwep.truck.constant.XWEPTruckConfigurationConstants;
import com.daimler.xwep.truck.dto.GenericResponse;
import com.daimler.xwep.truck.service.DRDService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class DRDController {
	
private static final Logger logger = LoggerFactory.getLogger(DRDController.class);
    
    @Value("${name.value}")
    private String name;

    @Autowired
    private DRDService drdService;

    @GetMapping("/getUserInfo")
    public ResponseEntity<GenericResponse> getUserInfo(HttpServletRequest request)
            throws JsonMappingException, JsonProcessingException {
        return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                drdService.getuserInfo(request)), HttpStatus.OK);
    }

}
