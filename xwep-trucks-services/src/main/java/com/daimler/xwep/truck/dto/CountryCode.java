package com.daimler.xwep.truck.dto;

import java.time.ZonedDateTime;

public class CountryCode {
	
	private String countryName;

    private CountryLanguage countryLanguage;

    private ZonedDateTime lastSyncDate;
    
    private ZonedDateTime nextSyncDate;

    public CountryCode() {
        super();
    }

    public CountryLanguage getCountryLanguage() {
        return countryLanguage;
    }

    public void setCountryLanguage(CountryLanguage countryLanguage) {
        this.countryLanguage = countryLanguage;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ZonedDateTime getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(ZonedDateTime lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public ZonedDateTime getNextSyncDate() {
        return nextSyncDate;
    }

    public void setNextSyncDate(ZonedDateTime nextSyncDate) {
        this.nextSyncDate = nextSyncDate;
    }
}
