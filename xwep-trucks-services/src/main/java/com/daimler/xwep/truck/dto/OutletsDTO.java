package com.daimler.xwep.truck.dto;

import java.util.List;

public class OutletsDTO {
	
	private List<Outlet> outlets;

	public List<Outlet> getOutlets() {
		return outlets;
	}

	public void setOutlets(List<Outlet> outlets) {
		this.outlets = outlets;
	}

}
