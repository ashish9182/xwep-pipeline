package com.daimler.xwep.truck.repository;

import com.daimler.xwep.truck.dto.AggregateSubscriptionDTO;


public interface CustomSubscriptionStatusRepository {
    public AggregateSubscriptionDTO aggregatedSubscriptionDetails(String nationality);
}
