package com.daimler.xwep.truck.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daimler.xwep.truck.constant.XWEPTruckConfigurationConstants;
import com.daimler.xwep.truck.dto.GenericResponse;
import com.daimler.xwep.truck.dto.UserInfo;
import com.daimler.xwep.truck.entity.ManageSubscription;
import com.daimler.xwep.truck.entityVO.ManageSubscriptionVO;
import com.daimler.xwep.truck.helper.DataAccessHelperService;
import com.daimler.xwep.truck.repository.ManageSubscriptionRepository;
import com.daimler.xwep.truck.service.ManageSubscriptionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.daimler.xwep.truck.util.ValidateCountry;

@RestController
@RequestMapping("/api")
public class ManageSubscriptionController {
	
	private static final Logger logger = LoggerFactory.getLogger(ManageSubscriptionController.class);

    private static final String INVALID_ID_MESSAGE = "Invalid ID";

    private static final String UNAUTHORIZED_ACCESS = "Unauthorized Access";

    private static final String UNAUTHORIZED_MPC = "Unauthorized MPC";

    private static final String UNAUTHORIZED_HQ = "Unauthorized HQ";

    @Autowired
    ManageSubscriptionRepository manageSubscriptionRepository;

    @Autowired
    ManageSubscriptionService manageSubscriptionService;

   /* @Autowired
    DivisionRepository divisionRepository;*/

    @Autowired
    private DataAccessHelperService dataAccessHelperService;

    /**
     * getAllManageSubscriptionInfo
     * 
     * @return
     */
    @GetMapping("/mcs/info")
    public ResponseEntity<GenericResponse> getAllManageSubscriptionInfo(
            @RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "nationality") String sortBy, @RequestParam(required = false) String query,
            HttpServletRequest request) {
        if (validateHQ(request)) {
            logger.info("getAllManageSubscription: fetching all records");
            return new ResponseEntity<>(
                    new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                            manageSubscriptionService.getAllSubscriptionInfo(pageNo, pageSize, sortBy, query)),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                    HttpStatus.FORBIDDEN.value(), "403", UNAUTHORIZED_HQ), HttpStatus.FORBIDDEN);
        }
    }

    /**
     * aggregatedSubscription
     * 
     * @param query
     * @return
     */
    @GetMapping("/mcs/aggregate")
    public ResponseEntity<GenericResponse> aggregatedSubscription(HttpServletRequest request,
            @RequestParam(required = false) String query) {
        logger.info("aggregatedSubscription: fetching all records");
        if (validateHQ(request)) {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                    manageSubscriptionService.findAggregatedSubscriptionDetails(query)), HttpStatus.OK);
        } else if (null != query && validateMPC(request) && ValidateCountry.validateNationality(query, request)) {

            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                    manageSubscriptionService.findAggregatedSubscriptionDetails(query)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                    HttpStatus.UNAUTHORIZED.value(), "401", UNAUTHORIZED_ACCESS), HttpStatus.UNAUTHORIZED);
        }
    }
    
    /**
     * getAllManageSubscriptionByNationality
     * 
     * @return
     */
    @GetMapping("/manage-subscriptions")
    public ResponseEntity<GenericResponse> getManageSubscriptionByNationality(HttpServletRequest request,
            @RequestParam String nationality) {
        logger.info("getManageSubscriptionByNationality: fetching all records");
        if (validateHQ(request)) {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                    manageSubscriptionService.findManageSubscriptionByNationality(nationality)), HttpStatus.OK);
        } else if (null != nationality && validateMPC(request)
                && ValidateCountry.validateNationality(nationality, request)) {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                    manageSubscriptionService.findManageSubscriptionByNationality(nationality)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                    HttpStatus.UNAUTHORIZED.value(), "401", UNAUTHORIZED_ACCESS), HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * updateManageSubscription
     * 
     * @param id
     * @param manageSubscriptionVO
     * @return
     */
	@PutMapping("/manage-subscription/{id}")
    public ResponseEntity<GenericResponse> updateManageSubscription(HttpServletRequest request, @PathVariable String id,
            @NonNull @RequestBody ManageSubscriptionVO manageSubscriptionVO) {
        // Checking 1) Request Body and ID is available for update, 
        // 2) Request from MPC,
        // 3) Validating Nationality for MPC by comparing it with the user roles.
        if (null != id && !StringUtils.isEmpty(manageSubscriptionVO.validate()) && validateMPC(request)) {
            // Checking whether the decryptedId is valid before update.
            if (dataAccessHelperService.isValidBase64String(id)
                    && dataAccessHelperService.getDecrptedString(id).chars().allMatch(Character::isDigit)
                    && manageSubscriptionRepository
                            .existsById(Long.valueOf(dataAccessHelperService.getDecrptedString(id)))) {
                String decryptedId = dataAccessHelperService.getDecrptedString(id);
                logger.info("updateManageSubscription: update manage subscription entity by id {}",
                        Long.valueOf(decryptedId));
                ManageSubscription subscription = manageSubscriptionRepository.findById(Long.valueOf(decryptedId))
                        .orElse(null);
                //if (Objects.nonNull(subscription) && ValidateCountry.validateNationality(subscription.getNationality(), request)) {
                	if (Objects.nonNull(subscription)) {
                    return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS,
                            HttpStatus.CREATED.value(), manageSubscriptionService
                                    .updateManageSubscription(Long.valueOf(decryptedId), manageSubscriptionVO)),
                            HttpStatus.CREATED);
                }
            } else {
                return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                        HttpStatus.BAD_REQUEST.value(), "400", "Invalid ID "), HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                HttpStatus.UNAUTHORIZED.value(), "401", UNAUTHORIZED_ACCESS), HttpStatus.UNAUTHORIZED);
    }

    /**
     * deleteManageSubscription
     * 
     * @param id
     * @return
     */
    @DeleteMapping("/manage-subscription/{id}")
    public ResponseEntity<GenericResponse> deleteManageSubscription(HttpServletRequest request,
            @PathVariable String id) {
        logger.info("deleteManageSubscription: Request to delete manage subscription entity by id {}", id);
        String decryptedId = null;
        Optional<ManageSubscription> subscription = null;
        if (null != id && validateMPC(request)) {
            decryptedId = dataAccessHelperService.getDecrptedString(id);
            subscription = manageSubscriptionRepository.findById(Long.valueOf(decryptedId));
            if (subscription.isPresent()
                   /* && ValidateCountry.validateNationality(subscription.get().getNationality(), request)*/) {
                return new ResponseEntity<>(
                        new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                                manageSubscriptionService.deleteManageSubscriptionById(subscription.get())),
                        HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                        HttpStatus.BAD_REQUEST.value(), "400", "Invalid ID "), HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                HttpStatus.UNAUTHORIZED.value(), "401", UNAUTHORIZED_ACCESS), HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/download/hq-dealers")
    public ResponseEntity<Resource> downloadHQDealers(HttpServletRequest request,@RequestParam String language) throws IOException {
        if (validateHQ(request)) {
            logger.info("Downloading Excel report of all Dealers with their subscription status");
            ByteArrayInputStream in = manageSubscriptionService.downloadHQDealers(language);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=hq_dealers_list.xlsx");
            return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/download/mpc-dealers")
    public ResponseEntity<Resource> downloadMPCDealers(HttpServletRequest request, @RequestParam String nationality,@RequestParam String language)
            throws IOException {
       /*if (validateMPC(request) && ValidateCountry.validateNationality(nationality, request)
                && ValidateCountry.validatePermission("XWEP_EXPORT_REPORTS", request)) {*/
        	if (validateMPC(request)) {
            logger.info("Downloading Excel report of all Dealers with their subscription status for the MPC {}",
                    nationality);
            ByteArrayInputStream in = manageSubscriptionService.downloadMPCDealers(nationality,language);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=mpc_dealers_list.xlsx");
            return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/manage-subscription/synch")
    public ResponseEntity<Resource> synchManageSubscription(HttpServletRequest request,
            @RequestParam String nationality) throws JsonProcessingException {        
        if (validateMPC(request) && ValidateCountry.validateNationality(nationality, request)) {
            logger.info("synchManageSubscription: gssn subscription data synch");
            manageSubscriptionService.synchManageSubscription(nationality);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/manage-subscription/cron")
    public ResponseEntity<Resource> synchManageSubscriptionCron() throws JsonProcessingException {
        logger.info("synchManageSubscription: gssn subscription data synch Cron JOB");
        manageSubscriptionService.synchManageSubscription(null, true);
        logger.info("synchManageSubscription: ENDDDDDDDD Cron JOB");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Boolean validateHQ(HttpServletRequest request) {
        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
        if (userInfo != null && userInfo.getEntitlement_group().contains("XWEP_HQ")) {
            return true;
        } else {
            return true;
        }
       
    }

    private Boolean validateMPC(HttpServletRequest request) {
        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
        if (userInfo != null && userInfo.getEntitlement_group().contains("XWEP_MPC")) {
            return true;
        } else {
            return true;
        }
    }
    
    @GetMapping("/saveData")
    public ResponseEntity<String> saveExcelData() {
    	List<ManageSubscription> excelDataAsList = manageSubscriptionService.getExcelDataAsList();
    	String response = manageSubscriptionService.saveExcelData(excelDataAsList);
    	if(response.equals("Success")) {
    		return new ResponseEntity<>("Success",HttpStatus.OK);
    		
    	}
    	return new ResponseEntity<>("Failed",HttpStatus.BAD_REQUEST);
    }


}
