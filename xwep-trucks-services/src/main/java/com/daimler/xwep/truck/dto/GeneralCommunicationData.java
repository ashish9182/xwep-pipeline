package com.daimler.xwep.truck.dto;

public class GeneralCommunicationData {
	
	private CommunicationData communicationData;

	public CommunicationData getCommunicationData() {
		return communicationData;
	}

	public void setCommunicationData(CommunicationData communicationData) {
		this.communicationData = communicationData;
	}
}
