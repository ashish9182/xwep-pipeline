package com.daimler.xwep.truck.dto;

public class CountryLanguage {
	
	private String primaryLanguage;
	
	private String secondaryLanguage;

	private String tertiaryLanguage;

	public CountryLanguage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPrimaryLanguage() {
		return primaryLanguage;
	}

	public void setPrimaryLanguage(String primaryLanguage) {
		this.primaryLanguage = primaryLanguage;
	}

	public String getSecondaryLanguage() {
		return secondaryLanguage;
	}

	public void setSecondaryLanguage(String secondaryLanguage) {
		this.secondaryLanguage = secondaryLanguage;
	}

	public String getTertiaryLanguage() {
		return tertiaryLanguage;
	}

	public void setTertiaryLanguage(String tertiaryLanguage) {
		this.tertiaryLanguage = tertiaryLanguage;
	}

}
