package com.daimler.xwep.truck.helper;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DataAccessHelperService {
	
	private static final Logger logger = LoggerFactory.getLogger(DataAccessHelperService.class);

    public String getEncrptedString(String inputString) {
        String encryptedString = null;
        if (null != inputString) {
            encryptedString = Base64.getUrlEncoder().encodeToString(inputString.getBytes());
        }
        return encryptedString;
    }

    public String getDecrptedString(String encryptedString) {
        String decryptedString = null;
        if (null != encryptedString) {
            byte[] decodedBytes = Base64.getUrlDecoder().decode(encryptedString);
            decryptedString = new String(decodedBytes);
        }
        return decryptedString;
    }

    public boolean isValidBase64String(String encryptedString) {
        String pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$";
        logger.info("******Valid Base64 Encoded String : " + encryptedString.matches(pattern));
        return encryptedString.matches(pattern);
    }

}
