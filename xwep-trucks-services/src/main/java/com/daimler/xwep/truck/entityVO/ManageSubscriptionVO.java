package com.daimler.xwep.truck.entityVO;

import java.util.List;

public class ManageSubscriptionVO {

    private Long id;

    private String uuid;

    private String nationality;

    private String outletId;

    private String companyId;

    private String legalName;

    private String brandCode;

    private String road;

    private String postCode;

    private String place;

    private String email;

    /*private String emailPassengerCars;

    private String emailTransporter;*/

    private String emailLKW;

   // private String emailSmartPassengerCars;

    private List<SubscriptionStatusVO> subscriptionStatus;
    
 /*   private boolean eqEnabled;
    
    private boolean carEnabled;
    
    private boolean truckEnabled;
    
    private boolean vanEnabled;*/

    private String status;

    private boolean userUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public List<SubscriptionStatusVO> getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(List<SubscriptionStatusVO> subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*public String getEmailPassengerCars() {
        return emailPassengerCars;
    }

    public void setEmailPassengerCars(String emailPassengerCars) {
        this.emailPassengerCars = emailPassengerCars;
    }

    public String getEmailTransporter() {
        return emailTransporter;
    }

    public void setEmailTransporter(String emailTransporter) {
        this.emailTransporter = emailTransporter;
    }*/

    public String getEmailLKW() {
        return emailLKW;
    }

    public void setEmailLKW(String emailLKW) {
        this.emailLKW = emailLKW;
    }

   /* public String getEmailSmartPassengerCars() {
        return emailSmartPassengerCars;
    }

    public void setEmailSmartPassengerCars(String emailSmartPassengerCars) {
        this.emailSmartPassengerCars = emailSmartPassengerCars;
    }*/

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isUserUpdated() {
        return userUpdated;
    }

    public void setUserUpdated(boolean userUpdated) {
        this.userUpdated = userUpdated;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

   /* public boolean isEqEnabled() {
        return eqEnabled;
    }

    public void setEqEnabled(boolean eqEnabled) {
        this.eqEnabled = eqEnabled;
    }

    public boolean isCarEnabled() {
        return carEnabled;
    }

    public void setCarEnabled(boolean carEnabled) {
        this.carEnabled = carEnabled;
    }

    public boolean isTruckEnabled() {
        return truckEnabled;
    }

    public void setTruckEnabled(boolean truckEnabled) {
        this.truckEnabled = truckEnabled;
    }

    public boolean isVanEnabled() {
        return vanEnabled;
    }

    public void setVanEnabled(boolean vanEnabled) {
        this.vanEnabled = vanEnabled;
    }*/
    
    public String validate() {
        return (subscriptionStatus != null ? "subscriptionStatus=" + subscriptionStatus + ", " : "");
    }
  
}
