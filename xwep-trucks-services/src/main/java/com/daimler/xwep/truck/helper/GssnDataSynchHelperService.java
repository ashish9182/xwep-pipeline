package com.daimler.xwep.truck.helper;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.daimler.xwep.truck.constant.OutletStatus;
import com.daimler.xwep.truck.dto.BrandCodeDTO;
import com.daimler.xwep.truck.dto.OfferedServiceRequest;
import com.daimler.xwep.truck.dto.Outlet;
import com.daimler.xwep.truck.dto.SubscriptionDTO;
import com.daimler.xwep.truck.entity.BrandDetails;
import com.daimler.xwep.truck.entity.EquipmentType;
import com.daimler.xwep.truck.entity.ManageSubscription;
import com.daimler.xwep.truck.entity.SubscriptionStatus;
import com.daimler.xwep.truck.repository.BrandDetailsRepository;
import com.daimler.xwep.truck.repository.SubscriptionStatusRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GssnDataSynchHelperService {
	private static final Logger logger = LoggerFactory.getLogger(GssnDataSynchHelperService.class);

    @Value("${gssn.count}")
    private Integer count;

    @Value("${gssn.url}")
    private String gssn_url;

    //@Value("${gssn.comnUrl}")
    private String comm_url;

    //@Value("${gssn.brandsUrl}")
    private String brands_url;

    @Value("${gssn.proxy}")
    private String gssnProxy;

    @Value("${gssn.port}")
    private int gssnPort;

    @Value("${gssn.apiKey}")
    private String apiKey;

    @Autowired
    BrandDetailsRepository brandDetailsRepository;

    private static List<String> gssOutletIds = new ArrayList<>();

	@Autowired
    SubscriptionStatusRepository subscriptionStatusRepository;

    private RestTemplate restTemplate;

    @SuppressWarnings("serial")
    public List<ManageSubscription> gssnExternalAPICall(List<ManageSubscription> savedSubscriptions, String nationality)
            throws JsonProcessingException {
        SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
        ;

        if (null == restTemplate) {
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                    createHttpClient());
            requestFactory.setConnectTimeout(60 * 1000);
            requestFactory.setReadTimeout(60 * 1000);
            restTemplate = new RestTemplate(requestFactory);
        }
        List<ManageSubscription> newsubscriptions = new ArrayList<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("x-api-key", apiKey);
        logger.info("gssn_url + count" + gssn_url + count);

        // Search API Call
       /* OfferedServiceRequest requestForGssnWholeData = new OfferedServiceRequest();
        List<String> countryIsoCodes = new ArrayList<String>();
        countryIsoCodes.add(nationality);
        requestForGssnWholeData.setCountryIsoCodes(countryIsoCodes);
        requestForGssnWholeData.setIncludeApplicants(true);

        HttpEntity<OfferedServiceRequest> request = new HttpEntity<>(requestForGssnWholeData, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(gssn_url + count, request, String.class);

        OfferedServiceRequest offeredServiceRequest = new OfferedServiceRequest();
        offeredServiceRequest.setCountryIsoCodes(countryIsoCodes);
        List<String> distributionLevels = new ArrayList<String>();
        distributionLevels.add("RETAILER");
        offeredServiceRequest.setDistributionLevels(distributionLevels);
        HashMap<String, String> serviceListMap = new HashMap<String, String>();
        serviceListMap.put("service", "Electric Vehicle After Sales");
        List<Map<String, String>> offeredServices = new ArrayList<Map<String, String>>();
        offeredServices.add(serviceListMap);
        offeredServiceRequest.setOfferedServices(offeredServices);
        offeredServiceRequest.setOnlyActive(true);
        HttpEntity<OfferedServiceRequest> eqRequest = new HttpEntity<OfferedServiceRequest>(offeredServiceRequest,
                headers);
        ResponseEntity<String> eqResponse = restTemplate.postForEntity(gssn_url + count, eqRequest, String.class);

        SubscriptionDTO eqSubscriptions = new SubscriptionDTO();
        if (eqResponse.getStatusCode() == HttpStatus.OK) {
            String result = eqResponse.getBody();
            JSONObject responseJson = new JSONObject(result.toString());
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            eqSubscriptions = mapper.readValue(responseJson.toString(), SubscriptionDTO.class);
            if (eqSubscriptions.get_embedded() != null)
                logger.info("data of eqsubs - " + eqSubscriptions.get_embedded().getOutlets().size()
                        + " in request for eqsubs" + nationality);
        } else {
            logger.info("eq search API fail");
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            logger.info("API success for " + nationality);
            String result = response.getBody();
            JSONObject responseJson = new JSONObject(result.toString());
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            subscriptionDTO = mapper.readValue(responseJson.toString(), SubscriptionDTO.class);
            if (subscriptionDTO.get_embedded() != null)
                logger.info("data of " + subscriptionDTO.get_embedded().getOutlets().size() + " in request for"
                        + nationality);
        } else {
            logger.info("normal search API fail2");
        }

        // Start of Search API call based on ProductGroup (VAN, Truck and
        // Passenger Car)
        // API Call for Passenger Car
       /* OfferedServiceRequest req = this.buildRequestForSearchAPI(nationality, "Passenger Car");
        HttpEntity<OfferedServiceRequest> prodGroupRequest = new HttpEntity<OfferedServiceRequest>(req, headers);
        ResponseEntity<String> prodGroupResponse = restTemplate.postForEntity(gssn_url + count, prodGroupRequest,
                String.class);
        SubscriptionDTO groupSubscriptions = new SubscriptionDTO();
        if (prodGroupResponse.getStatusCode() == HttpStatus.OK) {
            String result = prodGroupResponse.getBody();
            JSONObject responseJson = new JSONObject(result.toString());
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            groupSubscriptions = mapper.readValue(responseJson.toString(), SubscriptionDTO.class);
            if (groupSubscriptions.get_embedded() != null)
                logger.info("data of CAR - " + groupSubscriptions.get_embedded().getOutlets().size()
                        + " in request for eqsubs" + nationality);
        } else {
            logger.info("Search API failed for ProductGrup Passenger Car");
        }*/

        // API Call for Vans
       /* OfferedServiceRequest van_req = this.buildRequestForSearchAPI(nationality, "Vans");
        HttpEntity<OfferedServiceRequest> prodGroupRequest_van = new HttpEntity<OfferedServiceRequest>(van_req,
                headers);
        ResponseEntity<String> prodGroupResponse_van = restTemplate.postForEntity(gssn_url + count,
                prodGroupRequest_van, String.class);
        SubscriptionDTO groupSubscriptions_van = new SubscriptionDTO();
        if (prodGroupResponse_van.getStatusCode() == HttpStatus.OK) {
            String result = prodGroupResponse_van.getBody();
            JSONObject responseJson = new JSONObject(result.toString());
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            groupSubscriptions_van = mapper.readValue(responseJson.toString(), SubscriptionDTO.class);
            if (groupSubscriptions_van.get_embedded() != null)
                logger.info("data of VAN - " + groupSubscriptions_van.get_embedded().getOutlets().size()
                        + " in request for eqsubs" + nationality);
        } else {
            logger.info("Search API failed for ProductGrup Vans");
        }*/

        // API Call for Trucks
        OfferedServiceRequest req_trucks = this.buildRequestForSearchAPI(nationality, "Trucks");
        HttpEntity<OfferedServiceRequest> prodGroupRequest_trucks = new HttpEntity<OfferedServiceRequest>(req_trucks,
                headers);
        ResponseEntity<String> prodGroupResponse_trucks = restTemplate.postForEntity(gssn_url + count,
                prodGroupRequest_trucks, String.class);
        SubscriptionDTO groupSubscriptions_trucks = new SubscriptionDTO();
        if (prodGroupResponse_trucks.getStatusCode() == HttpStatus.OK) {
            String result = prodGroupResponse_trucks.getBody();
            JSONObject responseJson = new JSONObject(result.toString());
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            groupSubscriptions_trucks = mapper.readValue(responseJson.toString(), SubscriptionDTO.class);
            if (groupSubscriptions_trucks.get_embedded() != null)
                logger.info("data of Trucks - " + groupSubscriptions_trucks.get_embedded().getOutlets().size()
                        + " in request for eqsubs" + nationality);
        } else {
            logger.info("Search API failed for ProductGrup Trucks");
        }

        if (subscriptionDTO != null )
            newsubscriptions = geSubscriptions(savedSubscriptions, nationality, groupSubscriptions_trucks);

        return newsubscriptions;
    }

    /**
     * @param nationality
     * @param productGroup
     */
    @SuppressWarnings("serial")
    private OfferedServiceRequest buildRequestForSearchAPI(String nationality, String productGroup) {
        OfferedServiceRequest requestBasedOnProductGroup = new OfferedServiceRequest();
        List<String> countryIsoCodes = new ArrayList<String>();
        countryIsoCodes.add(nationality);
        requestBasedOnProductGroup.setCountryIsoCodes(countryIsoCodes);
        HashMap<String, String> serviceMap = new HashMap<String, String>();
        serviceMap.put("brandId", "MB");
        serviceMap.put("productGroup", productGroup);
        serviceMap.put("service", "Repair & Maintenance");
        List<Map<String, String>> offeredServices = new ArrayList<Map<String, String>>();
        offeredServices.add(serviceMap);
        requestBasedOnProductGroup.setOfferedServices(offeredServices);
        requestBasedOnProductGroup.setOnlyActive(true);
        requestBasedOnProductGroup.setIncludeApplicants(true);
        return requestBasedOnProductGroup;
    }

    private List<ManageSubscription> geSubscriptions(List<ManageSubscription> subscriptions,
            String nationality,SubscriptionDTO groupSubscriptions_trucks) {
       /* List<Outlet> outlets = new ArrayList<Outlet>();
        if (newOutlets.get_embedded() != null)
            outlets = newOutlets.get_embedded().getOutlets();*/

        List<ManageSubscription> finalSubscription = null;
        Map<String, ManageSubscription> subscriptionsMap = subscriptions.stream()
                .collect(Collectors.toMap(ManageSubscription::getOutletId, Function.identity()));

        // EQ Outlets
        /*List<Outlet> eq_outlets = new ArrayList<Outlet>();
        if (eq_subscriptions.get_embedded() != null)
            eq_outlets = eq_subscriptions.get_embedded().getOutlets();
        Map<String, Outlet> eq_outletsMap = eq_outlets.stream()
                .collect(Collectors.toMap(Outlet::getOutletId, Function.identity()));*/

        // Outlets CAR
       /* List<Outlet> car_outlets = new ArrayList<Outlet>();
        if (groupSubscriptions_car.get_embedded() != null)
            car_outlets = groupSubscriptions_car.get_embedded().getOutlets();
        Map<String, Outlet> groupSubscriptionsForCar = car_outlets.stream()
                .collect(Collectors.toMap(Outlet::getOutletId, Function.identity()));*/

        // Outlets Vans
        /*List<Outlet> van_outlets = new ArrayList<Outlet>();
        if (groupSubscriptions_van.get_embedded() != null)
            van_outlets = groupSubscriptions_van.get_embedded().getOutlets();
        Map<String, Outlet> groupSubscriptionsForVan = van_outlets.stream()
                .collect(Collectors.toMap(Outlet::getOutletId, Function.identity()));*/

        // Outlets Trucks
        List<Outlet> trucks_outlets = new ArrayList<Outlet>();
        if (groupSubscriptions_trucks.get_embedded() != null)
            trucks_outlets = groupSubscriptions_trucks.get_embedded().getOutlets();
        /*Map<String, Outlet> groupSubscriptionsForTrucks = trucks_outlets.stream()
                .collect(Collectors.toMap(Outlet::getOutletId, Function.identity()));*/

        List<ManageSubscription> newSubscriptions = Optional.ofNullable(trucks_outlets)
                .orElse(Collections.emptyList())
                .stream()
                .map(trucks_outlet -> buildSubscriptions(subscriptionsMap, trucks_outlet, nationality))
                .collect(Collectors.toList());

        List<ManageSubscription> inActiveSubscriptions = getInactiveSubscriptions(subscriptionsMap);

        if (CollectionUtils.isNotEmpty(newSubscriptions) && CollectionUtils.isNotEmpty(inActiveSubscriptions)) {
            finalSubscription = Stream.concat(newSubscriptions.stream(), inActiveSubscriptions.stream())
                    .collect(Collectors.toList());
        } else if (CollectionUtils.isNotEmpty(newSubscriptions)) {
            finalSubscription = newSubscriptions;
        } else if (CollectionUtils.isNotEmpty(inActiveSubscriptions)) {
            finalSubscription = inActiveSubscriptions;
        }
        return finalSubscription;
    }

    private ManageSubscription buildSubscriptions(Map<String, ManageSubscription> subscriptionsMap, Outlet outlet,
            String nationality) {
        ManageSubscription subscription = subscriptionsMap.get(outlet.getOutletId());
        gssOutletIds.add(outlet.getOutletId());
        boolean isNewOutlet = false;
        if (null == subscription) {
            subscription = new ManageSubscription();
            subscription.setStatus(OutletStatus.NEW.toString());
            isNewOutlet = true;
        } else {
            if (subscription.isUserUpdated()) {
                subscription.setStatus(OutletStatus.OLD.toString());
            } else {
                subscription.setStatus(OutletStatus.NEW.toString());
            }
        }
        subscription.setOutletId(outlet.getOutletId());
        subscription.setCompanyId(outlet.getCompanyId());
        subscription.setNationality(nationality);
        subscription.setLegalName(outlet.getLegalName());
        subscription.setGssnClassicId(outlet.getGssnClassicId());
        if (null != outlet.getAddress()) {
            subscription.setPostCode(outlet.getAddress().getZipCode());
            subscription.setPlace(outlet.getAddress().getCity());
            if (null != outlet.getAddress().getStreetNumber()) {
                subscription.setRoad(outlet.getAddress().getStreet() + " " + outlet.getAddress().getStreetNumber());
            } else {
                subscription.setRoad(outlet.getAddress().getStreet());
            }
        }
        subscription.setCreatedDate(ZonedDateTime.now());

        // Check whether the Outlet is present in the EQ Subscription list
        boolean isEqDealer = false;
       /* Outlet obj_eq = eqOutletsMap.get(outlet.getOutletId());
        if (null != obj_eq) {
            isEqDealer = true;
            subscription.setEqService(true);
        } else {
            subscription.setEqService(false);
        }
        // Check whether the Outlet is present in the CAR Subscription list
        Outlet obj_car = groupSubscriptionsForCar.get(outlet.getOutletId());
        if (null != obj_car) {
            subscription.setCarService(true);
        } else {
            subscription.setCarService(false);
        }
        // Check whether the Outlet is present in the VAN Subscription list
        Outlet obj_van = groupSubscriptionsForVan.get(outlet.getOutletId());
        if (null != obj_van) {
            subscription.setVanService(true);
        } else {
            subscription.setVanService(false);
        }*/
        // Check whether the Outlet is present in the TRUCK Subscription list
       // Outlet obj_truck = groupSubscriptionsForTrucks.get(outlet.getOutletId());
      /*  if (null != obj_truck) {
            subscription.setTruckService(true);
        } else {
            subscription.setTruckService(false);
        }*/

        List<SubscriptionStatus> subscriptionStatusList = buildSubscriptionStatus(subscription, isNewOutlet);
        if (null != subscriptionStatusList) {
            subscription.setSubscriptionStatus(subscriptionStatusList);
        }

        List<BrandCodeDTO> brandCodes = outlet.getBrandCodes();
        if (null != brandCodes && !brandCodes.isEmpty()) {
            List<BrandDetails> brandDetails = new ArrayList<BrandDetails>();
            List<BrandDetails> savedBrandDetails = subscription.getBrandDetails();
            Map<String, Long> savedBrandDetailsMap = new HashMap<String, Long>();
            if (null != savedBrandDetails && !savedBrandDetails.isEmpty()) {
                for (BrandDetails brandCode : savedBrandDetails) {
                    savedBrandDetailsMap.put(brandCode.getBrandId(), brandCode.getId());
                }
            }
            for (BrandCodeDTO brandCodeDTO : brandCodes) {
                if (null != brandCodeDTO.getBrand() && null != brandCodeDTO.getBrandCode()) {
                    BrandDetails brandDetail = new BrandDetails();
                    if (null != savedBrandDetailsMap.get(brandCodeDTO.getBrand())) {
                        brandDetail.setId(savedBrandDetailsMap.get(brandCodeDTO.getBrand()));
                    }
                    brandDetail.setBrandId(brandCodeDTO.getBrand());
                    brandDetail.setManageSubscription(subscription);
                    brandDetail.setValue(brandCodeDTO.getBrandCode());
                    brandDetails.add(brandDetail);
                    if (brandCodeDTO.getBrand().equals("MB")) {
                        subscription.setBrandCode(brandCodeDTO.getBrandCode());
                    }
                }
                subscription.setBrandDetails(brandDetails);
            }
        }
        return subscription;
    }

    private List<SubscriptionStatus> buildSubscriptionStatus(ManageSubscription subscription, boolean isNewOutlet) {
        if (subscription.getStatus().equals(OutletStatus.NEW.toString()) && isNewOutlet) {
            List<SubscriptionStatus> subscriptionStatusList = new ArrayList<>();
            //List<Division> divisions = divisionRepository.findAll();
           // for (Division division : divisions) {
                SubscriptionStatus subscriptionStatus = new SubscriptionStatus();
                subscriptionStatus.setSubscribed(false);
                subscriptionStatus.setEquipmentType(EquipmentType.NA);
                //subscriptionStatus.setDivision("TRUCK");
                subscriptionStatus.setCreatedDate(ZonedDateTime.now());
                subscriptionStatus.setManageSubscription(subscription);
                subscriptionStatusList.add(subscriptionStatus);
            //}
            return subscriptionStatusList;
        } /*else {
            if (is_eqDealer) {
                List<SubscriptionStatus> oldStatusList = subscriptionStatusRepository
                        .findByManageSubscriptionId(subscription.getId());
                for (SubscriptionStatus status : oldStatusList) {
                    //Division division = status.getDivision();
                    if (null != division && division.getName().equals("EQ")) {
                        for (SubscriptionStatus subscriptionStatus : oldStatusList) {
                            Division div = subscriptionStatus.getDivision();
                            if (null != div && div.getName().equals("PASSENGER")) {
                                status.setSubscribed(subscriptionStatus.isSubscribed());
                                if (subscriptionStatus.getEquipmentType()
                                        .toString()
                                        .equals(String.valueOf(EquipmentType.FULL_EQUIPMENT))
                                        || subscriptionStatus.getEquipmentType()
                                                .toString()
                                                .equals(String.valueOf(EquipmentType.PARTIAL_EQUIPMENT))) {
                                    status.setEquipmentType(EquipmentType.FULL_EQUIPMENT);
                                } else {
                                    status.setEquipmentType(subscriptionStatus.getEquipmentType());

                                }
                            }
                        }
                    }
                }
                return oldStatusList;
            }
        }*/
        return null;
    }

    private List<ManageSubscription> getInactiveSubscriptions(Map<String, ManageSubscription> outletsInDBMap) {
        Set<String> outletIds = outletsInDBMap.keySet();
        List<ManageSubscription> inActiveSubscriptions = new ArrayList<ManageSubscription>();
        for (String outletId : outletIds) {
            if (!gssOutletIds.contains(outletId)) {
                ManageSubscription subscription = outletsInDBMap.get(outletId);
                subscription.setStatus(OutletStatus.INACTIVE.toString());
                inActiveSubscriptions.add(subscription);
            }
        }
        return inActiveSubscriptions;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @SuppressWarnings("unused")
    private HttpClient createHttpClient() {
        HttpHost myProxy = null;
        myProxy = new HttpHost(gssnProxy, gssnPort);
        return HttpClients.custom().setProxy(myProxy).build();
    }

}

