package com.daimler.xwep.truck.dto;

import java.util.List;

public class UserInfo {
	
	private Long exp;

    private String sub;

    private String name;

    private String email;

    private String entitlement_group;

    private String authorization_group;

    private String scoped_entitlement;

    private List<CountryCode> countryCodes;
    
    private String outletId;

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEntitlement_group() {
        return entitlement_group;
    }

    public void setEntitlement_group(String entitlement_group) {
        this.entitlement_group = entitlement_group;
    }

    public String getAuthorization_group() {
        return authorization_group;
    }

    public void setAuthorization_group(String authorization_group) {
        this.authorization_group = authorization_group;
    }

    public String getScoped_entitlement() {
        return scoped_entitlement;
    }

    public void setScoped_entitlement(String scoped_entitlement) {
        this.scoped_entitlement = scoped_entitlement;
    }

    public List<CountryCode> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(List<CountryCode> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }


}
