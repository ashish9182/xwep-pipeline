package com.daimler.xwep.truck.constant;

public enum OutletStatus {
	INACTIVE, NEW, OLD
}
