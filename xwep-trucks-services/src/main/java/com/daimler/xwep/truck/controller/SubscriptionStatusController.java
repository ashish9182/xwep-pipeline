package com.daimler.xwep.truck.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daimler.xwep.truck.constant.XWEPTruckConfigurationConstants;
import com.daimler.xwep.truck.dto.GenericResponse;
import com.daimler.xwep.truck.dto.UserInfo;
import com.daimler.xwep.truck.service.ManageSubscriptionService;
import com.daimler.xwep.truck.entityVO.DealerInfoWithSubscriptionStatusVO;

@RestController
@RequestMapping("/api")
public class SubscriptionStatusController {

    @Autowired
    ManageSubscriptionService manageSubscriptionService;

    /**
     * getSubscriptionStatusByDealer
     *
     * @return
     */
    @GetMapping("/manage-subscription/subscription-status")
    public ResponseEntity<GenericResponse> getSubscriptionStatusByDealer(HttpServletRequest request) {
        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
        DealerInfoWithSubscriptionStatusVO dealerInfoWithSubscriptionStatusVO = manageSubscriptionService
                .getDealerInfo("GS0052453");
        if (dealerInfoWithSubscriptionStatusVO != null) {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.SUCCESS, HttpStatus.OK.value(),
                    dealerInfoWithSubscriptionStatusVO), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                    HttpStatus.NOT_FOUND.value(), "404", "Dealer does not have an outlet"), HttpStatus.NOT_FOUND);
        }
    }
}
