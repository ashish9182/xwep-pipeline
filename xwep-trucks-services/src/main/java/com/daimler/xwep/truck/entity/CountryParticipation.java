package com.daimler.xwep.truck.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;


/**
 */
@Entity
@Table(name = "country_participation")
public class CountryParticipation implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NonNull
    @Id
    @Column(name = "code", nullable = false)
    private Integer code;
    
    @NonNull
    @Column(name = "iso_code", nullable = false)
    private String isoCode;

    @NonNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "country_vo")
    private String countryVO;
    
    @Column(name = "participation")
    private String participation;
    
    @Column(name = "primary_lang")
    private String primaryLanguage;
    
    @Column(name = "secondary_lang")
    private String secondaryLanguage;
    
    @Column(name = "tertiary_lang")
    private String tertiaryLanguage;
    
    @Column(name = "last_sync_date")
    private ZonedDateTime lastSyncDate;
    
    @Column(name = "is_synch_active")
    private boolean activeSynch;
    
    @Column(name = "next_sync_date")
    private ZonedDateTime nextSyncDate;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryVO() {
        return countryVO;
    }

    public void setCountryVO(String countryVO) {
        this.countryVO = countryVO;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public String getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public void setSecondaryLanguage(String secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public String getTertiaryLanguage() {
        return tertiaryLanguage;
    }

    public void setTertiaryLanguage(String tertiaryLanguage) {
        this.tertiaryLanguage = tertiaryLanguage;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public ZonedDateTime getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(ZonedDateTime lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public boolean isActiveSynch() {
        return activeSynch;
    }

    public void setActiveSynch(boolean activeSynch) {
        this.activeSynch = activeSynch;
    }

    public ZonedDateTime getNextSyncDate() {
        return nextSyncDate;
    }

    public void setNextSyncDate(ZonedDateTime nextSyncDate) {
        this.nextSyncDate = nextSyncDate;
    }

}
