package com.daimler.xwep.truck.entity;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name = "manage_subscription")
public class ManageSubscription {
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "nationality")
	private String nationality;
	
	@Column(name = "outlet_id")
	private String outletId;
	
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "legalName")
	private String legalName;
	
	@Column(name = "brand_code")
	private String brandCode;
	
	@Column(name = "road")
	private String road;
	
	@Column(name = "post_code")
	private String postCode;
	
	@Column(name = "place")
	private String place;
	
	@Column(name = "email")
	private String email;

	@Column(name = "created_date")
	private ZonedDateTime createdDate;

	/*@Column(name = "email_passenger_cars")
	private String emailPassengerCars;
	
	@Column(name = "email_transporter")
	private String emailTransporter;*/
	
	@Column(name = "email_lkw")
	private String emailLKW;
	
	/*@Column(name = "email_smart_passenger_cars")
	private String emailSmartPassengerCars;*/
	
	@Column(name = "status", nullable = false)
	private String status;
	
	@Column(name = "is_user_updated", nullable = false)
	private boolean userUpdated = false; 
	
	/*@Column(name = "eq_service_enabled", nullable = false)
    private boolean eqService = false; 
	
	@Column(name = "car_service_enabled", nullable = false)
    private boolean carService = false; 
	
	@Column(name = "van_service_enabled", nullable = false)
    private boolean vanService = false; 
	
	@Column(name = "truck_service_enabled", nullable = false)
    private boolean truckService = false; */
	
	@OneToMany(mappedBy = "manageSubscription", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SubscriptionStatus> subscriptionStatus;
	
    @OneToMany(mappedBy = "manageSubscription", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BrandDetails> brandDetails;

    @Column(name = "gssn_classic_id")
    private String gssnClassicId;

    public ManageSubscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOutletId() {
		return outletId;
	}

	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public List<SubscriptionStatus> getSubscriptionStatus() {
		return subscriptionStatus;
	}

	public void setSubscriptionStatus(List<SubscriptionStatus> subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/*public String getEmailPassengerCars() {
		return emailPassengerCars;
	}

	public void setEmailPassengerCars(String emailPassengerCars) {
		this.emailPassengerCars = emailPassengerCars;
	}

	public String getEmailTransporter() {
		return emailTransporter;
	}

	public void setEmailTransporter(String emailTransporter) {
		this.emailTransporter = emailTransporter;
	}*/
	public String getEmailLKW() {
		return emailLKW;
	}

	public void setEmailLKW(String emailLKW) {
		this.emailLKW = emailLKW;
	}

	/*public String getEmailSmartPassengerCars() {
		return emailSmartPassengerCars;
	}

	public void setEmailSmartPassengerCars(String emailSmartPassengerCars) {
		this.emailSmartPassengerCars = emailSmartPassengerCars;
	}*/

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isUserUpdated() {
		return userUpdated;
	}

	public void setUserUpdated(boolean userUpdated) {
		this.userUpdated = userUpdated;
	}

    public List<BrandDetails> getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(List<BrandDetails> brandDetails) {
        this.brandDetails = brandDetails;
    }

    public String getGssnClassicId() {
        return gssnClassicId;
    }

    public void setGssnClassicId(String gssnClassicId) {
        this.gssnClassicId = gssnClassicId;
    }

    /*public boolean isEqService() {
        return eqService;
    }

    public void setEqService(boolean eqService) {
        this.eqService = eqService;
    }

    public boolean isCarService() {
        return carService;
    }

    public void setCarService(boolean carService) {
        this.carService = carService;
    }

    public boolean isVanService() {
        return vanService;
    }

    public void setVanService(boolean vanService) {
        this.vanService = vanService;
    }

    public boolean isTruckService() {
        return truckService;
    }

    public void setTruckService(boolean truckService) {
        this.truckService = truckService;
    }*/

}
