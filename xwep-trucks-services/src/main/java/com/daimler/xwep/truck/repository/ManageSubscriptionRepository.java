package com.daimler.xwep.truck.repository;



import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.daimler.xwep.truck.dto.SubscriptionCountDTO;
import com.daimler.xwep.truck.entity.ManageSubscription;

public interface ManageSubscriptionRepository extends PagingAndSortingRepository<ManageSubscription, Long> {

    @Cacheable("outletsByNationality")
	List<ManageSubscription> findByNationality(String Nationality);

	/*@Query("Select new com.daimler.xwep.dto.SubscriptionCountDTO(count(m.id), m.nationality) from ManageSubscription m group by m.nationality")
	public Page<SubscriptionCountDTO> getAllDealCountWithNationality(Pageable pageable);*/
	
	@Query("Select new com.daimler.xwep.truck.dto.SubscriptionCountDTO(count(m.id), m.nationality) from ManageSubscription m group by m.nationality")
    public Page<SubscriptionCountDTO> getAllDealCountWithNationality(Pageable pageable);

	/*@Query("Select new com.daimler.xwep.dto.SubscriptionCountDTO(count(m.id), m.nationality) from ManageSubscription m where m.nationality like :query% group by m.nationality")
	public Page<SubscriptionCountDTO> getAllDealCountWithNationalitySearch(Pageable pageable,
			@Param("query") String query);*/
	
	@Query("Select new com.daimler.xwep.truck.dto.SubscriptionCountDTO(count(m.id), m.nationality) from ManageSubscription m where m.nationality like :query% group by m.nationality")
    public Page<SubscriptionCountDTO> getAllDealCountWithNationalitySearch(Pageable pageable,
            @Param("query") String query);
	
    ManageSubscription findByOutletId(String outletId);

    ManageSubscription findByGssnClassicId(String gssnClassicId);
    
    @Query("Select m from ManageSubscription m where m.nationality=:nationality")
    List<ManageSubscription> findByNationalityandValidDivision(@Param("nationality") String nationality);
    
    /*@Query("Select m from ManageSubscription m where m.truckService=true or m.vanService=true or  m.carService=true or m.eqService=true")
    List<ManageSubscription> findByValidDivision();*/
    
    @Query("Select count(m.id) from ManageSubscription m where m.nationality=:nationality")
    Integer findCountByNationalityandValidDivision(@Param("nationality") String nationality);
    
   /* @Query("Select count(m.id) from ManageSubscription m where m.truckService=true or m.vanService=true or  m.carService=true or m.eqService=true")
    Integer findCountByValidDivision();*/
    
    @Override
    @Cacheable("allOutlets")
    List<ManageSubscription> findAll();

}
