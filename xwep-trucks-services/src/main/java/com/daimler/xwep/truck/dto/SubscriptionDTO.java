package com.daimler.xwep.truck.dto;

import java.io.Serializable;

public class SubscriptionDTO implements Serializable {
	
	private static final long serialVersionUID = -4450098967335011000L;
	
	private OutletsDTO _embedded;

	public OutletsDTO get_embedded() {
		return _embedded;
	}

	public void set_embedded(OutletsDTO _embedded) {
		this._embedded = _embedded;
	}
	
}
	
	


