package com.daimler.xwep.truck.entityVO;

public class DealerBasicInfoVO {

    public DealerBasicInfoVO() {
        super();
    }

    public DealerBasicInfoVO(String nationality, String legalName, String brandCode, String road, String postCode,
            String place, String email, String outletId, String companyId) {
        super();
        this.nationality = nationality;
        this.legalName = legalName;
        this.brandCode = brandCode;
        this.road = road;
        this.postCode = postCode;
        this.place = place;
        this.email = email;
        this.outletId = outletId;
        this.companyId = companyId;
        /*this.eqDealer = eqDealer;
        this.carDealer = carDealer;
        this.vanDealer = vanDealer;
        this.truckDealer = truckDealer;*/
    }

    private String nationality;

    private String legalName;

    private String brandCode;

    private String road;

    private String postCode;

    private String place;

    private String email;
    
    private String outletId;
    
    private String companyId;
    
    /*private boolean eqDealer;
    
    private boolean carDealer;
    
    private boolean vanDealer;
    
    private boolean truckDealer;*/

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

   /* public boolean isEqDealer() {
        return eqDealer;
    }

    public void setEqDealer(boolean eqDealer) {
        this.eqDealer = eqDealer;
    }

    public boolean isCarDealer() {
        return carDealer;
    }

    public void setCarDealer(boolean carDealer) {
        this.carDealer = carDealer;
    }

    public boolean isVanDealer() {
        return vanDealer;
    }

    public void setVanDealer(boolean vanDealer) {
        this.vanDealer = vanDealer;
    }

    public boolean isTruckDealer() {
        return truckDealer;
    }

    public void setTruckDealer(boolean truckDealer) {
        this.truckDealer = truckDealer;
    }*/

}
