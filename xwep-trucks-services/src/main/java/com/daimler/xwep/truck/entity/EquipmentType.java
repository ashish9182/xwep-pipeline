package com.daimler.xwep.truck.entity;

public enum EquipmentType {

	NA,FULL_EQUIPMENT, PARTIAL_EQUIPMENT
}
