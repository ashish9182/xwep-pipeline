package com.daimler.xwep.truck.dto;

import java.util.List;

public class ExternalKeys {
	
	List<BrandCodeDTO> externalKeys;

    public List<BrandCodeDTO> getExternalKeys() {
        return externalKeys;
    }

    public void setExternalKeys(List<BrandCodeDTO> externalKeys) {
        this.externalKeys = externalKeys;
    }


}
