package com.daimler.xwep.truck.dto;

import java.util.List;

public class Outlet {
	
private String outletId;
	
	private String companyId;
	
	private String legalName;
	
	private String registeredOffice;
	
	private String mainOutlet;
	
	private String affiliate;
	
	private String startOperationDate;
	
	private String outletIsActive;
	
	private Address address;
	
	private Region region;
	
	private String gssnClassicId;
	
	private List<BrandCodeDTO> brandCodes;

	public String getOutletId() {
		return outletId;
	}

	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getRegisteredOffice() {
		return registeredOffice;
	}

	public void setRegisteredOffice(String registeredOffice) {
		this.registeredOffice = registeredOffice;
	}

	public String getMainOutlet() {
		return mainOutlet;
	}

	public void setMainOutlet(String mainOutlet) {
		this.mainOutlet = mainOutlet;
	}

	public String getAffiliate() {
		return affiliate;
	}

	public void setAffiliate(String affiliate) {
		this.affiliate = affiliate;
	}

	public String getStartOperationDate() {
		return startOperationDate;
	}

	public void setStartOperationDate(String startOperationDate) {
		this.startOperationDate = startOperationDate;
	}

	public String getOutletIsActive() {
		return outletIsActive;
	}

	public void setOutletIsActive(String outletIsActive) {
		this.outletIsActive = outletIsActive;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

    public String getGssnClassicId() {
        return gssnClassicId;
    }

    public void setGssnClassicId(String gssnClassicId) {
        this.gssnClassicId = gssnClassicId;
    }

    public List<BrandCodeDTO> getBrandCodes() {
        return brandCodes;
    }

    public void setBrandCodes(List<BrandCodeDTO> brandCodes) {
        this.brandCodes = brandCodes;
    }

}
