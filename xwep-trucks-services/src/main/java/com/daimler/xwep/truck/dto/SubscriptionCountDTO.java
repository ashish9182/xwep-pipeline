package com.daimler.xwep.truck.dto;

import java.util.Map;

public class SubscriptionCountDTO {
	
	private String mpc;
    private Long dealerCount;
    private Long subscriptionCount;
    public SubscriptionCountDTO() {
    }

    public Long getSubscriptionCount() {
		return subscriptionCount;
	}



	public void setSubscriptionCount(Long subscriptionCount) {
		this.subscriptionCount = subscriptionCount;
	}



	public SubscriptionCountDTO(Long dealerCount, String mpc) {
        this.mpc = mpc;
        this.dealerCount = dealerCount;
    }

    public String getMpc() {
        return mpc;
    }

    public void setMpc(String mpc) {
        this.mpc = mpc;
    }

    public Long getDealerCount() {
        return dealerCount;
    }

    public void setDealerCount(Long dealerCount) {
        this.dealerCount = dealerCount;
    }
}
