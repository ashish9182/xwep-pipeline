package com.daimler.xwep.truck.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "subscription_status")
public class SubscriptionStatus {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "is_subscribed")
	private boolean isSubscribed;
	
	@Column(name = "equipment_type")
	private EquipmentType equipmentType;

	@Column(name = "created_date")
	private ZonedDateTime createdDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manage_subscription_id")
	private ManageSubscription manageSubscription;
	
	/*@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "division_id")
    private Division division;*/
	
    public SubscriptionStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}

	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public ManageSubscription getManageSubscription() {
		return manageSubscription;
	}

	public void setManageSubscription(ManageSubscription manageSubscription) {
		this.manageSubscription = manageSubscription;
	}

	/*public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}*/

	
	
}
