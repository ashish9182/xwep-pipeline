package com.daimler.xwep.truck.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;


@Entity
@Table(name = "division")
public class Division implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @NotNull
    @Column(name = "description")
    private String description;
    
   /* @OneToMany(mappedBy = "division", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<SubscriptionStatus> subscriptionStatus;*/
    
    

    public Division(Long id, String name, ZonedDateTime createdDate, String description
			/*List<SubscriptionStatus> subscriptionStatus*/) {
		super();
		this.id = id;
		this.name = name;
		this.createdDate = createdDate;
		this.description = description;
		//this.subscriptionStatus = subscriptionStatus;
	}



	public Division() {
		super();
		// TODO Auto-generated constructor stub
	}



	@Override
    public String toString() {
        return "Division{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createdDate=" + createdDate +
                ", description='" + description + '\'' +
                '}';
    }



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	/*public List<SubscriptionStatus> getSubscriptionStatus() {
		return subscriptionStatus;
	}



	public void setSubscriptionStatus(List<SubscriptionStatus> subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}*/



	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
