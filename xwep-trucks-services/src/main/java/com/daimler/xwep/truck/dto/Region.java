package com.daimler.xwep.truck.dto;

public class Region {
	
	private String state;

	private String province;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
}
