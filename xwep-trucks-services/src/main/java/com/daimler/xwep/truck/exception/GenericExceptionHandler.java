package com.daimler.xwep.truck.exception;

import com.daimler.xwep.truck.constant.XWEPTruckConfigurationConstants;
import com.daimler.xwep.truck.dto.GenericResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<GenericResponse> unHandledException(ResourceNotFoundException exception,
			HttpServletRequest httpServletRequest) {
	    if(!StringUtils.isEmpty(exception.getMessage())){
		return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
				HttpStatus.NOT_FOUND.value(), "404", exception.getMessage()),
				HttpStatus.NOT_FOUND);}
	    return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                HttpStatus.NOT_FOUND.value(), "404", "Invalid request"),
                HttpStatus.NOT_FOUND);
	    
	}
	
	@ExceptionHandler(ForbiddenException.class)
	public ResponseEntity<GenericResponse> forbiddenException(ForbiddenException exception,
			HttpServletRequest httpServletRequest) {
		return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
				HttpStatus.FORBIDDEN.value(), "403", "User is not authorized"),
				HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<GenericResponse> SubscriptionNotFoundException(UnauthorizedException exception,
            HttpServletRequest httpServletRequest) {
        return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                HttpStatus.UNAUTHORIZED.value(), "401", exception.getMessage()),
                HttpStatus.UNAUTHORIZED);
    }
	
	@ExceptionHandler(RequestTimeOutException.class)
    public ResponseEntity<GenericResponse> requestTimeoutException(RequestTimeOutException exception,
            HttpServletRequest httpServletRequest) {
        return new ResponseEntity<>(new GenericResponse(XWEPTruckConfigurationConstants.FAILURE,
                HttpStatus.REQUEST_TIMEOUT.value(), "408", "Request Timeout Exception"),
                HttpStatus.REQUEST_TIMEOUT);
    }
	
}
