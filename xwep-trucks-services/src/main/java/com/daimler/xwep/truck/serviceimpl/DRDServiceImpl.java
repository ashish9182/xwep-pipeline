package com.daimler.xwep.truck.serviceimpl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.daimler.xwep.truck.dto.UserInfo;
import com.daimler.xwep.truck.service.DRDService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class DRDServiceImpl implements DRDService {

    @Override
    public UserInfo getuserInfo(HttpServletRequest request) throws JsonMappingException, JsonProcessingException {
        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
        return userInfo;
    }

}


