package com.daimler.xwep.truck.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.daimler.xwep.truck.dto.AggregateSubscriptionDTO;
import com.daimler.xwep.truck.dto.SubscriptionCountDTO;
import com.daimler.xwep.truck.entity.CountryParticipation;
import com.daimler.xwep.truck.entity.ManageSubscription;
import com.daimler.xwep.truck.entityVO.DealerInfoWithSubscriptionStatusVO;
import com.daimler.xwep.truck.entityVO.ManageSubscriptionVO;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface ManageSubscriptionService {
	
	public ManageSubscriptionVO updateManageSubscription(Long id, ManageSubscriptionVO manageSubscriptionVO);

    public List<ManageSubscriptionVO> findManageSubscriptionByNationality(String nationality);

    public AggregateSubscriptionDTO findAggregatedSubscriptionDetails(String query);

    public Page<SubscriptionCountDTO> getAllSubscriptionInfo(Integer pageNo, Integer pageSize, String sort,
            String query);

    public ByteArrayInputStream downloadHQDealers(String language) throws IOException;

    public ByteArrayInputStream downloadMPCDealers(String nationality, String language) throws IOException;

    void synchManageSubscription(String nationality, boolean isCronJob) throws JsonProcessingException;

    void syncOperation(CountryParticipation countryParticipation);

    void synchManageSubscription(String nationality) throws JsonProcessingException;

    public DealerInfoWithSubscriptionStatusVO getDealerInfo(String outletId);

    String deleteManageSubscriptionById(ManageSubscription manageSubscription);
    
    public List<ManageSubscription> getExcelDataAsList();
	
	public String saveExcelData(List<ManageSubscription> manageSubscriptions);
    

}
