package com.daimler.xwep.truck.service;

import javax.servlet.http.HttpServletRequest;

import com.daimler.xwep.truck.dto.UserInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface DRDService {
	
	public UserInfo getuserInfo(HttpServletRequest request) throws JsonMappingException, JsonProcessingException;
}
