package com.daimler.xwep.truck.dto;

public class CommunicationData {
	
	private String communicationFieldId;
	
	private String value;
	
	public String getCommunicationFieldId() {
		return communicationFieldId;
	}

	public void setCommunicationFieldId(String communicationFieldId) {
		this.communicationFieldId = communicationFieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
