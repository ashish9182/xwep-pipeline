package com.daimler.xwep.truck.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.daimler.xwep.truck.entity.BrandDetails;

@Repository
public interface BrandDetailsRepository extends JpaRepository<BrandDetails, Long>{
    
}
