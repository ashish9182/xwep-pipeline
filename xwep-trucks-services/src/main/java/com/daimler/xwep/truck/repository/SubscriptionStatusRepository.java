package com.daimler.xwep.truck.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.daimler.xwep.truck.entity.SubscriptionStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionStatusRepository extends JpaRepository<SubscriptionStatus, Long>{

	List<SubscriptionStatus> findByManageSubscriptionId (Long manageSubscriptionId);

	@Query("Select count(s.id) from SubscriptionStatus s where s.isSubscribed is true and s.manageSubscription.nationality=:nationality and s.manageSubscription.status!='INACTIVE'")
	public Long countByNationality(@Param("nationality") String nationality);
	
	/*@Query("Select count(s.id) from SubscriptionStatus s where s.isSubscribed is true and s.division.name=:division and s.manageSubscription.nationality=:nationality and s.manageSubscription.vanService=true and s.manageSubscription.status!='INACTIVE'")
    public Long countByVan(@Param("nationality") String nationality,@Param("division")String division);
	
	@Query("Select count(s.id) from SubscriptionStatus s where s.isSubscribed is true and s.division.name=:division and s.manageSubscription.nationality=:nationality and s.manageSubscription.carService=true and s.manageSubscription.status!='INACTIVE'")
    public Long countByCar(@Param("nationality") String nationality,@Param("division")String division);
	
	@Query("Select count(s.id) from SubscriptionStatus s where s.isSubscribed is true and s.division.name=:division and s.manageSubscription.nationality=:nationality and s.manageSubscription.eqService=true and s.manageSubscription.status!='INACTIVE'")
    public Long countByEQ(@Param("nationality") String nationality,@Param("division")String division);*/
}
