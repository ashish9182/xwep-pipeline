package com.daimler.xwep.truck.entityVO;

import java.util.List;

public class DealerInfoWithSubscriptionStatusVO {

    public DealerInfoWithSubscriptionStatusVO(List<SubscriptionStatusVO> subscriptionStatusList,
            DealerBasicInfoVO dealerBasicInfoVO) {
        super();
        this.subscriptionStatusList = subscriptionStatusList;
        this.dealerBasicInfoVO = dealerBasicInfoVO;
    }

    private List<SubscriptionStatusVO> subscriptionStatusList;

    private DealerBasicInfoVO dealerBasicInfoVO;

    public List<SubscriptionStatusVO> getSubscriptionStatusList() {
        return subscriptionStatusList;
    }

    public void setSubscriptionStatusList(List<SubscriptionStatusVO> subscriptionStatusList) {
        this.subscriptionStatusList = subscriptionStatusList;
    }

    public DealerBasicInfoVO getDealerBasicInfoVO() {
        return dealerBasicInfoVO;
    }

    public void setDealerBasicInfoVO(DealerBasicInfoVO dealerBasicInfoVO) {
        this.dealerBasicInfoVO = dealerBasicInfoVO;
    }

}
