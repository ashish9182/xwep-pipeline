package com.daimler.xwep.truck.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse {
	
	 	private String responseStatus;   // string success or failure
	    private int responseCode;       // http status code 200/205/400
	    private Object data;
	    private String errorCode;
	    private Object error;

	    public Object getError() {
	        return error;
	    }

	    public void setError(Object error) {
	        this.error = error;
	    }

	    public String getErrorCode() {
	        return errorCode;
	    }

	    public void setErrorCode(String errorCode) {
	        this.errorCode = errorCode;
	    }


	    public GenericResponse(String responseStatus, int responseCode, Object data) {
	        this.responseStatus = responseStatus;
	        this.responseCode = responseCode;
	        this.data = data;
	    }

	    public GenericResponse(String responseStatus, int responseCode, String errorCode, Object error) {
	        this.responseStatus = responseStatus;
	        this.responseCode = responseCode;
	        this.errorCode = errorCode;
	        this.error = error;
	    }

	    public String getResponseStatus() {
	        return responseStatus;
	    }

	    public void setResponseStatus(String responseStatus) {
	        this.responseStatus = responseStatus;
	    }

	    public int getResponseCode() {
	        return responseCode;
	    }

	    public void setResponseCode(int responseCode) {
	        this.responseCode = responseCode;
	    }

	    public Object getData() {
	        return data;
	    }

	    public void setData(Object data) {
	        this.data = data;
	    }

}
