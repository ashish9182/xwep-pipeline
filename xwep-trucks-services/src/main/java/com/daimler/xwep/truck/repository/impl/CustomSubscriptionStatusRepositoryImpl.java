package com.daimler.xwep.truck.repository.impl;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.daimler.xwep.truck.dto.AggregateSubscriptionDTO;
import com.daimler.xwep.truck.repository.CustomSubscriptionStatusRepository;
import com.daimler.xwep.truck.repository.ManageSubscriptionRepository;
import com.daimler.xwep.truck.entity.EquipmentType;
import com.google.common.base.Strings;

@Repository
public class CustomSubscriptionStatusRepositoryImpl implements CustomSubscriptionStatusRepository {
	
	@Autowired
    EntityManager em;
	
	@Autowired
	private ManageSubscriptionRepository manageSubscriptionRepository;
	
	@Override
	public AggregateSubscriptionDTO aggregatedSubscriptionDetails(String nationality) {
		StringBuilder queryBuilder = new StringBuilder();
		/*Long count = null;
		if (!Strings.isNullOrEmpty(nationality)) {
			count = (long) manageSubscriptionRepository.findCountByNationalityandValidDivision(nationality);
		}*/
		queryBuilder.append("Select Count(DISTINCT manage_subscription.id) as total");
		queryBuilder.append(", SUM(CASE WHEN subscription_status.equipment_type!='" + EquipmentType.NA.ordinal()
		+ "' AND manage_subscription.status!='INACTIVE' THEN 1 ELSE 0 END) as NA");
		queryBuilder.append(", SUM(CASE WHEN subscription_status.equipment_type='" + EquipmentType.FULL_EQUIPMENT.ordinal()
		+ "' AND subscription_status.is_subscribed is true " 
		+ "AND manage_subscription.status!='INACTIVE' "
		+ "THEN 1 ELSE 0 END) fullSubscribed");
		queryBuilder.append(", SUM(CASE WHEN subscription_status.equipment_type='" + EquipmentType.PARTIAL_EQUIPMENT.ordinal()
		+ "' AND subscription_status.is_subscribed is true "
		+ "AND manage_subscription.status!='INACTIVE' "
		+ "THEN 1 ELSE 0 END) partialSubscribed");
		queryBuilder.append(", SUM(CASE WHEN subscription_status.equipment_type='" + EquipmentType.PARTIAL_EQUIPMENT.ordinal()
		+ "' AND subscription_status.is_subscribed is false "
		+ "AND manage_subscription.status!='INACTIVE' "
		+ "THEN 1 ELSE 0 END) partialUnsub");
		queryBuilder.append(", SUM(CASE WHEN subscription_status.equipment_type='" + EquipmentType.FULL_EQUIPMENT.ordinal()
		+ "' AND subscription_status.is_subscribed is false "
		+ "AND manage_subscription.status!='INACTIVE' "
		+ "THEN 1 ELSE 0 END) fullUnsub");
		 queryBuilder.append(
	                " FROM subscription_status left join manage_subscription  on manage_subscription.id=subscription_status.manage_subscription_id where subscription_status.id is not null");
	        if (!Strings.isNullOrEmpty(nationality)) {
	            queryBuilder.append("  and manage_subscription.nationality like '" + nationality + "'");
	        }
	     Query query = em.createNativeQuery(queryBuilder.toString());
	     Object details[] = (Object[]) (query.getSingleResult());
	     
	     AggregateSubscriptionDTO aggregateSubscriptionDTO = new AggregateSubscriptionDTO();
	     
	      if (details != null && details[0] != null && ((BigInteger) details[0]).longValue() > 0) {

	    	 aggregateSubscriptionDTO.setTotal((((BigInteger) details[0]).longValue()));
	    	 aggregateSubscriptionDTO.setFullyEquipped((((BigInteger) details[0 + 2]).longValue()));
	    	 aggregateSubscriptionDTO.setPartiallyEquipped((((BigInteger) details[0 + 3]).longValue()));
	    	 aggregateSubscriptionDTO.setPartiallyEquippedWithoutSubscription((((BigInteger) details[0 + 4]).longValue()));
	    	 aggregateSubscriptionDTO.setFullyEquippedWithoutSubscription(((BigInteger) details[0 + 5]).longValue());
	     }
	     return aggregateSubscriptionDTO;
	}

}
