package com.daimler.xwep.truck.dto;

import java.util.List;

public class GssnCommunicationResponse {
	
	private List<ServiceCommunicationData> offeredServiceCommunicationData;
	
	private List<GeneralCommunicationData> generalCommunicationData;

	public List<ServiceCommunicationData> getOfferedServiceCommunicationData() {
		return offeredServiceCommunicationData;
	}

	public void setOfferedServiceCommunicationData(List<ServiceCommunicationData> offeredServiceCommunicationData) {
		this.offeredServiceCommunicationData = offeredServiceCommunicationData;
	}

	public List<GeneralCommunicationData> getGeneralCommunicationData() {
		return generalCommunicationData;
	}

	public void setGeneralCommunicationData(List<GeneralCommunicationData> generalCommunicationData) {
		this.generalCommunicationData = generalCommunicationData;
	}
}
