package com.daimler.xwep.truck.serviceimpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.daimler.xwep.truck.constant.OutletStatus;
import com.daimler.xwep.truck.constant.XWEPTruckConfigurationConstants;
import com.daimler.xwep.truck.dto.AggregateSubscriptionDTO;
import com.daimler.xwep.truck.dto.SubscriptionCountDTO;
import com.daimler.xwep.truck.entity.ManageSubscription;
import com.daimler.xwep.truck.entity.SubscriptionStatus;
import com.daimler.xwep.truck.entity.CountryParticipation;
import com.daimler.xwep.truck.entity.EquipmentType;
import com.daimler.xwep.truck.entityVO.DealerBasicInfoVO;
import com.daimler.xwep.truck.entityVO.DealerInfoWithSubscriptionStatusVO;
import com.daimler.xwep.truck.entityVO.ManageSubscriptionVO;
import com.daimler.xwep.truck.entityVO.SubscriptionStatusVO;
import com.daimler.xwep.truck.exception.ForbiddenException;
import com.daimler.xwep.truck.exception.ResourceNotFoundException;
import com.daimler.xwep.truck.helper.DataAccessHelperService;
import com.daimler.xwep.truck.helper.GssnDataSynchHelperService;
import com.daimler.xwep.truck.repository.CountryParticipationRepository;
import com.daimler.xwep.truck.repository.CustomSubscriptionStatusRepository;
import com.daimler.xwep.truck.repository.ManageSubscriptionRepository;
import com.daimler.xwep.truck.repository.SubscriptionStatusRepository;
import com.daimler.xwep.truck.service.ManageSubscriptionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;

@Service
public class ManageSubscriptionServiceImpl implements ManageSubscriptionService {
	
	private static final Logger logger = LoggerFactory.getLogger(ManageSubscriptionServiceImpl.class);

    @Autowired
    private ManageSubscriptionRepository manageSubscriptionRepository;

    @Autowired
    private SubscriptionStatusRepository subscriptionStatusRepository;

    @Autowired
    private CustomSubscriptionStatusRepository customSubscriptionStatusRepository;

    @Autowired
    private CountryParticipationRepository countryParticipationRepository;
    
    @Autowired
    CacheManager cacheManager;
    
    @Autowired
    private DataAccessHelperService dataAccessHelperService;
    
    @Autowired
    private GssnDataSynchHelperService gssnDataSynchHelperService;
    
    @Value("${app.upload.file:${user.home}}")
	public String EXCEL_FILE_PATH;

	Workbook workbook;
	
    /**
     * findAggregatedSubscriptionDetails
     * 
     * @param query
     * @return
     */
    public AggregateSubscriptionDTO findAggregatedSubscriptionDetails(String query) {
        return customSubscriptionStatusRepository.aggregatedSubscriptionDetails(query);
    }
	
	/**
     * getAllSubscriptionInfo
     * 
     * @return
     */
    public Page<SubscriptionCountDTO> getAllSubscriptionInfo(Integer pageNo, Integer pageSize, String sort,
            String query) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sort));
        //List<Division> divisions = divisionRepository.findAll();
        Page<SubscriptionCountDTO> dealerCountList;
        if (Strings.isNullOrEmpty(query)) {
            dealerCountList = manageSubscriptionRepository.getAllDealCountWithNationality(pageable);
        } else {
            dealerCountList = manageSubscriptionRepository.getAllDealCountWithNationalitySearch(pageable,
                    query.toUpperCase());
        }
        dealerCountList.getContent().forEach(dealer -> {
        	Long subscriptionCount = subscriptionStatusRepository.countByNationality(dealer.getMpc());
        	dealer.setSubscriptionCount(subscriptionCount);
        });
        return dealerCountList;
    }
    
    private ManageSubscriptionVO getManageSubscriptionVO(ManageSubscription manageSubscription) {
        ManageSubscriptionVO manageSubscriptionVO = new ManageSubscriptionVO();
        manageSubscriptionVO.setId(manageSubscription.getId());
        manageSubscriptionVO.setCompanyId(manageSubscription.getCompanyId());
        manageSubscriptionVO.setLegalName(manageSubscription.getLegalName());
        manageSubscriptionVO.setNationality(manageSubscription.getNationality());
        manageSubscriptionVO.setOutletId(manageSubscription.getOutletId());
        manageSubscriptionVO.setPlace(manageSubscription.getPlace());
        manageSubscriptionVO.setPostCode(manageSubscription.getPostCode());
        manageSubscriptionVO.setRoad(manageSubscription.getRoad());
        manageSubscriptionVO.setEmail(manageSubscription.getEmail());
        manageSubscriptionVO.setEmailLKW(manageSubscription.getEmailLKW());
        manageSubscriptionVO.setStatus(manageSubscription.getStatus());
        manageSubscriptionVO.setUserUpdated(manageSubscription.isUserUpdated());
        manageSubscriptionVO.setBrandCode(manageSubscription.getBrandCode());
        manageSubscriptionVO.setUuid(dataAccessHelperService.getEncrptedString(manageSubscription.getId().toString()));
      //  manageSubscriptionVO.setTruckEnabled(manageSubscription.isTruckService());
        List<SubscriptionStatusVO> subscriptionStatusList = new ArrayList<>();
        for (SubscriptionStatus subscriptionStatus : manageSubscription.getSubscriptionStatus()) {
            SubscriptionStatusVO subscriptionStatusVO = new SubscriptionStatusVO();
            subscriptionStatusVO.setId(subscriptionStatus.getId());
            subscriptionStatusVO.setSubscribed(subscriptionStatus.isSubscribed());
            subscriptionStatusVO.setEquipmentType(subscriptionStatus.getEquipmentType());
            //if (subscriptionStatus.getDivision() != null) {
                subscriptionStatusVO.setDivisionName("TRUCK");
            //}
            subscriptionStatusList.add(subscriptionStatusVO);
        }
        manageSubscriptionVO.setSubscriptionStatus(subscriptionStatusList);
        return manageSubscriptionVO;
    }
    

    /**
     * updateManageSubscription
     * 
     * @param id
     * @param manageSubscriptionVO
     * @return
     */
    @Override
    @CacheEvict(value = "outletsByNationality")
    public ManageSubscriptionVO updateManageSubscription(Long id, ManageSubscriptionVO manageSubscriptionVO) {
        clearCache();
        ManageSubscription manageSubscription = manageSubscriptionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("manageSubscriptionId " + id + " not found"));
        manageSubscription.setStatus(OutletStatus.OLD.toString());
        manageSubscription.setUserUpdated(Boolean.TRUE);
        List<SubscriptionStatus> subscriptionStatusList = new ArrayList<>();
        List<SubscriptionStatus> existingsubscriptionStatusList = manageSubscription.getSubscriptionStatus();
        for (SubscriptionStatusVO subscriptionStatusVO : manageSubscriptionVO.getSubscriptionStatus()) {
            SubscriptionStatus subscriptionStatus = null;
            if (Objects.nonNull(subscriptionStatusVO.getId())) {
                Optional<SubscriptionStatus> subscriptionStatusOptional = existingsubscriptionStatusList.stream()
                        .filter(x -> x.getId().equals(subscriptionStatusVO.getId()))
                        .findFirst();
                if (!subscriptionStatusOptional.isPresent()) {
                    throw new ResourceNotFoundException("Invalid Subscription Id");
                } else {
                    subscriptionStatus = subscriptionStatusOptional.get();
                }
            } /*else {
                if (subscriptionStatusVO.getDivisionName().equals("EQ")) {
                    subscriptionStatus = createBaseSubscriptionStatus(manageSubscription, subscriptionStatusVO);
                } else {
                    throw new ResourceNotFoundException(
                            "subscription with division" + subscriptionStatusVO.getDivisionName() + " alreday exist");
                }

            }*/

            subscriptionStatus.setSubscribed(subscriptionStatusVO.isSubscribed());
            subscriptionStatus.setEquipmentType(subscriptionStatusVO.getEquipmentType());
            subscriptionStatusList.add(subscriptionStatus);

        }
        manageSubscription.setSubscriptionStatus(subscriptionStatusList);
        manageSubscriptionRepository.save(manageSubscription);
        ManageSubscriptionVO manageSubscriptionVO1 = new ManageSubscriptionVO();
        manageSubscriptionVO1.setId(manageSubscription.getId());
        manageSubscriptionVO1.setBrandCode(manageSubscription.getBrandCode());
        manageSubscriptionVO1.setCompanyId(manageSubscription.getCompanyId());
        manageSubscriptionVO1.setLegalName(manageSubscription.getLegalName());
        manageSubscriptionVO1.setNationality(manageSubscription.getNationality());
        manageSubscriptionVO1.setOutletId(manageSubscription.getOutletId());
        manageSubscriptionVO1.setPlace(manageSubscription.getPlace());
        manageSubscriptionVO1.setPostCode(manageSubscription.getPostCode());
        manageSubscriptionVO1.setRoad(manageSubscription.getRoad());
        manageSubscriptionVO1.setEmail(manageSubscription.getEmail());
        manageSubscriptionVO1.setEmailLKW(manageSubscription.getEmailLKW());
        manageSubscriptionVO1.setUuid(dataAccessHelperService.getEncrptedString(manageSubscription.getId().toString()));
        manageSubscriptionVO1.setStatus(OutletStatus.OLD.toString());
        manageSubscriptionVO1.setUserUpdated(true);
        //manageSubscriptionVO1.setTruckEnabled(manageSubscription.isTruckService());
        List<SubscriptionStatusVO> subscriptionStatusList1 = new ArrayList<>();
        for (SubscriptionStatus subscriptionStatus1 : manageSubscription.getSubscriptionStatus()) {
            SubscriptionStatusVO subscriptionStatusVO1 = new SubscriptionStatusVO();
            subscriptionStatusVO1.setId(subscriptionStatus1.getId());
            subscriptionStatusVO1.setSubscribed(subscriptionStatus1.isSubscribed());
            subscriptionStatusVO1.setEquipmentType(subscriptionStatus1.getEquipmentType());
            subscriptionStatusVO1.setDivisionName("TRUCK");
            //if (subscriptionStatus1.getDivision() != null) {
                //subscriptionStatusVO1.setDivisionName(subscriptionStatus1.getDivision().getName());
               // subscriptionStatusVO1.setDivisionName("TRUCK");
            //}
            subscriptionStatusList1.add(subscriptionStatusVO1);

        }
        manageSubscriptionVO1.setSubscriptionStatus(subscriptionStatusList1);
        //manageSubscriptionVO1.setEqEnabled(manageSubscription.isEqService());
        return manageSubscriptionVO1;
    }

    /**
     * deleteManageSubscriptionById
     * 
     * @param id
     * @return
     */
    @Override
    public String deleteManageSubscriptionById(ManageSubscription manageSubscription) {
        clearCache();
        manageSubscriptionRepository.delete(manageSubscription);
        return "Deleted Successfully!";
    }
    
    @Override
    public List<ManageSubscriptionVO> findManageSubscriptionByNationality(String nationality) {
        List<ManageSubscription> manageSubscriptions = manageSubscriptionRepository
                .findByNationalityandValidDivision(nationality);
        List<ManageSubscriptionVO> manageSubscriptionVOs = new ArrayList<>();
        for (ManageSubscription manageSubscription : manageSubscriptions) {
            manageSubscriptionVOs.add(getManageSubscriptionVO(manageSubscription));
        }
        return manageSubscriptionVOs;
    }

    @Override
    public ByteArrayInputStream downloadHQDealers(String language) throws IOException {
        Iterable<ManageSubscription> manageSubscriptions = manageSubscriptionRepository.findAll();
        ByteArrayOutputStream out = extractCSVInfo(manageSubscriptions, language);
        return new ByteArrayInputStream(out.toByteArray());

    }

    private ByteArrayOutputStream extractCSVInfo(Iterable<ManageSubscription> manageSubscriptions, String language)
            throws IOException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
        int rowNum = 0;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sonderwerkzeug-Statistik");
        XSSFCellStyle style = workbook.createCellStyle();
        try {
            String[][] headers = XWEPTruckConfigurationConstants.getHeaders(language);
            XSSFCellStyle hStyle = workbook.createCellStyle();
            XSSFCellStyle hCStyle = workbook.createCellStyle();
            XSSFCellStyle rStyle = workbook.createCellStyle();
            rStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = workbook.createFont();
            font.setFontName("Calibri");
            font.setBold(true);
            font.setFontHeight(11);
            font.setColor(IndexedColors.BLACK.getIndex());
            hStyle.setFont(font);
            hCStyle.setFont(font);
            hCStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            hCStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            List<Integer> borderCells = new ArrayList<>();
            for (int i = 0; i < headers.length; i++) {
                Row headerRow = sheet.createRow(rowNum++);
                headerRow.setRowStyle(style);
                for (int j = 0; j < headers[i].length; j++) {
                    Cell cell = headerRow.createCell(j);
                    if (i == 0 && j == 7) {
                        sheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 7));
                        
                    } else if (i == 0 && j == 12) {
                        sheet.addMergedRegion(new CellRangeAddress(0, 0, 10, 12));
                       
                    } else if(i == 0 && j == 0) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
                    	
                    } else if(i == 0 && j == 1) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
                    	
                    } else if(i == 0 && j == 2) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 2, 2));
                    	
                    } else if(i == 0 && j == 3) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 3, 3));
                    	
                    } else if(i == 0 && j == 4) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 4, 4));
                    	sheet.autoSizeColumn(j,true);
                    } else if(i == 0 && j == 8) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 8, 8));
                    	
                    } else if(i == 0 && j == 9) {
                    	sheet.addMergedRegion(new CellRangeAddress(0, 1, 9, 9));
                    	
                    } 
                    cell.setCellValue(headers[i][j]);
                    if((i==0 || i== 1) && (j== 7 || j== 8 || j ==9)) {
                    	cell.setCellStyle(hCStyle);
                    } else {
                    	cell.setCellStyle(hStyle);
                    }
                    sheet.autoSizeColumn(j,true);
                }

            }
            XSSFRow hRow = sheet.getRow(0);
            CellUtil.setAlignment(hRow.getCell(5), hStyle.getAlignmentEnum().CENTER);
            CellUtil.setAlignment(hRow.getCell(10), hStyle.getAlignmentEnum().CENTER);
            /*CellUtil.setAlignment(hRow.getCell(16), hStyle.getAlignmentEnum().CENTER);
            CellUtil.setAlignment(hRow.getCell(19), hStyle.getAlignmentEnum().CENTER);
            CellUtil.setAlignment(hRow.getCell(22), hStyle.getAlignmentEnum().CENTER);*/

            int n[] = { rowNum }, z[] = { rowNum };
            for (ManageSubscription manageSubscription : manageSubscriptions) {
                Row row = sheet.createRow(n[0]++);
                row.createCell(0).setCellValue(manageSubscription.getNationality());
                row.createCell(1).setCellValue(manageSubscription.getOutletId());
                row.createCell(2).setCellValue(manageSubscription.getCompanyId());
                row.createCell(3).setCellValue(manageSubscription.getLegalName());
                row.createCell(4).setCellValue(manageSubscription.getBrandCode());
                row.createCell(5).setCellValue(manageSubscription.getRoad());
                row.createCell(6).setCellValue(manageSubscription.getPostCode());
                row.createCell(7).setCellValue(manageSubscription.getPlace());
                row.createCell(8).setCellValue(manageSubscription.getEmail());
                row.createCell(9).setCellValue(manageSubscription.getEmailLKW());
                for (SubscriptionStatus subscriptionStatus : manageSubscription.getSubscriptionStatus()) {
                        if (subscriptionStatus.isSubscribed()) {
                            Cell cell= row.createCell(10);
                            cell.setCellValue("X");
                            cell.setCellStyle(rStyle);
                        } else {
                            row.createCell(10).setCellValue("");
                        }
                        if (subscriptionStatus.getEquipmentType().equals(EquipmentType.NA)) {
                            row.createCell(11).setCellValue("");
                            row.createCell(12).setCellValue("");
                        } else if (subscriptionStatus.getEquipmentType().equals(EquipmentType.FULL_EQUIPMENT)) {
                        	 Cell cell= row.createCell(11);
                             cell.setCellValue("X");
                             cell.setCellStyle(rStyle);
                            row.createCell(12).setCellValue("");
                        } else {
                            row.createCell(11).setCellValue("");
                            Cell cell= row.createCell(12);
                            cell.setCellValue("X");
                            cell.setCellStyle(rStyle);
                        }
                    
                }
            }
            rowNum = n[0];

            workbook.write(out);
        } finally {
            workbook.close();
        }
        return out;
    }
    
    @Override
    public void synchManageSubscription(String nationality, boolean isCronJob) throws JsonProcessingException {
        clearCache();
        List<CountryParticipation> countries = countryParticipationRepository.findAll();
        for (CountryParticipation country : countries) {
            System.out.println(country.isActiveSynch() + "nationality-" + country.getIsoCode());
            if (Boolean.FALSE.equals(country.isActiveSynch())) {
                country.setActiveSynch(true);
                country = countryParticipationRepository.save(country);
                logger.info("Started for GSSN Synch" + country.getIsoCode());
                try {
                    syncOperation(country);
                } catch (Exception e) {
                    logger.info("EXCEPTION" + e.getMessage());
                    logger.info("Sync failed CRON OUTER" + nationality);
                    country.setActiveSynch(false);
                    countryParticipationRepository.save(country);
                }
            } else {
                logger.info("Running GSSN synch present. Current synch not started " + country.getIsoCode() + " Flag : "
                        + country.isActiveSynch());
            }
        }
        ZonedDateTime lt = ZonedDateTime.now();
        if (lt.getDayOfMonth() < 15) {
            lt = lt.withDayOfMonth(15).withHour(00).withMinute(00).withSecond(00);
        } else if (lt.getDayOfMonth() >= 28) {
            lt = lt.plusMonths(1).withDayOfMonth(15).withHour(00).withMinute(00).withSecond(00);
        } else {
            if (lt.getMonthValue() != 2)
                lt = lt.withDayOfMonth(30).withHour(00).withMinute(00).withSecond(00);
            else
                lt = lt.withDayOfMonth(28).withHour(00).withMinute(00).withSecond(00);
        }
        for (CountryParticipation country : countries) {
            country.setActiveSynch(false);
            country.setNextSyncDate(lt);
        }
        countryParticipationRepository.saveAll(countries);
    }

    @Override
    public void synchManageSubscription(String nationality) throws JsonProcessingException {
        CountryParticipation countryParticipation = countryParticipationRepository.findByIsoCodeIgnoreCase(nationality);
        System.out.println(countryParticipation.isActiveSynch() + "nationality-" + countryParticipation.getIsoCode());
        if (countryParticipation != null && Boolean.FALSE.equals(countryParticipation.isActiveSynch())) {
            countryParticipation.setActiveSynch(true);
           // countryParticipation = countryParticipationRepository.save(countryParticipation);
            try {
                syncOperation(countryParticipation);
            } catch (Exception e) {
                logger.info("Sync failed OUTER FOR SYNC ONLY" + nationality);
                countryParticipation.setActiveSynch(false);
                countryParticipationRepository.save(countryParticipation);
            }
        } else {
            throw new ForbiddenException("Previous sync in progress " + nationality);
        }
        countryParticipation.setActiveSynch(false);
        countryParticipationRepository.save(countryParticipation);
    }

    @Override
    public void syncOperation(CountryParticipation countryParticipation) {
        List<ManageSubscription> newSubscriptions = null;
        List<ManageSubscription> subscriptions = (List<ManageSubscription>) manageSubscriptionRepository
                .findByNationality(countryParticipation.getIsoCode());
        newSubscriptions = syncGssnData(1, countryParticipation, subscriptions);
        if (newSubscriptions != null && newSubscriptions.size() > 0) {
            //manageSubscriptionRepository.saveAll(newSubscriptions);
            clearCache();
            logger.info("Synch completed and data saved and the Nationality is : " + countryParticipation.getIsoCode());
        } else {
            logger.info("zero synch data saved for the Nationality is : " + countryParticipation.getIsoCode());
        }
        logger.info("Sync opERATION SUCCESS " + countryParticipation.isActiveSynch() + "-"
                + countryParticipation.getIsoCode());
        countryParticipation.setActiveSynch(false);
        countryParticipation.setLastSyncDate(ZonedDateTime.now());
        countryParticipationRepository.save(countryParticipation);
    }

    private List<ManageSubscription> syncGssnData(int syncCount, CountryParticipation countryParticipation,
            List<ManageSubscription> subscriptions) {
        List<ManageSubscription> newSubscriptions = new ArrayList<>();
        try {
            logger.info(syncCount + " syncCount");
            newSubscriptions = gssnDataSynchHelperService.gssnExternalAPICall(subscriptions,
                    countryParticipation.getIsoCode());
        } catch (JsonProcessingException e) {
            logger.info("Sync jsonprocess false-" + countryParticipation.getIsoCode());
            countryParticipation.setActiveSynch(false);
            countryParticipationRepository.save(countryParticipation);
        } catch (Exception e) {
            syncCount++;
            if (syncCount < 4) {
                newSubscriptions = syncGssnData(syncCount, countryParticipation, subscriptions);
            }
            if (syncCount == 4) {
                logger.info("Sync count 4-" + countryParticipation.getIsoCode());
                countryParticipation.setActiveSynch(false);
                countryParticipationRepository.save(countryParticipation);
                logger.info("nationality " + countryParticipation.getIsoCode() + " timeout");
            }
        }
        return newSubscriptions;
    }

    @Override
    public DealerInfoWithSubscriptionStatusVO getDealerInfo(String outletId) {
        ManageSubscription manageSubscription = manageSubscriptionRepository.findByOutletId(outletId);
        List<SubscriptionStatusVO> subscriptionStatusList = new ArrayList<>();
        if (manageSubscription != null) {
            DealerBasicInfoVO basicInfoVO = new DealerBasicInfoVO(manageSubscription.getNationality(),
                    manageSubscription.getLegalName(), manageSubscription.getBrandCode(), manageSubscription.getRoad(),
                    manageSubscription.getPostCode(), manageSubscription.getPlace(), manageSubscription.getEmail(),
                    manageSubscription.getOutletId(), manageSubscription.getCompanyId()
                    /*manageSubscription.isEqService(), manageSubscription.isCarService(),
                    manageSubscription.isVanService(), manageSubscription.isTruckService()*/);
            for (SubscriptionStatus subscriptionStatus : manageSubscription.getSubscriptionStatus()) {
                SubscriptionStatusVO subscriptionStatusVO = new SubscriptionStatusVO();
                subscriptionStatusVO.setId(subscriptionStatus.getId());
                subscriptionStatusVO.setSubscribed(subscriptionStatus.isSubscribed());
                subscriptionStatusVO.setEquipmentType(subscriptionStatus.getEquipmentType());
                subscriptionStatusVO.setDivisionName("Truck");
                /*if (subscriptionStatus.getDivision() != null) {
                    
                }*/
                subscriptionStatusList.add(subscriptionStatusVO);
            }
            return new DealerInfoWithSubscriptionStatusVO(subscriptionStatusList, basicInfoVO);
        }
        return null;
    }
    private void clearCache() {
        logger.info("cache cleared");
        cacheManager.getCacheNames().stream().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }

    @Override
    public ByteArrayInputStream downloadMPCDealers(String nationality, String language) throws IOException {
        List<ManageSubscription> manageSubscriptions = manageSubscriptionRepository.findByNationalityandValidDivision(nationality);
        ByteArrayOutputStream out = extractCSVInfo(manageSubscriptions, language);
        return new ByteArrayInputStream(out.toByteArray());
    }
    
    public List<ManageSubscription> getExcelDataAsList(){
		List<String> list = new ArrayList<String>();
		DataFormatter dataFormatter = new DataFormatter();
		try {
			workbook = WorkbookFactory.create(new File(EXCEL_FILE_PATH));
		} catch (EncryptedDocumentException | IOException |InvalidFormatException e) {
			e.printStackTrace();
		} 
		Sheet sheet = workbook.getSheet("latest");
		int noOfColumns = sheet.getRow(0).getLastCellNum();
		for (Row row : sheet) {
			for(int col=0;col<noOfColumns;col++){
				Cell cell = row.getCell(col);
				if(cell == null) {
					list.add("");
				}else {
					String cellValue = dataFormatter.formatCellValue(cell);
					list.add(cellValue);
				}
			}
		}
		List<ManageSubscription> subscriptionList = createList(list, noOfColumns);
		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return subscriptionList;
	}

	private List<ManageSubscription> createList(List<String> excelData, int noOfColumns) {
		List<ManageSubscription> subscriptionList = new ArrayList<ManageSubscription>();
		int count = 0;
		count = count + 44;
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
		do {
			ManageSubscription manageSubscription = new ManageSubscription();
			manageSubscription.setNationality(excelData.get(count));
			manageSubscription.setOutletId(excelData.get(count+1));
			manageSubscription.setCompanyId(excelData.get(count+2));
			manageSubscription.setLegalName(excelData.get(count+3));
			manageSubscription.setBrandCode(excelData.get(count+4));
			manageSubscription.setRoad(excelData.get(count+5));
			manageSubscription.setPostCode(excelData.get(count+6));
			manageSubscription.setPlace(excelData.get(count+7));
			manageSubscription.setEmail(excelData.get(count+8));
			manageSubscription.setEmailLKW(excelData.get(count+11));
			manageSubscription.setCreatedDate(zonedDateTime);
			manageSubscription.setStatus(OutletStatus.NEW.toString());
			//manageSubscription.setTruckService(true);
			List<SubscriptionStatus> subScriptionStatusList = new ArrayList<SubscriptionStatus>();

			SubscriptionStatus subScriptionStatus = new SubscriptionStatus();
			if("X".equals(excelData.get(count+19))) {
				subScriptionStatus.setSubscribed(true);
			}else {
				subScriptionStatus.setSubscribed(false);
			}
			if("X".equals(excelData.get(count+20))) {
				subScriptionStatus.setEquipmentType(EquipmentType.FULL_EQUIPMENT);
			}else if("X".equals(excelData.get(count+21))){
				subScriptionStatus.setEquipmentType(EquipmentType.PARTIAL_EQUIPMENT);
			}else {
				subScriptionStatus.setEquipmentType(EquipmentType.NA);
			}
			subScriptionStatus.setCreatedDate(zonedDateTime);
			subScriptionStatus.setManageSubscription(manageSubscription);
			subScriptionStatusList.add(subScriptionStatus);
			manageSubscription.setSubscriptionStatus(subScriptionStatusList);

			subscriptionList.add(manageSubscription);
			count = count + noOfColumns;
			//System.out.println(count);    		
		}while(count<excelData.size());

		return subscriptionList;
	}

	public String saveExcelData(List<ManageSubscription> manageSubscriptions) {

		List<ManageSubscription> responseList =(List<ManageSubscription>) manageSubscriptionRepository.saveAll(manageSubscriptions);
		if(responseList==null && responseList.isEmpty()) {
			return "Failed";
		}
		return "Success";
	}

}

