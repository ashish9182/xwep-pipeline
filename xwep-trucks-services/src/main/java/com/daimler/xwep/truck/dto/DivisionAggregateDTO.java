package com.daimler.xwep.truck.dto;

public class DivisionAggregateDTO {
	
	private String division;
    private Long total;
    private Long fullyEquipped;
    private Long partiallyEquipped;
    private Long fullyEquippedWithoutSubscription;
    private Long partiallyEquippedWithoutSubscription;

    public DivisionAggregateDTO(String division, Long total, Long fullyEquipped, Long partiallyEquipped, Long fullyEquippedWithoutSubscription, Long partiallyEquippedWithoutSubscription) {
        this.division = division;
        this.total = total;
        this.fullyEquipped = fullyEquipped;
        this.partiallyEquipped = partiallyEquipped;
        this.fullyEquippedWithoutSubscription = fullyEquippedWithoutSubscription;
        this.partiallyEquippedWithoutSubscription = fullyEquippedWithoutSubscription;
    }

    public DivisionAggregateDTO() {
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getFullyEquipped() {
        return fullyEquipped;
    }

    public void setFullyEquipped(Long fullyEquipped) {
        this.fullyEquipped = fullyEquipped;
    }

    public Long getPartiallyEquipped() {
        return partiallyEquipped;
    }

    public void setPartiallyEquipped(Long partiallyEquipped) {
        this.partiallyEquipped = partiallyEquipped;
    }

    public Long getFullyEquippedWithoutSubscription() {
        return fullyEquippedWithoutSubscription;
    }

    public void setFullyEquippedWithoutSubscription(Long fullyEquippedWithoutSubscription) {
        this.fullyEquippedWithoutSubscription = fullyEquippedWithoutSubscription;
    }

    public Long getPartiallyEquippedWithoutSubscription() {
        return partiallyEquippedWithoutSubscription;
    }

    public void setPartiallyEquippedWithoutSubscription(Long partiallyEquippedWithoutSubscription) {
        this.partiallyEquippedWithoutSubscription = partiallyEquippedWithoutSubscription;
    }
}
