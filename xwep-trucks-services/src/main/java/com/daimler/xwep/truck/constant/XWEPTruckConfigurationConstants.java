package com.daimler.xwep.truck.constant;

public class XWEPTruckConfigurationConstants {
	
	public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";
    public static final String PACKAGE_NAME="com.daimler.xwep.truck";
    public static final String PROJECT_TITLE="XWEP TRuck Application";
    public static final String VERSION="1.0.0";
    public static final String SUCCESS="Success";
    public static final String FAILURE="Failure";
    
    protected static final String[][] headersGerman = { { "Nationalitäten-Kennzeichen", "Outlet ID", "Company ID", "Juristischer Name",
        "Führender Markencode", "Lieferadresse", "", "", "E-Mail",
        "Email Mercedes-Benz - Personenwagen - Kontakt für Werkstatteinrichtung",
        "Email Mercedes-Benz - Transporter - Kontakt für Werkstatteinrichtung",
        "Email Mercedes-Benz - LKW - Kontakt für Werkstatteinrichtung",
        "Email smart - Personenwagen - Kontakt für Werkstatteinrichtung", "Mercedes-Benz - Personenwagen",
        "", "", "Mercedes-Benz - Transporter", "", "", "Mercedes-Benz - LKW", "", "", "EQ- Personenwagen","" },
        { "", "", "", "", "", "Straße", "Postleitzahl", "Ort", "", "", "", "", "", "Abonnement",
                "Vollausstattung", "Teilausstattung", "Abonnement", "Vollausstattung", "Teilausstattung",
                "Abonnement", "Vollausstattung", "Teilausstattung","Abonnement", "Vollausstattung" }, };
    
    
    protected static final String[][] headersEnglish = { { "Nationality code", "Outlet ID", "Company ID", "Legal name",
        "Leading brand code", "Delivery address", "", "", "E-Mail",
        "Email Mercedes-Benz - Cars - Contact for workshop equipment",
        "Email Mercedes-Benz - Vans - Contact for workshop equipment",
        "Email Mercedes-Benz - Trucks - Contact for workshop equipment",
        "Email smart - Cars - Contact for workshop equipment", "Mercedes-Benz - Cars",
        "", "", "Mercedes-Benz - Vans", "", "", "Mercedes-Benz - Trucks", "", "", "EQ- passenger car","" },
        { "", "", "", "", "", "Street", "Postal code", "City", "", "", "", "", "", "Subscription",
                "Full equipment", "Partial equipment", "Subscription", "Full equipment", "Partial equipment",
                "Subscription", "Full equipment", "Partial equipment","Subscription", "Full equipment" }, };
    
    protected static final String[][] headersEnglishNew = { { "Nationality code", "Outlet ID", "Company ID", "Legal name",
        "Leading brand code", "Delivery address", "", "", "E-Mail",
        "Email Mercedes-Benz - Trucks - Contact for workshop equipment",
        "Mercedes-Benz - Trucks", "", ""},
        { "", "", "", "", "", "Street", "Postal code", "City", "", "", "Subscription",
                "Full equipment", "Partial equipment" }, };
    
    protected static final String[][] headersChina = { { "国家标识", "输出口 ID", "公司 ID", "合法名称",
        "主市场代码", "供货地址", "", "", "电子邮件",
        "梅赛德斯-奔驰电子邮件 - 乘用车 - 维修间装置联系方式",
        "梅赛德斯-奔驰电子邮件 - 轻型商用车 - 维修间装置联系方式",
        "梅赛德斯-奔驰电子邮件 - 载重车 - 维修间装置联系方式",
        "smart 电子邮件 - 乘用车 - 维修间装置联系方式", "梅赛德斯-奔驰 - 乘用车",
        "", "", "梅赛德斯-奔驰 - 乘用车", "", "", "梅赛德斯-奔驰 - 载重车", "", "", "EQ-乘用车","" },
        { "", "", "", "", "", "街道", "邮政编码", "地点", "", "", "", "", "", "订阅",
                "全装备", "部分装备", "订阅", "全装备", "部分装备",
                "订阅", "全装备", "部分装备","订阅", "全装备" }, };
    
    protected static final String[][] headersItaly = { { "Sigla nazionalità", "Outlet ID", "Company ID", "Nome giuridico",
        "Codice marchio principale", "Indirizzo di consegna", "", "", "E-Mail",
        "E-mail Mercedes-Benz - Autovetture - Contatto per attrezzature per officina",
        "E-mail Mercedes-Benz - Veicoli commerciali - Contatto per attrezzature per officina",
        "E-mail Mercedes-Benz - Autocarri - Contatto per attrezzature per officina",
        "E-mail smart - Autovetture - Contatto per attrezzature per officina", "Mercedes-Benz - Autovetture",
        "", "", "Mercedes-Benz - Veicoli commerciali", "", "", "Mercedes-Benz - Autocarri", "", "", "EQ- autovettura","" },
        { "", "", "", "", "", "Via", "CAP", "Località", "", "", "", "", "", "Abbonamento",
                "Equipaggiamento completo", "Equipaggiamento parziale", "Abbonamento", "Equipaggiamento completo", "Equipaggiamento parziale",
                "Abbonamento", "Equipaggiamento completo", "Equipaggiamento parziale","Abbonamento", "Equipaggiamento completo" }, };
    
    
    protected static final String[][] headersSpanish = { { "Código de nacionalidad", "Outlet ID", "Company ID", "Nombre jurídico",
        "Código de marca principal", "Dirección de entrega", "", "", "Correo electrónico",
        "Correo electrónico Mercedes-Benz - Turismos - Contacto para equipo de taller",
        "Correo electrónico Mercedes-Benz - Furgonetas - Contacto para equipo de taller",
        "Correo electrónico Mercedes-Benz - Camiones - Contacto para equipo de taller",
        "Correo electrónico smart - Turismos - Contacto para equipo de taller", "Mercedes-Benz - Turismos",
        "", "", "Mercedes-Benz - Furgonetas", "", "", "Mercedes-Benz - Camiones", "", "", "EQ- coche de pasajeros","" },
        { "", "", "", "", "", "Calle", "Código postal", "Lugar", "", "", "", "", "", "Suscripción",
                "Equipamiento completo", "Equipamiento parcial", "Suscripción", "Equipamiento completo", "Equipamiento parcial",
                "Suscripción", "Equipamiento completo", "Equipamiento parcial","Suscripción", "Equipamiento completo" }, };
 
    protected static final String[][] headersFrench = { { "Plaquette de nationalité", "Outlet ID", "Company ID", "Nom juridique",
        "Code de marque principal", "Adresse de livraison", "", "", "E-Mail",
        "E-mail Mercedes-Benz - Voitures particulières - Contact pour équipement d'atelier",
        "E-mail Mercedes-Benz - Utilitaires légers - Contact pour équipement d'atelier",
        "E-mail Mercedes-Benz - Camions - Contact pour équipement d'atelier",
        "E-mail smart - Voitures particulières - Contact pour équipement d'atelier", "Mercedes-Benz - Voitures particulières",
        "", "", "Mercedes-Benz - Utilitaires légers", "", "", "Mercedes-Benz - Camions", "", "", "Voiture de tourisme EQ","" },
        { "", "", "", "", "", "Rue", "Code postal", "Ville", "", "", "", "", "", "Abonnement",
                "Equipement complet", "équipement partiel", "Abonnement", "Equipement complet", "équipement partiel",
                "Abonnement", "Equipement complet", "équipement partiel","Abonnement", "Equipement complet" }, };
    private XWEPTruckConfigurationConstants() {
    }
    
    public static String[][] getHeaders(String language) {
        switch (language) {
        case "de":
            return headersGerman;
        case "en":
            return headersEnglish;
        case "es":
            return headersSpanish;
        case "fr":
            return headersFrench;
        case "it":
            return headersItaly;
        case "zh":
            return headersChina;
        case "ey":
        	return headersEnglishNew;
        default:
            return headersGerman;
        }
    }
}
