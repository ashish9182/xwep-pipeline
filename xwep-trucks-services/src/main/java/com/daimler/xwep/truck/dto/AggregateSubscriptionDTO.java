package com.daimler.xwep.truck.dto;

import java.util.List;

public class AggregateSubscriptionDTO {
	
	private Long total;
    private Long fullyEquipped;
    private Long partiallyEquipped;
    private Long fullyEquippedWithoutSubscription;
    private Long partiallyEquippedWithoutSubscription;
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public Long getFullyEquipped() {
		return fullyEquipped;
	}
	public void setFullyEquipped(Long fullyEquipped) {
		this.fullyEquipped = fullyEquipped;
	}
	public Long getPartiallyEquipped() {
		return partiallyEquipped;
	}
	public void setPartiallyEquipped(Long partiallyEquipped) {
		this.partiallyEquipped = partiallyEquipped;
	}
	public Long getFullyEquippedWithoutSubscription() {
		return fullyEquippedWithoutSubscription;
	}
	public void setFullyEquippedWithoutSubscription(Long fullyEquippedWithoutSubscription) {
		this.fullyEquippedWithoutSubscription = fullyEquippedWithoutSubscription;
	}
	public Long getPartiallyEquippedWithoutSubscription() {
		return partiallyEquippedWithoutSubscription;
	}
	public void setPartiallyEquippedWithoutSubscription(Long partiallyEquippedWithoutSubscription) {
		this.partiallyEquippedWithoutSubscription = partiallyEquippedWithoutSubscription;
	}

	    


}
