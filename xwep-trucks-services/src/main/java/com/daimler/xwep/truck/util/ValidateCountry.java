package com.daimler.xwep.truck.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.daimler.xwep.truck.dto.CountryCode;
import com.daimler.xwep.truck.dto.UserInfo;

public class ValidateCountry {
	
	 private static final Logger logger = LoggerFactory.getLogger(ValidateCountry.class);
	   
	    public static Boolean validateNationality(String nationality, HttpServletRequest request){
	        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
	        List<CountryCode> countryCodes =  userInfo.getCountryCodes();
	        for(CountryCode countryCode : countryCodes){
	            if(countryCode.getCountryName().equalsIgnoreCase(nationality)){
	                return true;
	            }
	        }
	        return false;       
	    }
	    
	    public static Boolean validatePermission(String entitlement, HttpServletRequest request) {
	        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
	        String entitlementGroup = userInfo.getEntitlement_group();
	        if (entitlementGroup.contains(entitlement))
	            return true;
	        return false;
	    }

}
