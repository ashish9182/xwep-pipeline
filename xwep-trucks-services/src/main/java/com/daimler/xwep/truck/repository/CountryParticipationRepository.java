package com.daimler.xwep.truck.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.daimler.xwep.truck.entity.CountryParticipation;

@Repository
public interface CountryParticipationRepository extends JpaRepository<CountryParticipation, Integer>{
    
    CountryParticipation findByIsoCodeIgnoreCase(String code);

}
