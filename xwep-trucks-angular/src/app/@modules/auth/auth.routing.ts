import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RedirectComponent } from './components/redirect/redirect.component';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'redirect', component: RedirectComponent },
    { path: 'logout', component: LogoutComponent },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);