import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { RedirectComponent } from './components/redirect/redirect.component';
import { LogoutComponent } from './components/logout/logout.component';

import { routing } from './auth.routing';

@NgModule({
  declarations: [LoginComponent, RedirectComponent, LogoutComponent],
  imports: [
    CommonModule,
    SharedModule,
    routing
  ]
})
export class AuthModule { }
