import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { RoleService, LoaderService } from '@core/services';
import { of, Observable, observable } from 'rxjs';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let RoleServiceSpy: jasmine.SpyObj<RoleService>;
  let LoaderServiceSpy: jasmine.SpyObj<LoaderService>;

  beforeEach(async () => {

    const RoleSpy = jasmine.createSpyObj('RoleService', [
      'getUserRole',
    ]);

    // const LoaderSpy = jasmine.createSpyObj('LoaderService', [
    //   'loaderStatus$',
    //   '_loaderStatusSource'
    // ]);

    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers : [
        { provide: RoleService, useValue: RoleSpy },
        { provide: LoaderService, useValue: {loaderStatus$ : of()} },
      ]
    })
    .compileComponents();

    RoleServiceSpy = TestBed.inject(RoleService) as jasmine.SpyObj<RoleService>;

    LoaderServiceSpy = TestBed.inject(LoaderService) as jasmine.SpyObj<
      LoaderService
    >;

    // LoaderServiceSpy.loaderStatus$.subscribe((e)=>{});

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toogle the value of side nav if called', () => {
    let sideNav = component.sideNavOpen;
    component.toggleSideNav();
    expect(component.sideNavOpen).toBe(!sideNav);
  });

});
