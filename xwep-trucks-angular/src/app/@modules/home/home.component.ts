import { Component, OnInit } from '@angular/core';
import { RoleService, LoaderService } from '@core/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  sideNavOpen: boolean = false;
  userRole: string;
  showLoader: boolean;

  constructor(
    private _roleService: RoleService,
    private _loaderService: LoaderService
  ) {
    this.userRole = this._roleService.getUserRole();
  }

  ngOnInit(): void {
    this._loaderService.loaderStatus$.subscribe((status: boolean) => {
      this.showLoader = status;
    })
  }

  toggleSideNav() {
    this.sideNavOpen = !this.sideNavOpen;
    window.dispatchEvent(new Event('resize'));
  }

}
