import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { RoutePaths } from '@core/constants'
import { HomeComponent } from './home.component';
import { ErrorPageComponent } from '@shared/components/error-page/error-page.component';

import { HqGuardGuard, MpcGuardGuard, DealerGuardGuard } from '@core/guards';

export const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                redirectTo: RoutePaths.dashboard
            },
            {
                path: RoutePaths.dashboard,
                loadChildren: () => import('@modules/dashboard/dashboard.module').then(mod => mod.DashboardModule),
            },
            {
                path: RoutePaths.subscriptions,
                loadChildren: () => import('@modules/subscriptions/subscriptions.module').then(mod => mod.SubscriptionsModule),
                canActivate: [MpcGuardGuard]
            },
            {
                path: RoutePaths.viewAllSubscriptions,
                loadChildren: () => import('@modules/all-subscriptions/all-subscriptions.module').then(mod => mod.AllSubscriptionsModule),
                canActivate: [HqGuardGuard]

            },
            {
                path: RoutePaths.mySubscriptions,
                loadChildren: () => import('@modules/my-subscriptions/my-subscriptions.module').then(mod => mod.MySubscriptionsModule),
                canActivate: [DealerGuardGuard]
            },
            {
                path: 'error',
                component: ErrorPageComponent,
            },

        ]
    },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);