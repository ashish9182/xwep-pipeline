import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoleService } from '@core/services';
import { RoutePaths } from '@core/constants'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private _roleService: RoleService, private router: Router) { }

  userRole: string = "";

  ngOnInit(): void {
    this.userRole = this._roleService.getUserRole().toLowerCase();
    this.navigateBasedOnUser(this.userRole)
  }

  navigateBasedOnUser(user) {
    if (user === 'hq') {
      this.router.navigate([RoutePaths.viewAllSubscriptions])
    } else if (user === 'mpc') {
      this.router.navigate([RoutePaths.subscriptions])
    } else if (user === 'dealer') {
      this.router.navigate([RoutePaths.mySubscriptions])
    }
  }
}
