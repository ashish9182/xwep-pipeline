import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { routing } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';




@NgModule({
  declarations: [
  DashboardComponent],
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    routing
  ],
  exports: [
  ]
})
export class DashboardModule { }
