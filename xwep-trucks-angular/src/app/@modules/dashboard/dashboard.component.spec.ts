import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { RoleService } from '@core/services';
import { Router } from "@angular/router";

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let RouterSpy: Router;

  class RoleStub {
    _role = "dealer";
    getUserRole() { return this._role; }
    getUserData () { return {firstName : 'test'}}

  }

  
  class RouterStub {
    navigate() { }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      providers :[
        { provide: RoleService, useClass: RoleStub },
        { provide: Router, useClass: RouterStub },
      ]
    })
    .compileComponents();
    RouterSpy = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate if user is hq', () => {
    spyOn(RouterSpy, 'navigate');
    component.navigateBasedOnUser('hq')
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  it('should navigate if user is mpc', () => {
    spyOn(RouterSpy, 'navigate');
    component.navigateBasedOnUser('mpc')
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  it('should navigate if user is dealer', () => {
    spyOn(RouterSpy, 'navigate');
    component.navigateBasedOnUser('dealer')
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  
});
