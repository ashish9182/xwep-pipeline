import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { MySubscriptionsComponent } from './my-subscriptions.component';
import { RoutePaths } from '@core/constants';

export const routes: Routes = [
    {
        path: '',
        component: MySubscriptionsComponent,
    },

];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);