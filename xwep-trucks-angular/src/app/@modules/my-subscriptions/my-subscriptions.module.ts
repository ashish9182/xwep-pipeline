import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MySubscriptionsComponent } from './my-subscriptions.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { routing } from './my-subscriptions.routing';



@NgModule({
  declarations: [MySubscriptionsComponent],
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    routing
  ]
})
export class MySubscriptionsModule { }
