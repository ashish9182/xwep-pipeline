import { element } from 'protractor';
import { LoaderService } from '@core/services';
import { SubscriptionsService } from './../subscriptions/subscriptions.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.scss'],
})
export class MySubscriptionsComponent implements OnInit {
  dealerSubStatus: any = {};
  dealerDetails : any = {}
  loadingError: boolean = false;

  constructor(
    private _subscriptionService: SubscriptionsService,
    private _loaderService: LoaderService,

  ) { }

  ngOnInit(): void {
    this.fetchSubscriptionStatus();
  }

  refreshData(){
    this.fetchSubscriptionStatus();
  }

  fetchSubscriptionStatus() {
    this._loaderService.updateLoaderStatus(true);

    const mockResponse =

      { "subscriptionStatusList": [{ "id": 1441, "equipmentType": "PARTIAL_EQUIPMENT", "divisionName": "PASSENGER", "subscribed": true }, { "id": 1442, "equipmentType": "PARTIAL_EQUIPMENT", "divisionName": "VAN", "subscribed": true }, { "id": 1443, "equipmentType": "NA", "divisionName": "TRUCK", "subscribed": false }, { "id": 1444, "equipmentType": "NA", "divisionName": "EQ", "subscribed": false }], "dealerBasicInfoVO": { "nationality": "DE", "legalName": "Autohaus Jackwerth KG", "brandCode": "01715", "road": "Herborner Straße 2", "postCode": "35649", "place": "Bischoffen", "email": "info@autohaus-jackwerth.de" } }

    this._subscriptionService.getDealerSubscriptionStatus().subscribe(
      (data) => {
        this._loaderService.updateLoaderStatus(false);
        this.dealerSubStatus = this.processData(data.subscriptionStatusList);
        this.dealerDetails =  data.dealerBasicInfoVO;
        // this.dealerSubStatus = this.processData(mockResponse);
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
        // this.dealerSubStatus = this.processData(mockResponse.subscriptionStatusList);
        // this.dealerDetails =  mockResponse.dealerBasicInfoVO ;
        this.loadingError = true;
      }
    );
  }


  processData(data) {
    const returnData = {};
    data?.forEach((element) => {
      returnData[element.divisionName] = element;
    });
    return returnData;
  }
}
