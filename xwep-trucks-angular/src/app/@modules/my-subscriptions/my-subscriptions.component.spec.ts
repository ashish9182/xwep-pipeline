import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MySubscriptionsComponent } from './my-subscriptions.component';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { SubscriptionsService } from './../subscriptions/subscriptions.service';
import { LoaderService } from '@core/services';
import { of, } from 'rxjs';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('MySubscriptionsComponent', () => {
  let component: MySubscriptionsComponent;
  let fixture: ComponentFixture<MySubscriptionsComponent>;
  let TranslationServiceSpy: jasmine.SpyObj<TranslationService>;
  let SubscriptionsServiceSpy: jasmine.SpyObj<SubscriptionsService>;


  beforeEach(async () => {
    const SubscriptionsSpy = jasmine.createSpyObj('SubscriptionsService', [
      'getSubscriptionsAggregateChartData',
      'addNewSubscription',
      'deleteSubscription',
      'updateSubscription',
      'getDealerSubscriptionStatus',
      'fetchSubscriptionStatus'
    ]);

    const LoaderSpy = jasmine.createSpyObj('LoaderService', [
      'updateLoaderStatus',
    ]);


    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);

    await TestBed.configureTestingModule({
      declarations: [MySubscriptionsComponent],
      providers: [
        { provide: TranslationService, useValue: TranslationSpy },
        { provide: SubscriptionsService, useValue: SubscriptionsSpy },
        { provide: LoaderService, useValue: LoaderSpy },

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [MatTooltipModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader,
        },
      })],

    })
      .compileComponents();

    TranslationServiceSpy = TestBed.inject(TranslationService) as jasmine.SpyObj<TranslationService>;

    SubscriptionsServiceSpy = TestBed.inject(
      SubscriptionsService
    ) as jasmine.SpyObj<SubscriptionsService>;

    SubscriptionsServiceSpy.getDealerSubscriptionStatus.and.callFake(() => of([{
      id: 18,
      equipmentType: 'NA',
      divisionName: 'VAN',
      subscribed: false,
    },]));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MySubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
