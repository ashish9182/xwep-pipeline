import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';

import { SubscriptionsComponent } from './subscriptions.component';

import { DialogService, LoaderService, ApiService, RoleService, HttpService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { SubscriptionsService } from './subscriptions.service';
import { of, BehaviorSubject } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { MdePopoverModule } from '@material-extended/mde';
import { Router, ActivatedRoute } from '@angular/router';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { MatTooltipModule } from '@angular/material/tooltip';

@Pipe({ name: 'translate' })
class MockTranslatePipe implements PipeTransform {
  transform(value: any): any {
    return value;
  }
}

describe('SubscriptionsComponent', () => {
  let component: SubscriptionsComponent<any>;
  let fixture: ComponentFixture<SubscriptionsComponent<any>>;
  let ApiServiceSpy: jasmine.SpyObj<ApiService>;
  let LoaderServiceSpy: jasmine.SpyObj<LoaderService>;
  let ToastrServiceSpy: jasmine.SpyObj<ToastrService>;
  let DialogServiceSpy: jasmine.SpyObj<DialogService<any>>;
  let SubscriptionsServiceSpy: jasmine.SpyObj<SubscriptionsService>;
  let RoleServiceSpy: jasmine.SpyObj<RoleService>;


  class RouteStub {
    url = "test?role=mpc";
    navigate() { }
    events = of()
  }

  class RoleStub {
    _role = "test";
    countryEmmitter = new BehaviorSubject('');
    userData = { firstName: "Test", countryCode: "D", MPC_Type: 'EU' };
    getUserRole() { return this._role; }
    getUserData() { return this.userData };
    getCountryList() { return [] }
    getAuthorizationList() { return ['VIEW_OWN_SUBSCRIPTION'] }
  }

  beforeEach(async () => {
    const ApiSpy = jasmine.createSpyObj('ApiService', [
      'getSubscriptionsByNationality',
      'dealerDataSynch'
    ]);

    const LoaderSpy = jasmine.createSpyObj('LoaderService', [
      'updateLoaderStatus',
    ]);

    const ToastSpy = jasmine.createSpyObj('ToastrService', [
      'success',
      'error',
      'clear'
    ]);

    const DialogSpy = jasmine.createSpyObj('DialogService', [
      'addNewSubscription',
      'editSubscription',
      'confirmGssnSync'
    ]);

    const SubscriptionsSpy = jasmine.createSpyObj('SubscriptionsService', [
      'getSubscriptionsAggregateChartData',
      'addNewSubscription',
      'deleteSubscription',
      'updateSubscription',
      'downloadMPCDealersData',
    ]);

    await TestBed.configureTestingModule({
      declarations: [SubscriptionsComponent, MockTranslatePipe],
      providers: [
        { provide: ApiService, useValue: ApiSpy },
        { provide: LoaderService, useValue: LoaderSpy },
        { provide: ToastrService, useValue: ToastSpy },
        { provide: DialogService, useValue: DialogSpy },
        { provide: SubscriptionsService, useValue: SubscriptionsSpy },
        { provide: RoleService, useClass: RoleStub },
        { provide: HttpService, useClass: HttpService },
        { provide: Router, useClass: RouteStub },

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, MatTooltipModule, MdePopoverModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ],
    }).compileComponents();

    ApiServiceSpy = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;

    LoaderServiceSpy = TestBed.inject(
      LoaderService
    ) as jasmine.SpyObj<LoaderService>;

    ToastrServiceSpy = TestBed.inject(
      ToastrService
    ) as jasmine.SpyObj<ToastrService>;

    DialogServiceSpy = TestBed.inject(DialogService) as jasmine.SpyObj<
      DialogService<any>
    >;

    SubscriptionsServiceSpy = TestBed.inject(
      SubscriptionsService
    ) as jasmine.SpyObj<SubscriptionsService>;

    ApiServiceSpy.getSubscriptionsByNationality.and.callFake(() => of([]));
    RoleServiceSpy = TestBed.inject(RoleService) as jasmine.SpyObj<RoleService>;

    SubscriptionsServiceSpy.getSubscriptionsAggregateChartData.and.callFake(
      () => of({
        dealerCount: 1,
        aggregateData: []
      })
    );
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    RoleServiceSpy.countryEmmitter.next("DE");
    expect(component).toBeTruthy();
  });

  it('should downloadAsCSv call SubscriptionsSpy.downloadMPCDealersData', () => {
    component.downloadAsCSV();
    expect(SubscriptionsServiceSpy.downloadMPCDealersData.calls.count()).toBe(
      1
    );
  });

  it('Testing fetchSubscriptionData', fakeAsync(() => {
    ApiServiceSpy.getSubscriptionsByNationality.and.callFake(() => of([]));
    component.fetchSubscriptionData();
    // spyOn(component, 'fetchSubscriptionData');

    // expect(component.fetchSubscriptionData).toHaveBeenCalled();
    expect(ApiServiceSpy.getSubscriptionsByNationality).toHaveBeenCalled();
    // expect(LoaderServiceSpy.updateLoaderStatus).toHaveBeenCalled();
  }));


  it('should calling editSubscription function call the relevant services', () => {
    DialogServiceSpy.editSubscription.and.callFake(() =>
      of({ type: 'delete', payload: 1 })
    );
    SubscriptionsServiceSpy.deleteSubscription.and.callFake(() => of([]));
    spyOn(component, 'updateFilteredData');
    component.editSubscription({});
    expect(DialogServiceSpy.editSubscription).toHaveBeenCalled();
    expect(SubscriptionsServiceSpy.deleteSubscription).toHaveBeenCalled();
    expect(component.updateFilteredData).toHaveBeenCalled();
  });

  it('should calling editSubscription function call the relevant services - else branch', () => {
    DialogServiceSpy.editSubscription.and.callFake(() =>
      of({ type: 'edit', payload: 1 })
    );
    SubscriptionsServiceSpy.updateSubscription.and.callFake(() => of({}));
    component.editSubscription({});
    expect(DialogServiceSpy.editSubscription).toHaveBeenCalled();
    expect(SubscriptionsServiceSpy.updateSubscription).toHaveBeenCalled();
  });

  it('Check if filtered Sub data filters data based on string searched', () => {
    component.filterKeyword = 'Test1';
    component.subData = [{
      id: 1,
      brandCode: 'string',
      uuid: "",
      legalName: 'Test',
      companyId: 'string',
      email: 'string',
      emailLKW: 'string',
      nationality: 'string',
      outletId: 'string',
      place: 'string',
      postCode: 'string',
      road: 'string',
      subscriptionStatus: [],
      status : ''
    }];
    fixture.detectChanges();
    component.updateFilteredData();
    expect(component.filteredSubData.length).toBe(0)
  });

  it('should gssnDataSync call toasterService success', () => {
    DialogServiceSpy.confirmGssnSync.and.callFake(() => of(true));
    ApiServiceSpy.dealerDataSynch.and.callFake(() => of({}));
    component.gssnDataSync();
    fixture.detectChanges();
    expect(ToastrServiceSpy.success).toHaveBeenCalled();
  });


});
