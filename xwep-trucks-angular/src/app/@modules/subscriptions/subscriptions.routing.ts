import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { SubscriptionsComponent } from './subscriptions.component';
import { RoutePaths } from '@core/constants';

export const routes: Routes = [
    {
        path: '',
        component: SubscriptionsComponent,
    },

];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);