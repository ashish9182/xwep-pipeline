import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionsComponent } from './subscriptions.component';
import { SharedModule } from '@shared/shared.module';
import { routing } from './subscriptions.routing';
import { CoreModule } from '@core/core.module';


@NgModule({
  declarations: [SubscriptionsComponent],
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    routing
  ],
  exports: [
  ]
})
export class SubscriptionsModule { }
