import { element } from 'protractor';
import { filter } from '@core/utils';
import { Injectable } from '@angular/core';
import { ApiService, LoaderService } from '@core/services';
// import { BehaviorSubject, of, throwError } from 'rxjs';
import {
  // Response,
  SubscriptionsAggregateOverview,
  SubscriptionsAggregate,
  SubscriptionsAggregateNew,
} from '@core/interfaces';
import { catchError, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { API_PATHS, SUBSCRIPTION_OVERVIEW_CHART } from '@core/constants';
import { saveAs } from 'file-saver';
import { throwError } from 'rxjs';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionsService {
  // private _subscriptionsDataSource = new BehaviorSubject([]);
  // subscriptionsData$ = this._subscriptionsDataSource.asObservable();

  constructor(
    private _apiService: ApiService,
    private _toast: ToastrService,
    private _loaderService: LoaderService,
    private translate: TranslateService
  ) { }

  getAllSubscriptionsData() {
    return this._apiService.getAllSubscriptionsData().pipe(
      map((res: any) => res),
      catchError((err) => {
        return err;
      })
    );
  }

  getPaginatedSubscriptionsData() { }

  addNewSubscription(data) {
    return new Promise((resolve, reject) => {
      this._apiService.createNewSubscription(data).subscribe(() => {
        //   resolve();
        // }),
        //   () => {
        //     reject();
        //   };
      });
    });
  }

  deleteSubscription(id) {
    return this._apiService.deleteSubscription(id).pipe(
      map((res: any) => {
        this._toast.success(this.translate.instant('subscriptions.DELETED'));
        return res;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  updateSubscription(data) {
    this._loaderService.updateLoaderStatus(true);
    return this._apiService.updateSubscription(data).pipe(
      map((res: any) => {
        this._loaderService.updateLoaderStatus(false);
        this._toast.success(
          this.translate.instant('subscriptions.UPDATE_SUCCESS')
        );
        return res;
      }),
      catchError((err) => {
        this._loaderService.updateLoaderStatus(false);
        return err;
      })
    );
  }

  getAllSubscriptionsInfo() {
    return this._apiService.getAllSubscriptionsInfo().pipe(
      map((res: any) => {
        return res.content;
      }),
      catchError((err) => {
        return err;
      })
    );
  }

  /**
   * Subscriptions Aggregate Visualisation Data
   */
   getSubscriptionsAggregateChartData(code?) {
    return this._apiService.getSubscriptionsAggregate(code).pipe(
      map((res: any) => {
        return this.processAggregateTruckData(res);
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  processAggregateTruckData(data: SubscriptionsAggregateNew) {
    const formattedData = { dealerCount: data.total, aggregateData: {} };
      formattedData.aggregateData['truck'] = {
        division: 'TRUCK',
        total: data.total,
        chartData: SUBSCRIPTION_OVERVIEW_CHART.entries.map((entry) => {
          if (entry === 'noSubscription') {
            return {
              name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
              value: data.total - (data.fullyEquipped + data.fullyEquippedWithoutSubscription +
                data.partiallyEquipped + data.partiallyEquippedWithoutSubscription),
            };
          }
          return {
            name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
            value: data[entry],
          };
        }),
        colorData: SUBSCRIPTION_OVERVIEW_CHART.entries.map((entry) => {
          return {
            name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
            value: SUBSCRIPTION_OVERVIEW_CHART.colors[entry],
          };
        }),
      };
      console.log(formattedData)
    return formattedData;

  }

  processAggregateData(data: any ){
    const formattedData = { dealerCount: data.dealerCount, aggregateData: {} };
    data.divisionAggregateList.forEach((el: SubscriptionsAggregate) => {
      formattedData.aggregateData[el.division.toLowerCase()] = {
        division: el.division,
        total: el.total,
        chartData: SUBSCRIPTION_OVERVIEW_CHART.entries.map((entry) => {
          if (entry === 'noSubscription') {
            return {
              name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
              value: el.total - (el.fullyEquipped + el.fullyEquippedWithoutSubscription +
                el.partiallyEquipped + el.partiallyEquippedWithoutSubscription),
            };
          }
          return {
            name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
            value: el[entry],
          };
        }),
        colorData: SUBSCRIPTION_OVERVIEW_CHART.entries.map((entry) => {
          return {
            name: SUBSCRIPTION_OVERVIEW_CHART.labels[entry],
            value: SUBSCRIPTION_OVERVIEW_CHART.colors[entry],
          };
        }),
      };
    });
    formattedData.aggregateData['eq'].chartData.splice(4, 1);
    formattedData.aggregateData['eq'].colorData.splice(4, 1);
    return formattedData;
  }

  downloadMPCDealersData(code) {
    this._toast.success(
      this.translate.instant('subscriptions.DOWNLOAD_MESSAGE')
    );
    // const path = API_PATHS.downloadMPCDealers;
    // const apiBaseUrl = env.apiBaseUrl;
    // const url = `${apiBaseUrl}/${path}?nationality=${code}`;
    // const fileName = `SpecialToolStatisticreport_MPC_${code}_${moment().toISOString()}.xlsx`;
    // const link = document.createElement('a');
    // link.download = fileName;
    // link.href = url;
    // document.body.appendChild(link);
    // link.click();
    // document.body.removeChild(link);
    this._apiService.getMPCDealerDataCSV({ code: code, language: this.translate.currentLang }).subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'text/csv' });
        const fileName = `SpecialToolStatisticreport_MPC_${code}_${moment().toISOString()}.xlsx`;
        saveAs(blob, fileName);
      },
      (err) => {
        return err;
      }
    );
  }

  downloadHQDealersData() {
    this._toast.success(
      this.translate.instant('subscriptions.DOWNLOAD_MESSAGE')
    );
    // const path = API_PATHS.downloadHQDealers;
    // const apiBaseUrl = env.apiBaseUrl;
    // const url = `${apiBaseUrl}/${path}`;
    // const fileName = `SpecialToolStatisticreport_HQ_${moment().toISOString()}.xlsx`;
    // const link = document.createElement('a');
    // link.download = fileName;
    // link.href = url;
    // document.body.appendChild(link);
    // link.click();
    // document.body.removeChild(link);
    this._apiService.getHQDealersDataCSV(this.translate.currentLang).subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'text/csv' });
        const fileName = `SpecialToolStatisticreport_HQ_${moment().toISOString()}.xlsx`;
        saveAs(blob, fileName);
      },
      (err) => {
        return err;
      }
    );
  }

  /**
   * Dealer based sub status
   */
  getDealerSubscriptionStatus() {
    return this._apiService.getDealerSubscriptionStatus().pipe(
      map((res: any) => {
        return res;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }
}
