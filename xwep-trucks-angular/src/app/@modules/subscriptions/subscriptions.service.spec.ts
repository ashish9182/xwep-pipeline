import { TestBed } from '@angular/core/testing';

import { SubscriptionsService } from './subscriptions.service';
import { LoaderService, ApiService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';

describe('SubscriptionsService', () => {
  let service: SubscriptionsService;
  let ApiServiceSpy: jasmine.SpyObj<ApiService>;
  let ToastrServiceSpy: jasmine.SpyObj<ToastrService>;
  let LoaderServiceSpy: jasmine.SpyObj<LoaderService>;

  beforeEach(() => {
    const ApiSpy = jasmine.createSpyObj('ApiService', [
      'getAllSubscriptionsData',
      'createNewSubscription',
      'deleteSubscription',
      'updateSubscription',
      'getAllSubscriptionsInfo',
      'getSubscriptionsAggregate',
      'getMPCDealerDataCSV',
      'getHQDealersDataCSV',
      'updateSubscriptionStatus',
      'getDealerSubscriptionStatus'
    ]);

    const ToastSpy = jasmine.createSpyObj('ToastrService', [
      'success',
      'error',
    ]);

    const LoaderSpy = jasmine.createSpyObj('LoaderService', [
      'updateLoaderStatus',
    ]);

    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useValue: ApiSpy },
        { provide: ToastrService, useValue: ToastSpy },
        { provide: LoaderService, useValue: LoaderSpy },
      ],
      imports: [
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ],
    });
    ApiServiceSpy = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;

    LoaderServiceSpy = TestBed.inject(LoaderService) as jasmine.SpyObj<
      LoaderService
    >;

    ToastrServiceSpy = TestBed.inject(ToastrService) as jasmine.SpyObj<
      ToastrService
    >;
    service = TestBed.inject(SubscriptionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAllSubscriptionsData should call apiservice.getAllSubscriptionsData: success case', () => {
    ApiServiceSpy.getAllSubscriptionsData.and.callFake(() => of([]));
    service.getAllSubscriptionsData();
    expect(ApiServiceSpy.getAllSubscriptionsData).toHaveBeenCalled();
  });

  it('addNewSubscription should call apiservice.createNewSubscription', () => {
    ApiServiceSpy.createNewSubscription.and.callFake(() => of({}));
    service.addNewSubscription('data');
    expect(ApiServiceSpy.createNewSubscription).toHaveBeenCalled();
  });

  it('deleteSubscription should call apiservice.deleteSubscription', () => {
    ApiServiceSpy.deleteSubscription.and.callFake(() => of({}));
    service.deleteSubscription('data');
    expect(ApiServiceSpy.deleteSubscription).toHaveBeenCalled();
  });

  it('updateSubscription should call apiservice.updateSubscription', () => {
    ApiServiceSpy.updateSubscription.and.callFake(() => of({}));
    service.updateSubscription('data');
    expect(ApiServiceSpy.updateSubscription).toHaveBeenCalled();
  });

  it('getAllSubscriptionsInfo should call apiservice.getAllSubscriptionsInfo', () => {
    ApiServiceSpy.getAllSubscriptionsInfo.and.callFake(() => of({}));
    service.getAllSubscriptionsInfo();
    expect(ApiServiceSpy.getAllSubscriptionsInfo).toHaveBeenCalled();
  });

  it('getSubscriptionsAggregateChartData should call apiservice.getSubscriptionsAggregate', () => {
    ApiServiceSpy.getSubscriptionsAggregate.and.callFake(() => of({}));
    service.getSubscriptionsAggregateChartData();
    expect(ApiServiceSpy.getSubscriptionsAggregate).toHaveBeenCalled();
  });

  it('downloadMPCDealersData should call apiservice.getMPCDealerDataCSV', () => {
    ApiServiceSpy.getMPCDealerDataCSV.and.callFake(() => of({}));
    service.downloadMPCDealersData('data');
    expect(ApiServiceSpy.getMPCDealerDataCSV).toHaveBeenCalled();
    // expect(ToastrServiceSpy.success).toHaveBeenCalled();
  });

  it('downloadHQDealersData should call apiservice.getHQDealersDataCSV', () => {
    ApiServiceSpy.getHQDealersDataCSV.and.callFake(() => of({}));
    service.downloadHQDealersData();
    expect(ApiServiceSpy.getHQDealersDataCSV).toHaveBeenCalled();
    // expect(ToastrServiceSpy.success).toHaveBeenCalled();
  });

  it('should process response after api to processAggregateData', () => {
    let data = {
      dealerCount: 1,
      divisionAggregateList: [
        {
          division: "VAN",
          fullyEquipped: 348,
          fullyEquippedWithoutSubscription: 62,
          partiallyEquipped: 490,
          partiallyEquippedWithoutSubscription: 2,
          total: 902
        },
        {
          division: "EQ",
          fullyEquipped: 348,
          fullyEquippedWithoutSubscription: 62,
          partiallyEquipped: 490,
          partiallyEquippedWithoutSubscription: 2,
          total: 902
        },
        {
          division: "TRUCK",
          fullyEquipped: 348,
          fullyEquippedWithoutSubscription: 62,
          partiallyEquipped: 490,
          partiallyEquippedWithoutSubscription: 2,
          total: 902
        },
        {
          division: "PASSENGER",
          fullyEquipped: 348,
          fullyEquippedWithoutSubscription: 62,
          partiallyEquipped: 490,
          partiallyEquippedWithoutSubscription: 2,
          total: 902
        }
      ]
    }
    let resp = service.processAggregateData(data);
    expect(resp.dealerCount).toBe(1);
    expect(resp.hasOwnProperty('aggregateData')).toBe(true);
  })

  // it('should call updateSubscriptionStatus in apiservice when updateSubscriptionStatus is called', () => {
  //   ApiServiceSpy.updateSubscriptionStatus.and.callFake(() => of({}));
  //   service.updateSubscriptionStatus('data');
  //   expect(ApiServiceSpy.updateSubscriptionStatus).toHaveBeenCalled();
  // })

  it('should call getDealerSubscriptionStatus in apiservice when getDealerSubscriptionStatus is called', () => {
    ApiServiceSpy.getDealerSubscriptionStatus.and.callFake(() => of({}));
    service.getDealerSubscriptionStatus();
    expect(ApiServiceSpy.getDealerSubscriptionStatus).toHaveBeenCalled();
  })


});
