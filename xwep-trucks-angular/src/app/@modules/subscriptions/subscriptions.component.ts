import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { SubscriptionData } from '@core/interfaces';
import { TableConfig } from './subscriptions.config';
import { mapArrayToObject, get } from '@core/utils';
import {
  DialogService,
  LoaderService,
  ApiService,
  RoleService,
} from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { SubscriptionsService } from './subscriptions.service';
import { filter, includes, groupby } from '@core/utils';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SubscriptionsComponent<T> implements OnInit {
  constructor(
    private _subscriptionsService: SubscriptionsService,
    private _dialogService: DialogService<T>,
    private _toastr: ToastrService,
    private _loaderService: LoaderService,
    private _apiService: ApiService,
    private _subscriptionService: SubscriptionsService,
    private _roleService: RoleService,
    private _router: Router,
    private _translateService: TranslateService
  ) { }

  countryCode: string;

  subData: SubscriptionData[] = [];
  filteredSubData: SubscriptionData[] = [];

  displayedColumns: string[][] = TableConfig.columns;
  columnConfig: any = TableConfig.columnConfig;
  columnKeys: any = TableConfig.keys;
  cellSelectors: any = TableConfig.selectors;
  filterKeyword: string = '';

  overviewData: any;

  initialSubStatus: any = {
    truck: { subscribed: false, equipmentType: 'NA' },
  };

  lastSyncDate: any;
  nextSyncDate: any;
  isSyncInProgress: boolean = false;

  getValueIn = get;
  gssnSyncDates: any = {};
  authList: any = [];
  currentFilters = [];

  ngOnInit(): void {
    if (
      !this._roleService
        .getAuthorizationList()
        .includes('VIEW_OWN_SUBSCRIPTION')
    ) {
      this._router.navigate(['/error']);
    } else {
      this.authList = this._roleService.getAuthorizationList();
      this.gssnSyncDates = this._roleService.getUserData()?.gssnSyncDates || {};
      this.setCountry();
    }
  }

  setCountry() {
    this._roleService.countryEmmitter.subscribe((country: string) => {
      this.filterKeyword = '';
      if (country?.length) {
        this.countryCode = country;
        this.fetchSubscriptionData();
        this.fetchSubscriptionsAggregate();
      }
      this.processGssnSyncDates(country);
    });
  }

  processGssnSyncDates(country) {
    if (country?.length) {
      country = country.toLowerCase();
      let data = this.gssnSyncDates[country];
      this.lastSyncDate = data?.lastSyncDate
        ? moment(data.lastSyncDate).toDate()
        : null;
      this.nextSyncDate = data?.nextSyncDate
        ? moment(data.nextSyncDate).toDate()
        : null;
    }
  }

  fetchSubscriptionData() {
    this._loaderService.updateLoaderStatus(true);
    this._apiService.getSubscriptionsByNationality(this.countryCode).subscribe(
      (response) => {
        let data = this.groupDataBasedOnStatus(response);
        this.processSubscriptionData(data);
        this._loaderService.updateLoaderStatus(false);
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
      }
    );
  }

  groupDataBasedOnStatus(list) {
    let data: any = groupby(list, (item) => {
      return item.status;
    });
    const newData = data?.NEW || [];
    const inActiveData = data?.INACTIVE || [];
    const oldData = data?.OLD || [];
    data = [...newData, ...inActiveData, ...oldData];
    return data;
  }


  fetchSubscriptionsAggregate(country?) {
    this._loaderService.updateLoaderStatus(true);
    this._subscriptionService
      .getSubscriptionsAggregateChartData(this.countryCode)
      .subscribe(
        (data) => {
          this._loaderService.updateLoaderStatus(false);
          this.overviewData = data;
        },
        () => {
          this.overviewData = undefined;
          this._loaderService.updateLoaderStatus(false);
        }
      );
  }

  processSubscriptionData(data) {
    const subData = data.map((sub: SubscriptionData) => {
      return {
        ...sub,
        subscriptionStatus: {
          ...this.initialSubStatus,
          ...mapArrayToObject(sub.subscriptionStatus, 'divisionName'),
        },
      };
    });
    this.subData = subData;
    this.updateFilteredData();
    // this.filteredSubData = subData;
  }

  gssnDataSync() {
    if (this.isSyncInProgress) {
      return;
    }
    this._dialogService.confirmGssnSync().subscribe((action) => {
      if (action) {
        const toastrData = this._toastr.success(
          this._translateService.instant(
            'subscriptions.DEALER_DATA_SYNC_MESSAGE'
          ),
          this._translateService.instant('subscriptions.SUCCESS'),
          {
            timeOut: 3000,
          }
        );
        this.isSyncInProgress = true;
        const toastId = toastrData?.toastId || 0;
        const countryCode = this.countryCode?.toLowerCase();
        this._apiService.dealerDataSynch(this.countryCode).subscribe(
          () => {
            this._toastr.clear(toastId);
            this._toastr.success(
              this._translateService.instant(
                'subscriptions.SYNC_REFRESH_MESSAGE'
              ),
              this._translateService.instant('subscriptions.SYNC_SUCCESS_MESSAGE')
            );
            this.isSyncInProgress = false;
            try {
              this.lastSyncDate = moment().toDate();
              if (countryCode && this.gssnSyncDates[countryCode]) {
                this.gssnSyncDates[countryCode].lastSyncDate = moment().toDate();
                this._roleService.changeGssnSyncDates(this.gssnSyncDates);
              }
            } catch (error) {
              console.log(error)
            }
          },
          (error) => {
            this.isSyncInProgress = false;

            this._toastr.clear(toastId);
            if (error?.status === 403) {
              this._toastr.clear();
              this._toastr.error(
                this._translateService.instant(
                  'subscriptions.SYNC_DUPLICATE_ERROR'
                )
              );
              return;
            }
            this._toastr.error(
              this._translateService.instant(
                'subscriptions.SYNC_ERROR_MESSAGE'
              )
            );
          }
        );
      }
    });
  }

  updateFilteredData() {
    const { filterKeyword } = this;
    if (filterKeyword) {
      this.filteredSubData = filter(this.subData, (el: SubscriptionData) => {
        if (
          includes(el.legalName?.toLowerCase(), filterKeyword.toLowerCase()) ||
          includes(el.companyId?.toLowerCase(), filterKeyword.toLowerCase()) ||
          includes(el.outletId?.toLowerCase(), filterKeyword.toLowerCase()) ||
          includes(el.brandCode?.toLowerCase(), filterKeyword.toLowerCase())
        )
          return true;
        return false;
      });
    } else this.filteredSubData = this.subData;

    if (this.currentFilters.length !== 0)
      this.filteredSubData = this.filteredSubData.filter(item => this.currentFilters.includes(item.status))
  }

  editSubscription(subscription) {
    this._dialogService.editSubscription(subscription).subscribe((action) => {
      if (action && action.type == 'delete') {
        this._subscriptionsService.deleteSubscription(action.payload).subscribe(
          (data) => {
            this.subData = this.subData.filter(
              (el) => el.uuid !== action.payload
            );
            this.fetchSubscriptionsAggregate();
            this.updateFilteredData();
          },
          (err) => {
            console.log('ERROR');
          }
        );
      } else if (action && action.type == 'edit') {

        this._subscriptionsService
          .updateSubscription(action.payload)
          .subscribe((data: SubscriptionData) => {
            this.fetchSubscriptionsAggregate();
            const tableData = {
              ...data,
              subscriptionStatus: {
                ...this.initialSubStatus,
                ...mapArrayToObject(data.subscriptionStatus, 'divisionName'),
              },
            };
            this.subData = this.subData.map((el) =>
              el.id == tableData.id ? tableData : el
            );
            this.updateFilteredData();
          });
      }
    });
  }

  downloadAsCSV() {
    this._subscriptionService.downloadMPCDealersData(this.countryCode);
  }

  refreshData() {
    this.fetchSubscriptionData();
    this.fetchSubscriptionsAggregate();
  }

  setCurrentFitlers = (data) => {
    if (this.currentFilters.includes(data)) {
      this.currentFilters = this.currentFilters.filter(item => item !== data)
    } else {
      this.currentFilters.push(data);
    }
    this.updateFilteredData();
  }
}
