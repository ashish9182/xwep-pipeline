import { Component, OnInit } from '@angular/core';
import { SubscriptionsService } from '@modules/subscriptions/subscriptions.service';
import { COUNTRY_NAMES, RoutePaths } from '@core/constants';
import { SubscriptionsInfo, SubscriptionsInfoNew } from '@core/interfaces';
import { includes, get } from '@core/utils';
import { TableConfig } from './table-config';
import { Router } from '@angular/router';
// import { Route } from '@angular/compiler/src/core';
import { LoaderService, RoleService } from '@core/services';

@Component({
  selector: 'app-all-subscriptions',
  templateUrl: './all-subscriptions.component.html',
  styleUrls: ['./all-subscriptions.component.scss'],
})
export class AllSubscriptionsComponent implements OnInit {
  countryNames: any = COUNTRY_NAMES;
  filteredSubInfoData: SubscriptionsInfoNew[];
  subInfoData: SubscriptionsInfoNew[];
  filterKeyword: string = '';
  overviewData: any;

  displayedColumns: string[][] = TableConfig.columns;
  columnConfig: any = TableConfig.columnConfig;
  columnKeys: any = TableConfig.keys;
  cellSelectors: any = TableConfig.selectors;

  getValueIn: any = get;
  authList: any[] = [];

  constructor(
    private _subscriptionsService: SubscriptionsService,
    private _router: Router,
    private _loaderService: LoaderService,
    private _roleService: RoleService
  ) { }

  ngOnInit(): void {
    this.authList = this._roleService.getAuthorizationList();
    if (!this.authList.includes("VIEW_ALL_SUBSCRIPTION")) {
      this._router.navigate(['/error']);
    } else {
      this.fetchAllSubscriptionsData();
      this.fetchAllSubscriptionsAggregateData();
    }
  }

  fetchAllSubscriptionsDataOld() {
    this._loaderService.updateLoaderStatus(true);

    this._subscriptionsService.getAllSubscriptionsInfo().subscribe(
      (data: SubscriptionsInfo[]) => {
        this._loaderService.updateLoaderStatus(false);
        // this.subInfoData = data;
        this.updateFilteredSubInfoData();
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
      }
    );
  }

  fetchAllSubscriptionsData() {
    this._loaderService.updateLoaderStatus(true);

    this._subscriptionsService.getAllSubscriptionsInfo().subscribe(
      (data: SubscriptionsInfoNew[]) => {
        this._loaderService.updateLoaderStatus(false);
        this.subInfoData = data;
        console.log(this.subInfoData)
        this.updateFilteredSubInfoData();
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
      }
    );
  }

  fetchAllSubscriptionsAggregateData() {
    this._loaderService.updateLoaderStatus(true);

    this._subscriptionsService.getSubscriptionsAggregateChartData().subscribe(
      (data) => {
        
        this._loaderService.updateLoaderStatus(false);
        this.overviewData = data;
        console.log("new2",data);
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
      }
    );
  }

  updateFilteredSubInfoData() {
    const { filterKeyword } = this;
    if (this.filterKeyword) {
      this.filteredSubInfoData = this.subInfoData.filter((el) => {
        if (
          el.mpc &&
          this.countryNames[el.mpc] &&
          includes(
            this.countryNames[el.mpc].toLowerCase(),
            filterKeyword.toLowerCase()
          )
        )
          return true;
        return false;
      });
    } else this.filteredSubInfoData = this.subInfoData;
    console.log("filter",this.filteredSubInfoData)
  }
  goToSubscriptions(data) {
    this._router.navigate([RoutePaths.viewAllSubscriptions, data.mpc]);
  }

  downloadAsCSV() {
    this._subscriptionsService.downloadHQDealersData();
  }
}
