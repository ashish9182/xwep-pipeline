import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';

import { AllSubscriptionsComponent } from './all-subscriptions.component';
import { routing } from './all-subscriptions.routing';
import { CountryViewComponent } from './components/country-view/country-view.component';


@NgModule({
  declarations: [
    AllSubscriptionsComponent,
    CountryViewComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    routing
  ],
})
export class AllSubscriptionsModule { }
