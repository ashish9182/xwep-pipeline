import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import {
  COUNTRY_NAMES
  // , SUBSCRIPTION_OVERVIEW_CHART 
} from '@core/constants';
import { ApiService, DialogService, LoaderService, RoleService } from '@core/services';
import { SubscriptionsService } from '@modules/subscriptions/subscriptions.service';
import {
  SubscriptionData
  // , SubscriptionStatus
} from '@core/interfaces';
import { includes, filter, mapArrayToObject } from '@core/utils';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-country-view',
  templateUrl: './country-view.component.html',
  styleUrls: ['./country-view.component.scss'],
})
export class CountryViewComponent<T> implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _apiService: ApiService,
    private _subscriptionService: SubscriptionsService,
    private _dialogService: DialogService<T>,
    private _loaderService: LoaderService,
    private _roleService: RoleService,
    private _router: Router,
    private location: Location

  ) {

  }

  countryCode: string;
  countryName: string;
  subData: SubscriptionData[];
  filteredSubData: SubscriptionData[];

  initialSubStatus: any = {
    truck: { subscribed: false },
  };

  overviewData: any;
  chartActiveEl: any;
  authList = [];
  filterKeyword: string = '';

  ngOnInit(): void {
    this.authList = this._roleService.getAuthorizationList();
    if (!this.authList.includes("VIEW_ALL_SUBSCRIPTION")) {
      this._router.navigate(['/error']);
    } else {
      this._route.paramMap
        .pipe(map((paramMap) => paramMap.get('country')))
        .subscribe((country) => {
          this.countryCode = country;
          this.countryName = COUNTRY_NAMES[country] || country;
          this.fetchSubscriptionsByNationality(country);
          this.fetchSubscriptionsAggregate(country);
        });
    }
  }

  updateFilteredData() {
    const { filterKeyword } = this;
    this.filteredSubData = filter(this.subData, (el: SubscriptionData) => {
      if (
        includes(el.legalName?.toLowerCase(), filterKeyword.toLowerCase()) ||
        includes(el.companyId?.toLowerCase(), filterKeyword.toLowerCase()) ||
        includes(el.outletId?.toLowerCase(), filterKeyword.toLowerCase()) ||
        includes(el.brandCode?.toLowerCase(), filterKeyword.toLowerCase())
      )
        return true;
      return false;
    });
  }


  fetchSubscriptionsByNationality(country) {
    this._loaderService.updateLoaderStatus(true);
    this._apiService.getSubscriptionsByNationality(country).subscribe(
      (data: SubscriptionData[]) => {
        this._loaderService.updateLoaderStatus(false);
        this.subData = data.map((el) => ({
          ...el,
          subscriptionStatus: {
            ...this.initialSubStatus,
            ...mapArrayToObject(el.subscriptionStatus, 'divisionName'),
          },
        }
        )
        );
        

        console.log("subdata",this.subData)
        this.updateFilteredData();
      },
      () => {
        this._loaderService.updateLoaderStatus(false);
      }
    );
  }

  fetchSubscriptionsAggregate(country) {
    this._loaderService.updateLoaderStatus(true);
    this._subscriptionService
      .getSubscriptionsAggregateChartData(country)
      .subscribe(
        (data) => {
          this._loaderService.updateLoaderStatus(false);
          this.overviewData = data;
        },
        () => {
          this._loaderService.updateLoaderStatus(false);
        }
      );
  }

  openSubscriptionDetails(sub) {
    this._dialogService.viewSubscription(sub).subscribe(() => { });
  }

  showTooltip(el) {
    if (el)
      setTimeout(() => {
        this.chartActiveEl = el;
      }, 100);
  }


  goBack() {
    // this._router.navigate(['/all-subscriptions']);
    this.location.back();
  }
  returnSubscriptionStatus(el){
    console.log(el)
    return {truck:[...el.subscriptionStatus]}
  }

}
