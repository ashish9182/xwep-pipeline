import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';

import { CountryViewComponent } from './country-view.component';
import { SubscriptionsService } from '@modules/subscriptions/subscriptions.service';
import { DialogService, LoaderService, ApiService,RoleService } from '@core/services';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { RouterTestingModule } from "@angular/router/testing";
import { of, BehaviorSubject, observable } from 'rxjs';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { Location } from '@angular/common';

describe('CountryViewComponent', () => {
  let component: CountryViewComponent<any>;
  let fixture: ComponentFixture<CountryViewComponent<any>>;
  let ApiServiceSpy: jasmine.SpyObj<ApiService>;
  let LoaderServiceSpy: jasmine.SpyObj<LoaderService>;
  let DialogServiceSpy: jasmine.SpyObj<DialogService<any>>;
  let SubscriptionsServiceSpy: jasmine.SpyObj<SubscriptionsService>;
  let ActivatedRouterSpy: jasmine.SpyObj<ActivatedRoute>;
  let TranslationServiceSpy: jasmine.SpyObj<TranslationService>;
  let LocationSpy: Location;

  class RoleStub {
    _role = "test";
    countryEmmitter = new BehaviorSubject('');
    userData = {firstName : "Test", countryCode : "D"};
    getUserRole() { return this._role; }
    getUserData () { return this.userData };
    getCountryList() {return []}
    getAuthorizationList() {return ['VIEW_ALL_SUBSCRIPTION']}
  }

  class LocationStub {
    back () {}
  }


  beforeEach(async () => {
    const ApiSpy = jasmine.createSpyObj('ApiService', [
      'getSubscriptionsByNationality',
    ]);

    const LoaderSpy = jasmine.createSpyObj('LoaderService', [
      'updateLoaderStatus',
    ]);

    const DialogSpy = jasmine.createSpyObj('DialogService', [
      'viewSubscription',
      'editSubscription',
    ]);

    const SubscriptionsSpy = jasmine.createSpyObj('SubscriptionsService', [
      'getSubscriptionsByNationality',
      'getSubscriptionsAggregateChartData',
    ]);

    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);
    // const ActivateRouterSpy = jasmine.createSpyObj('ActivatedRoute');

    await TestBed.configureTestingModule({
      declarations: [CountryViewComponent],
      providers: [
        { provide: ApiService, useValue: ApiSpy },
        { provide: LoaderService, useValue: LoaderSpy },
        { provide: DialogService, useValue: DialogSpy },
        { provide: SubscriptionsService, useValue: SubscriptionsSpy },
        {
          provide: ActivatedRoute, useValue:
            { paramMap: of({ get: v => { return 'A' } }) }

          // { paramMap: of({ country: 'A' }) }

        },
        { provide: Location, useClass: LocationStub },
        { provide: TranslationService, useValue: TranslationSpy },
        {provide : RoleService, useClass : RoleStub},

      ],
      imports: [RouterTestingModule.withRoutes([{ path: 'A', component: CountryViewComponent }]),
        HttpClientTestingModule,
      TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader,
        },
      })
      ]
    })
      .compileComponents();

    ApiServiceSpy = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;

    LoaderServiceSpy = TestBed.inject(LoaderService) as jasmine.SpyObj<
      LoaderService
    >;

    DialogServiceSpy = TestBed.inject(DialogService) as jasmine.SpyObj<
      DialogService<any>
    >;

    ActivatedRouterSpy = TestBed.inject(ActivatedRoute) as jasmine.SpyObj<ActivatedRoute>;

    SubscriptionsServiceSpy = TestBed.inject(
      SubscriptionsService
    ) as jasmine.SpyObj<SubscriptionsService>;

    TranslationServiceSpy = TestBed.inject(TranslationService) as jasmine.SpyObj<TranslationService>;

    LocationSpy = TestBed.inject(Location);

    ApiServiceSpy.getSubscriptionsByNationality
      .withArgs('A')
      .and.callFake(() => of([]));

    SubscriptionsServiceSpy.getSubscriptionsAggregateChartData
      .withArgs('A')
      .and.callFake(() => of({
        dealerCount: 1,
        aggregateData: []
      }));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call view subscription', () => {
    DialogServiceSpy.viewSubscription.and.callFake(() => of({ type: 'update', payload: 1 }));
    component.openSubscriptionDetails({});
    expect(DialogServiceSpy.viewSubscription).toHaveBeenCalled();
  });

  it('check if tool tip is updated', fakeAsync(() => {
    component.showTooltip("Test");
    // fixture.detectChanges();
    tick(100);
    expect(component.chartActiveEl).toBe("Test");

  }));
  it('Check if filtered Sub data filters data based on string searched',()=>{
    component.filterKeyword = 'Test1';
    component.subData = [{ id: 1,
      brandCode: 'string',
      uuid : "",
      legalName: 'Test',
      companyId: 'string',
      email: 'string',
      emailLKW: 'string',
      nationality: 'string',
      outletId: 'string',
      place: 'string',
      postCode: 'string',
      road: 'string',
      subscriptionStatus: [],
      status:"string"}];
      fixture.detectChanges();
      component.updateFilteredData();
      expect(component.filteredSubData.length).toBe(0)
  });

  it('should call back subscription', () => {
     spyOn(LocationSpy, 'back');
    component.goBack()
    expect(LocationSpy.back).toHaveBeenCalled();
  });


});
