import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { RoutePaths } from '@core/constants';
import { AllSubscriptionsComponent } from './all-subscriptions.component';
import { CountryViewComponent } from './components/country-view/country-view.component';

export const routes: Routes = [
    {
        path: '',
        component: AllSubscriptionsComponent,
    },
    {
        path: ':country',
        component: CountryViewComponent,
    },

];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);