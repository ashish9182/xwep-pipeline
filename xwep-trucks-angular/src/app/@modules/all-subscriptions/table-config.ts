export const TableConfig = {
    keys: {
        mpc: 'mpc',
        dealerCount: 'dealerCount',
        subscriptionCount: 'subscriptionCount'
    },
    columns: [
        [
            'mpc',
            'dealerCount',
            'subscriptionCount'
        ]
    ],
    columnConfig: {
        mpc: {
            title: 'subscriptions.subscriptionData.MPC',
            rowSpan: 2,
            colSpan: 1,
            key: 'mpc'
        },
        dealerCount: {
            title: 'subscriptions.subscriptionData.DEALER_COUNT',
            rowSpan: 2,
            colSpan: 1,
            key: 'dealerCount'
        },
        subscriptionCount: {
            title: 'subscriptions.subscriptionData.SUBSCRIPTION_COUNT',
            rowSpan: 1,
            colSpan: 1,
            key: 'subscriptionCount'
        }
    },
    selectors: {
        mpc: 'mpc',
        dealerCount: 'dealerCount',
        subscriptionCount: 'subscriptionCount'
        
    }
} 