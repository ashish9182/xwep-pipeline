import { ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { AllSubscriptionsComponent } from './all-subscriptions.component';

import { SubscriptionsService } from '@modules/subscriptions/subscriptions.service';
import { DialogService, LoaderService, ApiService,RoleService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { of,BehaviorSubject } from 'rxjs';

import { RouterTestingModule } from "@angular/router/testing";
import { Router } from "@angular/router";
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('AllSubscriptionsComponent', () => {
  let component: AllSubscriptionsComponent;
  let fixture: ComponentFixture<AllSubscriptionsComponent>;

  let ApiServiceSpy: jasmine.SpyObj<ApiService>;
  let LoaderServiceSpy: jasmine.SpyObj<LoaderService>;
  let ToastrServiceSpy: jasmine.SpyObj<ToastrService>;
  let DialogServiceSpy: jasmine.SpyObj<DialogService<any>>;
  let SubscriptionsServiceSpy: jasmine.SpyObj<SubscriptionsService>;
  let RouterSpy: Router;
  let TranslationServiceSpy: jasmine.SpyObj<TranslationService>;

  class RouterStub {
    navigate() { }
  }

  class RoleStub {
    _role = "test";
    countryEmmitter = new BehaviorSubject('');
    userData = {firstName : "Test", countryCode : "D"};
    getUserRole() { return this._role; }
    getUserData () { return this.userData };
    getCountryList() {return []}
    getAuthorizationList() {return ['VIEW_ALL_SUBSCRIPTION']}
  }


  beforeEach(async () => {

    const ApiSpy = jasmine.createSpyObj('ApiService', [
      'getAllSubscriptionsInfo',
      'getSubscriptionsAggregate',
    ]);

    const LoaderSpy = jasmine.createSpyObj('LoaderService', [
      'updateLoaderStatus',
    ]);

    const ToastSpy = jasmine.createSpyObj('ToastrService', [
      'success',
      'error',
    ]);

    const DialogSpy = jasmine.createSpyObj('DialogService', [
      'addNewSubscription',
      'editSubscription',
    ]);

    const SubscriptionsSpy = jasmine.createSpyObj('SubscriptionsService', [
      'getAllSubscriptionsInfo',
      'getSubscriptionsAggregateChartData',
      'downloadHQDealersData'
    ]);

    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);

    await TestBed.configureTestingModule({
      declarations: [AllSubscriptionsComponent],
      providers: [
        { provide: ApiService, useValue: ApiSpy },
        { provide: LoaderService, useValue: LoaderSpy },
        { provide: ToastrService, useValue: ToastSpy },
        { provide: DialogService, useValue: DialogSpy },
        { provide: SubscriptionsService, useValue: SubscriptionsSpy },
        { provide: Router, useClass: RouterStub },
        { provide: TranslationService, useValue: TranslationSpy },
        {provide : RoleService, useClass : RoleStub},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, MatTooltipModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader,
        },
        
      })],
    })
      .compileComponents();


    ApiServiceSpy = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;

    LoaderServiceSpy = TestBed.inject(LoaderService) as jasmine.SpyObj<
      LoaderService
    >;

    ToastrServiceSpy = TestBed.inject(ToastrService) as jasmine.SpyObj<
      ToastrService
    >;

    DialogServiceSpy = TestBed.inject(DialogService) as jasmine.SpyObj<
      DialogService<any>
    >;

    SubscriptionsServiceSpy = TestBed.inject(
      SubscriptionsService
    ) as jasmine.SpyObj<SubscriptionsService>;

    TranslationServiceSpy = TestBed.inject(TranslationService) as jasmine.SpyObj<TranslationService>;

    SubscriptionsServiceSpy.getAllSubscriptionsInfo
      .withArgs()
      .and.callFake(() => of([]));

    SubscriptionsServiceSpy.getSubscriptionsAggregateChartData
      .withArgs()
      .and.callFake(() => of({
        dealerCount: 1,
        aggregateData: []
      }));

    RouterSpy = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call goto subscription and navigate should happen', () => {
    spyOn(RouterSpy, 'navigate')
    component.goToSubscriptions({ mpc: 'A' });
    fixture.detectChanges();
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });


  it('should download CSV after calling downloadHQDealersData', () => {
    component.downloadAsCSV();
    expect(SubscriptionsServiceSpy.downloadHQDealersData.calls.count()).toBe(
      1
    );
  });

  it('Check if filtered Sub data filters data based on string searched', () => {
    component.filterKeyword = 'string1';
    component.subInfoData = [{
      mpc: 'string',
      dealerCount: 1,
      countryName: 'string',
      subscriptionCount: 1
    }];
    fixture.detectChanges();
    component.updateFilteredSubInfoData();
    expect(component.filteredSubInfoData.length).toBe(0)
  });


});
