import { Component } from '@angular/core';
import { RoleService } from '@core/services';
import { TranslationService } from '@core/services/translation.service';
// import { ActivatedRoute } from '@angular/router';
// import { RoleService, LoaderService } from '@core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  isUserLoaded : boolean = false;
  showLoader: boolean;
  errorMessage : string;

  constructor(
    private _translationService: TranslationService,
    private roleService : RoleService
  ) {
    
    this._translationService.initTranslationService();
    this.roleService.getUserRole() ? this.isUserLoaded = true : this.isUserLoaded= false;
    if(!this.isUserLoaded) this.errorMessage = this.roleService.getErrorMessage();
  }
}
