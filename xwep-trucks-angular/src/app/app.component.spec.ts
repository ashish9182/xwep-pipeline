import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from './@core/services/translation.service';
import { RoleService } from '@core/services';

describe('AppComponent', () => {
  let TranslationServiceSpy: jasmine.SpyObj<TranslationService>;
  class RoleStub {
    _role = "test";
    userData = {};
    getUserRole() { return false; }
    getUserData () { return {firstName : 'test'}}
    getErrorMessage () { return "ERROR"}
  }

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        }),
      ],

      declarations: [AppComponent],
      providers: [{
        provide: TranslationService, useValue: spy
      },
      { provide: RoleService, useClass: RoleStub },


      ],
    }).compileComponents();
    TranslationServiceSpy = TestBed.inject(TranslationService) as jasmine.SpyObj<TranslationService>;
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should default the showLoader to undefined`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.showLoader).toEqual(undefined);
  });

  it(`should initialize the translation service'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    TranslationServiceSpy.initTranslationService
    expect(TranslationServiceSpy.initTranslationService.calls.count())
      .toBe(1, 'Translation spy was called once');
  });

});
