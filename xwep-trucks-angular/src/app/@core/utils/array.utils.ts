/**
 * Map array to object based on a key
 */
export function mapArrayToObject(array: Array<any>, key: string) {
    const obj = {};
    array.forEach(el => {
        if (el[key])
            obj[el[key].toLowerCase()] = el;
    })
    return obj;
}


/**
 * String to Array Buffer
 */

export function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}