import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RoleService } from '@core/services';
import { MpcGuardGuard } from './mpc-guard.guard';

describe('MpcGuardGuard', () => {
  let guard: MpcGuardGuard;
  let routeMock: any = { snapshot: {} };
  let routeStateMock: any = { snapshot: {}, url: '/cookies' };
  let routerMock = { navigate: jasmine.createSpy('navigate') }

  class RoleStub {
    _role = "test";
    userData = {firstName : "Test", countryCode : "D"};
    getUserRole() { return this._role; }
    getUserData () { return this.userData };

  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide : Router , useValue : Router},
        {provide : RoleService , useClass : RoleStub},
        MpcGuardGuard,
        { provide: Router, useValue: routerMock }
      ]
    });
    guard = TestBed.inject(MpcGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return true cuurently to test the current implemenation', () => {
    expect(guard.canActivate(routeMock, routeStateMock)).toEqual(true);
  });
  
});
