import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RoleService } from '@core/services';
import { HqGuardGuard } from './hq-guard.guard';

describe('HqGuardGuard', () => {
  let guard: HqGuardGuard;
  let routeMock: any = { snapshot: {} };
  let routeStateMock: any = { snapshot: {}, url: '/cookies' };
  let routerMock = { navigate: jasmine.createSpy('navigate') }

  class RoleStub {
    _role = "test";
    userData = { firstName: "Test", countryCode: "D" };
    getUserRole() { return this._role; }
    getUserData() { return this.userData };

  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: Router },
        { provide: RoleService, useClass: RoleStub },
        HqGuardGuard,
        { provide: Router, useValue: routerMock }
      ]
    });
    guard = TestBed.inject(HqGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return true cuurently to test the current implemenation', () => {
    expect(guard.canActivate(routeMock, routeStateMock)).toEqual(true);
  });

});
