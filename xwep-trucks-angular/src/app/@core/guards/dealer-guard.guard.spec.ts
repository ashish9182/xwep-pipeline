import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RoleService } from '@core/services';

import { DealerGuardGuard } from './dealer-guard.guard';

describe('DealerGuardGuard', () => {
  let guard: DealerGuardGuard;
  let routeMock: any = { snapshot: {} };
  let routeStateMock: any = { snapshot: {}, url: '/cookies' };
  let routerMock = { navigate: jasmine.createSpy('navigate') }

  class RoleStub {
    _role = "test";
    userData = {firstName : "Test", countryCode : "D"};
    getUserRole() { return this._role; }
    getUserData () { return this.userData };
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide : Router , useValue : Router},
        {provide : RoleService , useClass : RoleStub},
        DealerGuardGuard,
         { provide: Router, useValue: routerMock }
      ]
    });
    guard = TestBed.inject(DealerGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return true cuurently to test the current implemenation', () => {
    expect(guard.canActivate(routeMock, routeStateMock)).toEqual(true);
  });
  
});
