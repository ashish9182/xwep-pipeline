import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RoleService } from '@core/services';

@Injectable({
  providedIn: 'root'
})
export class DealerGuardGuard implements CanActivate {

  constructor(
    private roleService: RoleService,
    private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.roleService.getUserRole()?.toLowerCase() !== 'dealer') {
        this.router.navigate(['/dashboard']);
      }
      return true;
  }
  
}
