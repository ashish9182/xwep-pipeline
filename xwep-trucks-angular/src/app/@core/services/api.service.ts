import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { API_PATHS } from '@core/constants';
import { CreateSubscription, SubscriptionData } from '@core/interfaces';
// import { map } from 'rxjs/operators'
import { } from '@angular/common/http';
import { of } from 'rxjs';
@Injectable()
export class ApiService {
  constructor(private _http: HttpService) { }

  /**
   * Get All Subscriptions Data
   */

  getAllSubscriptionsData() {
    const url = API_PATHS.allSubscriptions;
    return this._http.get(url);
  }

  /**
   * Get Paginated Subscriptions Data
   */

  getSubscriptionsData(pageNumber = 0, pageSize = 10) {
    const url = API_PATHS.manageSubscriptions;
    return this._http.get(url, {
      params: {
        pageNo: pageNumber, //Change to pageNumber
        pageSize,
      },
    });
  }

  /**
   * Create New Subscription
   */

  createNewSubscription(data: CreateSubscription) {
    const url = API_PATHS.manageSubscriptions;
    return this._http.post(url, { data });
  }

  /**
   * Delete Subscription
   */

  deleteSubscription(id) {
    const url = `${API_PATHS.manageSubscriptions}/${id}`;
    return this._http.delete(url, {});
  }

  /**
   * Update Subscription
   */
  updateSubscription(data: SubscriptionData) {
    const url = `${API_PATHS.manageSubscriptions}/${data.uuid}`;
    return this._http.put(url, { data });
  }

  /**
   * Get All Subscrptions Info
   */
  getAllSubscriptionsInfo() {
    const url = API_PATHS.manageSubscriptionsInfo;
    return this._http.get(url, {
      params: {
        pageNo: 0,
        pageSize: 100,
        sortBy: 'nationality',
        query: '',
      },
    });
  }

  /**
   * Get Subscriptions By Nationality
   */
  getSubscriptionsByNationality(code) {
    const url = API_PATHS.manageSubscriptionsByNationality;
    return this._http.get(url, {
      params: {
        nationality: code,
      },
    });
  }

  /**
   * Get Subscription Aggregate Data
   */
  getSubscriptionsAggregate(code = '') {
    const url = API_PATHS.subscriptionsAggregate;
    return this._http.get(url, {
      params: {
        query: code,
      },
    });
  }

  /**
   * Download MPC Dealer Data as CSV
   */
  getMPCDealerDataCSV(data?) {
    const url = API_PATHS.downloadMPCDealers;
    return this._http.get(url, {
      params: {
        nationality: data.code,
        language: data.language
      },
      customHeaders: {
        // contentType: 'application/octet-stream'
      },
      responseType: 'arraybuffer',
    });
  }
  /**
   * Download HQ Dealer Data as CSV
   */
  getHQDealersDataCSV(lang?) {
    const url = API_PATHS.downloadHQDealers;
    return this._http.get(url, {
      responseType: 'arraybuffer',
      params: {
        language: lang
      },
    });
  }

  /**
   * dealer date synch
   */
  dealerDataSynch(countryCode) {
    const url = API_PATHS.dealerDataSynch;
    return this._http.get(url, {
      params: {
        nationality: countryCode,
      },
    });
  }

  /**
   * Dealer based sub status
   */
  getDealerSubscriptionStatus() {
    const url = API_PATHS.dealerMySubscription;
    return this._http.get(url);
  }
}
