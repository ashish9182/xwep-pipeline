import { TestBed } from '@angular/core/testing';

import { LoaderService } from './loader.service';

describe('LoaderService', () => {
  let service: LoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers : [
        {provide : LoaderService , useValues : {}}
      ]
    });
    service = TestBed.inject(LoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should increment apicount if called wirth status true', () => {
    service.updateLoaderStatus(true)
    expect(service.apiCount).toBe(1);
  });

  it('should decrement apicount if called wirth status true', () => {
    service.apiCount = 1;
    service.updateLoaderStatus(false);
    expect(service.apiCount).toBe(0);
  });

});
