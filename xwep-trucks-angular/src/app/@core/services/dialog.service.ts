// import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { map, take } from 'rxjs/operators';

import {
  AddSubscriptionComponent,
  EditSubscriptionComponent,
  ViewSubscriptionComponent,
  DeleteSubscriptionConfirmComponent,
  GssnSyncConfirmComponent
} from '@core/components/dialog';


@Injectable()

export class DialogService<T> {

  private _defaultConfig: MatDialogConfig = {

  }

  constructor(private _dialog: MatDialog) { }

  /**
   * Generic Method to open a dialog with a template as an argument
   */
  open(component, config: MatDialogConfig = {}) {

    let dialogRef: MatDialogRef<T> = this._dialog.open(component, { ...this._defaultConfig, ...config });

    return dialogRef.afterClosed().pipe(take(1), map(res => res));

  }

  /**
   * Add New Subscription Dialog
   */
  addNewSubscription() {
    return this.open(AddSubscriptionComponent, {})
  }

  /**
  * Edit Subscription Dialog
  */
  editSubscription(subData) {
    return this.open(EditSubscriptionComponent, { data: subData })
  }
  /**
  * View Subscription Dialog
  */
  viewSubscription(subData) {
    return this.open(ViewSubscriptionComponent, { data: subData })
  }

  /**
   * Confirm Subscription Deletion
   */
  confirmDeleteSubscription(subData) {
    return this.open(DeleteSubscriptionConfirmComponent, { data: subData })
  }

  /**
   * Confirm GSSN Sync
   */
  confirmGssnSync() {
    return this.open(GssnSyncConfirmComponent, { })
  }

}
