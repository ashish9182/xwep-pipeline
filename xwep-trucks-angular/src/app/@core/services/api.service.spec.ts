import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import { HttpService } from './http.service';


class HttpServiceStub {
  get () {

  }
  post(){

  }
  put(){

  }
  delete(){

  }
}
describe('ApiService', () => {
  let service: ApiService;
  let httpServiceSpy : HttpService

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide : ApiService, useClass: ApiService},
        {provide : HttpService, useClass : HttpServiceStub}
      ]
    });
    service = TestBed.inject(ApiService);
    httpServiceSpy = TestBed.inject(HttpService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get if getAllSubscriptionsData is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getAllSubscriptionsData();
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if getSubscriptionsData is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getSubscriptionsData();
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http post if createNewSubscription is called', () => {
    spyOn(httpServiceSpy, 'post')

    service.createNewSubscription({  brandCode: 'string',
      companyId: 'string',
      uuid : "",
      email: 'string',
      emailLKW: 'string',
      emailPassengerCars: 'string',
      emailSmartPassengerCars: 'string',
      emailTransporter: 'string',
      id: 0,
      legalName: 'string',
      nationality: 'string',
      outletId: 'string',
      place: 'string',
      postCode: 'string',
      road: 'string',
      subscriptionStatus: []});

    expect(httpServiceSpy.post).toHaveBeenCalled();
  });

  it('should call http delete if deleteSubscription is called', () => {
    spyOn(httpServiceSpy, 'delete')
    service.deleteSubscription(1);
    expect(httpServiceSpy.delete).toHaveBeenCalled();
  });

  it('should call http put if deleteSubscription is called', () => {
    spyOn(httpServiceSpy, 'put')
    service.updateSubscription({  id : 1, brandCode: 'string',
    uuid : "",
    companyId: 'string',
    email: 'string',
    emailLKW: 'string',
    legalName: 'string',
    nationality: 'string',
    outletId: 'string',
    place: 'string',
    postCode: 'string',
    road: 'string',
    subscriptionStatus: [],
    status:"string",
});
    expect(httpServiceSpy.put).toHaveBeenCalled();
  });

  it('should call http get if getAllSubscriptionsInfo is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getAllSubscriptionsInfo();
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if getSubscriptionsByNationality is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getSubscriptionsByNationality(1);
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if getSubscriptionsAggregate is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getSubscriptionsAggregate();
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if getMPCDealerDataCSV is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getMPCDealerDataCSV(1);
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if getHQDealersDataCSV is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.getHQDealersDataCSV();
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

  it('should call http get if dealerDataSynch is called', () => {
    spyOn(httpServiceSpy, 'get')
    service.dealerDataSynch('DE');
    expect(httpServiceSpy.get).toHaveBeenCalled();
  });

});
