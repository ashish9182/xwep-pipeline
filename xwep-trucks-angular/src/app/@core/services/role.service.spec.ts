import { TestBed } from '@angular/core/testing';
import { HttpService } from './http.service';

import { RoleService } from './role.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { of } from 'rxjs';

xdescribe('RoleService', () => {
  let service: RoleService;
  let httpMock: HttpTestingController;
  let response = {
    "sub": "VNITHIN",
    "name": "V R NITHIN",
    "email": "qburst.nithin@daimler.com",

    // HQ
    // "entitlement_group": ",APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_DATA_ADMIN,APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_CREATE_SUBSCRIPTION,APP30850.XWEP_GSSN_SYNC,APP30850.XWEP_HQ,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION",
    // "authorization_group": "APP30850.XWEP_AUTHORIZATION", 
    // "scoped_entitlement": "APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_CREATE_SUBSCRIPTION$$be,de,es,APP30850.XWEP_GSSN_SYNC$$be,de,es,APP30850.XWEP_MPC$$be,de,es,APP30850.XWEP_SURVEY_FEEDBACK$$be,de,es,APP30850.XWEP_VIEW_OWN_CAMPAIGN$$be,de,es,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$$be,de,es",

    // MPC
    "entitlement_group": "APP30850.XWEP_CREATE_SUBSCRIPTION,APP30850.XWEP_DECO_APPROVER,APP30850.XWEP_GSSN_SYNC,APP30850.XWEP_MPC,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION", "authorization_group": "APP30850.XWEP_AUTHORIZATION",
    "scoped_entitlement": "APP30850.XWEP_PARTICIPATE_SURVEY$$be,de,es,APP30850.XWEP_EXPORT_REPORTS$$be,de,es,APP30850.XWEP_DATA_ADMIN$$be,de,es,APP30850.XWEP_CREATE_SUBSCRIPTION$$be,de,es,APP30850.XWEP_DECO_APPROVER$$be,de,es,APP30850.XWEP_GSSN_SYNC$$be,de,es,APP30850.XWEP_MPC$$be,de,es,APP30850.XWEP_SURVEY_FEEDBACK$$be,de,es,APP30850.XWEP_VIEW_OWN_CAMPAIGN$$be,de,es,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$$be,de,es",

    // Dealer
    // "entitlement_group":"APP30850.XWEP_DEALER,APP30850.XWEP_MARKET_INFORMATION,APP30850.XWEP_PARTICIPATE_SURVEY,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION",
    // "authorization_group":"APP30850.XWEP_AUTHORIZATION",
    // "scoped_entitlement":"APP30850.EXPORT_REPORTS$D-Outlet.XY03684776,APP30850.XWEP_DEALER$D-Outlet.XY03684776,APP30850.XWEP_MARKET_INFORMATION$D-Outlet.XY03684776,APP30850.XWEP_PARTICIPATE_SURVEY$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_CAMPAIGN$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$D-Outlet.XY03684776",

    "countryCodes": [{ "countryName": "be", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": null, "nextSyncDate": "2020-12-16T04:13:59.356Z" }, { "countryName": "de", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": "2020-12-16T04:13:59.356Z", "nextSyncDate": null }, { "countryName": "es", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": null, "nextSyncDate": null }]
  }


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: RoleService, useClass: RoleService },
        { provide: HttpService, useClass: HttpService },
        { provide: TranslationService, useValue: { getTranslationObservable() { return of() }, setCurrentLang() { } } },

      ],
      imports: [HttpClientTestingModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })]
    });
    service = TestBed.inject(RoleService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update the role once response is recived', () => {
    let temp = service.processResponse(response)
    expect(temp.role).toBe('mpc');
  });

  it('should update the role once response is recived', () => {
    let temp = service.processResponse({
      "sub": "VNITHIN",
      "name": "V R NITHIN",
      "email": "qburst.nithin@daimler.com",

      // Dealer
      "entitlement_group": "APP30850.XWEP_DEALER,APP30850.XWEP_MARKET_INFORMATION,APP30850.XWEP_PARTICIPATE_SURVEY,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION",
      "authorization_group": "APP30850.XWEP_AUTHORIZATION",
      "scoped_entitlement": "APP30850.EXPORT_REPORTS$D-Outlet.XY03684776,APP30850.XWEP_DEALER$D-Outlet.XY03684776,APP30850.XWEP_MARKET_INFORMATION$D-Outlet.XY03684776,APP30850.XWEP_PARTICIPATE_SURVEY$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_CAMPAIGN$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$D-Outlet.XY03684776",

      "countryCodes": [{ "countryName": "be", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": null, "nextSyncDate": "2020-12-16T04:13:59.356Z" }, { "countryName": "de", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": "2020-12-16T04:13:59.356Z", "nextSyncDate": null }, { "countryName": "es", "countryLanguage": { "primaryLanguage": "EN", "secondaryLanguage": "DE", "tertiaryLanguage": "" }, "lastSyncDate": null, "nextSyncDate": null }]
    })
    expect(temp.role).toBe('dealer');
  });


  it('should update error message once 401 or 403 is receveid', () => {
    let error = {
      status: 403
    }
    expect(service.processError(error)).toBe('The logged in user is not allowed to access the application. Please contact your application admin.');
  });


  it('should update error message once 500 is receveid', () => {
    let error = {
      status: 500
    }
    expect(service.processError(error)).toBe('Failed to fetch user details');
  });



  it('should get user response once load is called', () => {
    let loaduser = service.load()();
    console.log('httpRequest', loaduser)
    const httpRequest = httpMock.expectOne('/api/getUserInfo');
    httpRequest.flush(response)
    expect(service.getUserData()['firstName']).toBe('V R NITHIN');
  });


  it('should get user fetch failed error if api is failed', () => {
    let loaduser = service.load()();
    console.log('httpRequest', loaduser)
    const httpRequest = httpMock.expectOne('/api/getUserInfo');
    httpRequest.error(
      new ErrorEvent('')
    )
    expect(service.getErrorMessage()).toBe('Failed to fetch user details');
  });

  it('should get user role as mpc', () => {
    let loaduser = service.load()();
    console.log('httpRequest', loaduser)
    const httpRequest = httpMock.expectOne('/api/getUserInfo');
    httpRequest.flush(
      response
    )
    expect(service.getUserRole()).toBe('mpc');
  });

  it('should get user role as hq', () => {
    let loaduser = service.load()();
    const httpRequest = httpMock.expectOne('/api/getUserInfo');
    httpRequest.flush({
      ...response,
      "entitlement_group": "APP30850.Campaign_Approver,APP30850.Survey_Feedback,APP30850.View_Campaign,APP30850.View_Subscription,APP30850.ViewShippingFile,APP30850.XWEP_HQ",
    })
    expect(service.getUserRole()).toBe('hq');
  });

  it('should get country length as 3 as per current input', () => {
    let loaduser = service.load()();
    console.log('httpRequest', loaduser)
    const httpRequest = httpMock.expectOne('/api/getUserInfo');
    httpRequest.flush(
      response
    )
    expect(service.getCountryList().length).toBe(3);
  });

  it('should get authList', () => {
    let temp = service.processResponse(response)
    expect(service.getAuthorizationList()[0]).toBe('CREATE_SUBSCRIPTION');
  });

});
