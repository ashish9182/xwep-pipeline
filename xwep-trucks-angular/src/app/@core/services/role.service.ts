import { Response } from './../interfaces/common';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { TranslationService } from './translation.service';
import { isNgTemplate } from '@angular/compiler';
@Injectable()
export class RoleService {

  constructor(private httpService: HttpService, private _translationService: TranslationService) { }



  private _role: string;
  private _countryList: string[];
  private userData: any;
  private errorMessage: string = '';
  public countryEmmitter = new BehaviorSubject('');
  public languageEmmitter = new BehaviorSubject(['de', 'en']);
  private _languageList: string[] = ['de', 'en'];
  public _authorizationList: string[] = [];

  getUserRole() {
    return this._role;
  }

  getCountryList() {
    return this._countryList;
  }

  getLanguageList() {
    return this._languageList;
  }

  getAuthorizationList() {
    return this._authorizationList;
  }

  updateCurrentCountry(country) {
    this.countryEmmitter.next(country);
    if (this._role === 'mpc') {
      this._languageList = this.userData.languageList[country]?.length ? this.userData.languageList[country] : ['de', 'en'];
      this.languageEmmitter.next(this._languageList);
      this._translationService.setCurrentLang(this._languageList[0]);
    }
  }

  getUserData() {
    return this.userData;
  }

  getErrorMessage() {
    return this.errorMessage;
  }

  changeGssnSyncDates(data) {
    this.userData.gssnSyncDates = data;
  }

  load() {
    let response =

    {
      "sub": "VNITHIN",
      "name": "V R NITHIN",
      "email": "qburst.nithin@daimler.com",

      // HQ
      // "entitlement_group": "APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_DATA_ADMIN,APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_CREATE_SUBSCRIPTION,APP30850.XWEP_GSSN_SYNC,APP30850.XWEP_HQ,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_ALL_SUBSCRIPTION",
      // "authorization_group": "APP30850.XWEP_AUTHORIZATION",
      // "scoped_entitlement": "APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_CREATE_SUBSCRIPTION$$be,de,es,APP30850.XWEP_GSSN_SYNC$$be,de,es,APP30850.XWEP_MPC$$be,de,es,APP30850.XWEP_SURVEY_FEEDBACK$$be,de,es,APP30850.XWEP_VIEW_OWN_CAMPAIGN$$be,de,es,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$$be,de,es",

      // MPC
      "entitlement_group": "APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_PARTICIPATE_SURVEY,APP30850.XWEP_CREATE_SUBSCRIPTION,APP30850.XWEP_DECO_APPROVER,APP30850.XWEP_GSSN_SYNC,APP30850.XWEP_MPC,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION",
      "authorization_group": "APP30850.XWEP_AUTHORIZATION",
      "scoped_entitlement": "APP30850.XWEP_CREATE_SUBSCRIPTION$$de,APP30850.XWEP_GSSN_SYNC$$de,APP30850.XWEP_MPC$$de,APP30850.XWEP_SURVEY_FEEDBACK$$de,APP30850.XWEP_VIEW_OWN_CAMPAIGN$$de,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$$de",
      "countryCodes": [{ "countryName": "de", "participation": 'Dealer', "countryLanguage": { "primaryLanguage": "de", "secondaryLanguage": "en", "tertiaryLanguage": "" }, "lastSyncDate": null, "nextSyncDate": null }]

      // Dealer
      // "entitlement_group": "APP30850.XWEP_DEALER,APP30850.XWEP_MARKET_INFORMATION,APP30850.XWEP_PARTICIPATE_SURVEY,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION",
      // "authorization_group": "APP30850.XWEP_AUTHORIZATION",
      // "scoped_entitlement": "APP30850.EXPORT_REPORTS$D-Outlet.XY03684776,APP30850.XWEP_DEALER$D-Outlet.XY03684776,APP30850.XWEP_MARKET_INFORMATION$D-Outlet.XY03684776,APP30850.XWEP_PARTICIPATE_SURVEY$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_CAMPAIGN$D-Outlet.XY03684776,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$D-Outlet.XY03684776",
      // "countryCodes": [{
      //   "countryName": "es", "participation": 'Dealer',
      //   "countryLanguage": { "primaryLanguage": "fr", "secondaryLanguage": "en", "tertiaryLanguage": "de" }, "lastSyncDate": null, "nextSyncDate": null
      // }]

    }

    return () => {
      return new Promise((resolve) => {
        // this.httpService.get('getUserInfo').subscribe(
          this.httpService.getUserInfo().subscribe(
          (res) => {
            try {
              // res = {"responseStatus":"Success","responseCode":200,"data":{"exp":1631017595,"sub":"MSHARIN","name":"Mohammed","email":"qburst.sharin@daimler.com","entitlement_group":"APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_CREATE_SHIPPING_FILE,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_VIEW_ALL_SHIPPING_FILE,APP30850.XWEP_HQ,APP30850.XWEP_VIEW_ALL_CAMPAIGN,APP30850.XWEP_DATA_ADMIN,APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_VIEW_ALL_SUBSCRIPTION","authorization_group":"APP30850.XWEP_AUTHORIZATION","scoped_entitlement":null,"countryCodes":null,"outletId":null}}
              this.userData = this.processResponse(res.data);
              // this.userData = this.processResponse(res)
              this._role = this.userData?.role;
              if (this._role === 'mpc') {
                this._countryList = this.userData?.countryList;
                this.updateCurrentCountry(this.userData?.countryCode);
                this._languageList = this.userData.languageList[this.userData?.countryCode];
              }
              this._translationService.setCurrentLang(this._languageList[0]);
              resolve(true);
            } catch (error) {
              console.log("Error Parsing Data", error);
              this.errorMessage = this.processError(error);
              this._role = undefined;
            }
          },
          (error) => {
            //////////////////////////////////
              // try {
              //   this.userData = this.processResponse(response);
              //   this._role = this.userData?.role;
              //   if (this._role === 'mpc') {
              //     this._countryList = this.userData?.countryList;
              //     this.updateCurrentCountry(this.userData?.countryCode);
              //     this._languageList = this.userData.languageList[this.userData?.countryCode];
              //   }
              //   this._translationService.setCurrentLang(this._languageList[0]);
              // } catch (error) {
              //   this._role = undefined;
              // }
            ////////////////////////////////////
            this.errorMessage = this.processError(error);
            resolve(true);
          }
        );
      });
    };
  }

  processResponse(response) {
    let userData: any = {};
    userData.firstName = response?.name;
    userData.emailId = response?.email;
    // userData.emailId = 'ashik@qburst.com';

    userData.userName = response?.sub;
    userData.countryList = [];
    let stringArray = [];
    if (response?.entitlement_group.includes('MPC')) {
      let entitlementString = response?.scoped_entitlement;

      stringArray = entitlementString.split(response?.authorization_group.split('.')[0] + ".");

      this._authorizationList = this.processEntitlementsAuthorization(response?.entitlement_group.split(response?.entitlement_group.split('.')[0] + "."), "MPC");

      stringArray = stringArray.filter((item) => {
        return item.includes('MPC') && item !== "";
      });

      // Alternative way to get country list
      userData.countryList = response.countryCodes.map(item => {
        return item.countryName.toUpperCase();
      });
      // console.log("Country List", userData.countryList)
      // userData.countryList = [];

      // (stringArray[0].split('$$')[1])?.split(',').forEach(item => {
      //   if (item !== '') userData.countryList.push(item.toUpperCase())
      // });

      userData.countryCode = userData.countryList[0];
      // New Code for MPC type
      if (response.countryCodes[0].participation !== null) {
        if (response.countryCodes[0].participation === 'Dealer') {
          userData.MPC_Type = 'EU';
        } else if (response.countryCodes[0].participation === 'MPC') {
          userData.MPC_Type = 'NON-EU';
        }
      } else {
        userData.MPC_Type = null;
      }

      //////////////////
      userData.languageList = this.processCountryLanguage(response.countryCodes);
      userData.role = 'mpc';
      let gssnSyncDates = {};
      response?.countryCodes?.forEach(element => {
        gssnSyncDates[element?.countryName] = {
          lastSyncDate: element?.lastSyncDate,
          nextSyncDate: element?.nextSyncDate
        };
      });
      userData.gssnSyncDates = gssnSyncDates;
    } else if (response?.entitlement_group.includes('HQ')) {

      let entitlementString = response?.entitlement_group
      stringArray = entitlementString.split(response?.entitlement_group.split('.')[0] + ".");
      this._authorizationList = this.processEntitlementsAuthorization(stringArray, "HQ");

      this._languageList = ['en', 'de'];
      this.languageEmmitter.next(this._languageList);
      userData.role = 'hq';

    } else if (response?.entitlement_group.includes('DEALER')) {
      let entitlementString = response?.entitlement_group
      stringArray = entitlementString.split(response?.entitlement_group.split('.')[0] + ".");
      this._authorizationList = this.processEntitlementsAuthorization(stringArray, "DEALER");

      this._languageList = this.getDealerLanguage(response);

      this.languageEmmitter.next(this._languageList);
      userData.role = 'dealer';
      userData.isUserSubscribedtoEmail=response.emailSubscribed;
    } 
    return userData;

  }

  getDealerLanguage(response) {
    let temp = [];
    if (response.countryCodes !== undefined && response.countryCodes[0].countryLanguage !== undefined) {
      if (response.countryCodes[0].countryLanguage.primaryLanguage !== "") temp.push(response.countryCodes[0].countryLanguage.primaryLanguage.toLowerCase());
      if (response.countryCodes[0].countryLanguage.secondaryLanguage !== "") temp.push(response.countryCodes[0].countryLanguage.secondaryLanguage.toLowerCase());
      if (response.countryCodes[0].countryLanguage.tertiaryLanguage !== "") temp.push(response.countryCodes[0].countryLanguage.tertiaryLanguage.toLowerCase());
    } else {
      temp = ['en', 'de'];
    }
    return temp;
  }

  processCountryLanguage(data) {
    let languageList = {}
    let temp = []
    data.forEach(item => {
      temp = [];
      if (item.countryLanguage.primaryLanguage !== "") temp.push(item.countryLanguage.primaryLanguage.toLowerCase());
      if (item.countryLanguage.secondaryLanguage !== "") temp.push(item.countryLanguage.secondaryLanguage.toLowerCase());
      if (item.countryLanguage.tertiaryLanguage !== "") temp.push(item.countryLanguage.tertiaryLanguage.toLowerCase());
      languageList[item.countryName.toUpperCase()] = temp;
    });
    return languageList;
  }

  processError(error) {
    let errorMessage = '';
    if (
      error?.status &&
      (error.status === 401 || error.status === 403)
    ) {
      errorMessage =
        'The logged in user is not allowed to access the application. Please contact your application admin.';
    } else if (
      error?.responseCode &&
      (error.responseCode === 401 || error.responseCode === 403)
    ) {
      errorMessage =
        'The logged in user is not allowed to access the application. Please contact your application admin.';
    } else {
      errorMessage = 'Failed to fetch user details';
    }
    return errorMessage;
  }

  processEntitlementsAuthorization(authString, user) {
    let authorizationList = [], tempString;
    authString.forEach(item => {
      if (!item.includes(user) && item !== "") {
        tempString = item.split(',')[0];
        tempString = tempString.split('XWEP_')[1]
        authorizationList.push(tempString);
      }
    });
    return authorizationList;
  }
}
