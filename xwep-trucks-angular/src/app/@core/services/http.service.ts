import { Injectable } from '@angular/core';
import { environment as env } from '@environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()

export class HttpService {

  private _apiBaseUrl: string;

  constructor(
    private _http: HttpClient,
  ) {
    this._apiBaseUrl = env.apiBaseUrl;
  }

  private _getRequestHeaders<T>(customHeaders?): HttpHeaders {
    const headers = new HttpHeaders()
      .set('content-type', customHeaders && customHeaders.contentType ? customHeaders.contentType : 'application/json')
      .set('Access-Control-Allow-Origin', '*');
    return headers;
  }

  private _handleSuccess<T>(response) {
    // console.log(response)
    return response?.data || response
  }
  private _handleError<T>(error: HttpErrorResponse) {
    return throwError(error)
  }

  get<T>(path = '', { absoluteUrl = false, params = {}, customHeaders = {}, responseType = 'json' } = {}): Observable<T> {
    const url = absoluteUrl ? path : `${this._apiBaseUrl}/${path}`;
    const headers = this._getRequestHeaders(customHeaders);
    const options: Object = {
      headers,
      params,
      responseType
    }
    return this._http.get<T>(url, options).pipe(
      map(this._handleSuccess),
      catchError(this._handleError)
    );
  }

  post<T>(path, { data, params = {}, customHeaders = {}, responseType = 'json' }): Observable<T> {
    const url = `${this._apiBaseUrl}/${path}`
    const headers = this._getRequestHeaders(customHeaders);
    const options: Object = {
      headers,
      params,
      responseType
    }
    return this._http.post<T>(url, data, options).pipe(
      map(this._handleSuccess),
      catchError(this._handleError)
    );
  }

  put<T>(path, { data, params = {} }): Observable<T> {
    const url = `${this._apiBaseUrl}/${path}`
    const headers = this._getRequestHeaders();
    return this._http.put<T>(url, data, { headers, params }).pipe(
      map(this._handleSuccess),
      catchError(this._handleError)
    );
  }

  delete<T>(path, { params = {} }): Observable<T> {
    const url = `${this._apiBaseUrl}/${path}`
    const headers = this._getRequestHeaders();
    return this._http.delete<T>(url, { headers, params }).pipe(
      map(this._handleSuccess),
      catchError(this._handleError)
    );
  }

  getUserInfo(){
    //HQuser
    // let dummy = {"responseStatus":"Success","responseCode":200,"data":{"exp":1631017595,"sub":"MSHARIN","name":"Mohammed","email":"qburst.sharin@daimler.com","entitlement_group":"APP30850.XWEP_CREATE_CAMPAIGN,APP30850.XWEP_CREATE_SHIPPING_FILE,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_PUBLISH_CAMPAIGN,APP30850.XWEP_VIEW_ALL_SHIPPING_FILE,APP30850.XWEP_HQ,APP30850.XWEP_VIEW_ALL_CAMPAIGN,APP30850.XWEP_DATA_ADMIN,APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_VIEW_ALL_SUBSCRIPTION","authorization_group":"APP30850.XWEP_AUTHORIZATION","scoped_entitlement":null,"countryCodes":null,"outletId":null}}
    
    // MPC user
    let dummy = {"responseStatus":"Success","responseCode":200,"data":{"exp":1633102844,"sub":"AKANTON","name":"Akshay","email":"qburst.antony@daimler.com","entitlement_group":"APP30850.XWEP_DECO_APPROVER,APP30850.XWEP_MPC,APP30850.XWEP_SURVEY_FEEDBACK,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_GSSN_SYNC,APP30850.XWEP_EXPORT_REPORTS,APP30850.XWEP_CREATE_SUBSCRIPTION","authorization_group":"APP30850.XWEP_AUTHORIZATION","scoped_entitlement":"APP30850.XWEP_VIEW_OWN_CAMPAIGN$D-Outlet.GS00110510,APP30850.XWEP_SURVEY_FEEDBACK$D-Outlet.GS00110510,APP30850.XWEP_MPC$D-Outlet.GS00110510,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$D-Outlet.GS00110510,APP30850.XWEP_CREATE_SUBSCRIPTION$D-Outlet.GS00110510,APP30850.XWEP_GSSN_SYNC$D-Outlet.GS00110510,APP30850.XWEP_EXPORT_REPORTS$D-Outlet.GS00110510","countryCodes":[{"countryName":"D","countryLanguage":{"primaryLanguage":"de","secondaryLanguage":"en","tertiaryLanguage":""},"lastSyncDate":"2021-09-30T00:01:24.622Z","nextSyncDate":"2021-10-15T00:00:00.622Z","participation":"Dealer"}],"outletId":"GS00110510","emailSubscribed":false}}
    
    //Dealer

    // let dummy = {"responseStatus":"Success","responseCode":200,"data":{"exp":1633589453,"sub":"VISHNN","name":"Vishnu","email":"qburst.n@daimler.com","entitlement_group":"APP30850.XWEP_MARKET_INFORMATION,APP30850.XWEP_PARTICIPATE_SURVEY,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION,APP30850.XWEP_VIEW_OWN_CAMPAIGN,APP30850.XWEP_DEALER","authorization_group":"APP30850.XWEP_AUTHORIZATION","scoped_entitlement":"APP30850.XWEP_VIEW_OWN_CAMPAIGN$D-Outlet.ES00001370,APP30850.XWEP_DEALER$D-Outlet.ES00001370,APP30850.XWEP_VIEW_OWN_SUBSCRIPTION$D-Outlet.ES00001370,APP30850.XWEP_MARKET_INFORMATION$D-Outlet.ES00001370,APP30850.XWEP_PARTICIPATE_SURVEY$D-Outlet.ES00001370","countryCodes":[{"countryName":"ES","countryLanguage":{"primaryLanguage":"es","secondaryLanguage":"en","tertiaryLanguage":""},"lastSyncDate":"2021-09-30T00:08:04.306Z","nextSyncDate":"2021-10-15T00:00:00.306Z","participation":null}],"outletId":"GS0010461","emailSubscribed":false}}
    
    return of(dummy)
  }

}
