import { Injectable } from '@angular/core';
import { TranslateService, TranslationChangeEvent } from '@ngx-translate/core';
import { map, startWith } from 'rxjs/operators';

@Injectable()

export class TranslationService {

  constructor(private _translate: TranslateService) { }

  initTranslationService() {
    this._translate.setDefaultLang('en');
    this._translate.use('en');
  }

  getTranslationObservable() {
    return this._translate.onLangChange.pipe(
      startWith(this._translate.currentLang),
      map((e:TranslationChangeEvent) => e.lang || e)
    );
  }

  setCurrentLang(lang) {
    this._translate.use(lang)
  }

  stream(key) {
    return this._translate.stream(key);
  }

  get(key) {
    return this._translate.get(key);
  }

  instant(key) {
    return this._translate.instant(key)
  }

}
