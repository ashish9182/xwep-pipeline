import { TestBed } from '@angular/core/testing';

import { TranslationService } from './translation.service';
import { TranslateService } from '@ngx-translate/core';
import { of, pipe } from 'rxjs';

class TranslateServiceStub {
  setDefaultLang(key) { }
  use(key) { }
  stream(key) { }
  get(key) { }
  instant(key) { }

}
describe('TranslationService', () => {
  let service: TranslationService;
  let TranslateServiceSpy: TranslateService
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: TranslationService, useClass: TranslationService },
        { provide: TranslateService, useClass: TranslateServiceStub }
      ]
    });
    service = TestBed.inject(TranslationService);
    TranslateServiceSpy = TestBed.inject(TranslateService);

    service.initTranslationService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should call use once setCurrentLang is called', () => {
    spyOn(TranslateServiceSpy, 'use');
    service.setCurrentLang('');
    expect(TranslateServiceSpy.use).toHaveBeenCalled();
  });

  it('should call stream once stream is called', () => {
    spyOn(TranslateServiceSpy, 'stream');
    service.stream('');
    expect(TranslateServiceSpy.stream).toHaveBeenCalled();
  });

  it('should call get once get is called', () => {
    spyOn(TranslateServiceSpy, 'get');
    service.get('');
    expect(TranslateServiceSpy.get).toHaveBeenCalled();
  });

  it('should call instant once instant is called', () => {
    spyOn(TranslateServiceSpy, 'instant');
    service.instant('');
    expect(TranslateServiceSpy.instant).toHaveBeenCalled();
  });

});
