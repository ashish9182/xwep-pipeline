import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';import { of } from 'rxjs';

describe('HttpService', () => {
  let service: HttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers : [
        {provide : HttpService , useValues : HttpService},
        // {provide : HttpClient , useValue : HttpClient}
      ],
      imports:[HttpClientTestingModule]
    });
    service = TestBed.inject(HttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('call get and http get method is called',()=>{
    service.get().subscribe(users => {
    });
    const req = httpMock.expectOne('/api/');
    expect(req.request.method).toBe("GET");
    req.flush({data : {}});

  });

  it('call post and http post method is called',()=>{
    service.post('',{data : {}}).subscribe(users => {
    });
    const req = httpMock.expectOne('/api/');
    expect(req.request.method).toBe("POST");
  });


  it('call put and http put method is called',()=>{
    service.put('',{data : {}}).subscribe(users => {
    });
    const req = httpMock.expectOne('/api/');
    expect(req.request.method).toBe("PUT");
  });

  it('call delete and http delete method is called',()=>{
    service.delete('', {}).subscribe(users => {
    });
    const req = httpMock.expectOne('/api/');
    expect(req.request.method).toBe("DELETE");
  });

});
