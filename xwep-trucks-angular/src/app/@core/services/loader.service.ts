import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()

export class LoaderService {

  private _loaderStatusSource = new Subject();
  loaderStatus$ = this._loaderStatusSource.asObservable();
  apiCount: number = 0;

  constructor() { }

  updateLoaderStatus(status) {
    this.apiCount = status ? this.apiCount + 1 : this.apiCount -1 ;
    let loaderFlag = this.apiCount > 0 ? true : false;
    this._loaderStatusSource.next(loaderFlag);
    // this._loaderStatusSource.next(status);
  }
}
