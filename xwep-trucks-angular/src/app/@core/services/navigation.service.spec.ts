import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { NavigationService } from './navigation.service';

class RouterStub {
  navigate(){}
}

describe('NavigationService', () => {
  let service: NavigationService;
  let RouterSpy : Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide : NavigationService , useClass : NavigationService},
        {provide : Router, useClass: RouterStub}
      ]
    });
    service = TestBed.inject(NavigationService);
    RouterSpy = TestBed.inject(Router);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call goto subscription and navigate should happen', () => {
    spyOn(RouterSpy, 'navigate');
    service.goToSubscriptions();
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  it('should call goto subscription and navigate should happen', () => {
    spyOn(RouterSpy, 'navigate');
    service.goToAllSubscriptions();
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });
});
