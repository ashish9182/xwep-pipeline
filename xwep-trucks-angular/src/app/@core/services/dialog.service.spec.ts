import { TestBed } from '@angular/core/testing';

import { DialogService } from './dialog.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { OverlayModule } from '@angular/cdk/overlay';
import { AddSubscriptionComponent } from '@core/components/dialog';


describe('DialogService', () => {
  let service: DialogService<any>;
  let MatDialogSpy: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide :DialogService, useClass : DialogService},
        {provide : MatDialog , useClass : MatDialog}
      ],
      imports: [
        OverlayModule,
        MatDialogModule
    ],
    });
    service = TestBed.inject(DialogService);
    MatDialogSpy = TestBed.inject(MatDialog);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call open if add subscription is called', () => {
    spyOn(service, 'open');
    service.addNewSubscription();
    expect(service.open).toHaveBeenCalled();
  });

  it('should call open if editSubscription is called', () => {
    spyOn(service, 'open');
    service.editSubscription({});
    expect(service.open).toHaveBeenCalled();
  });

  it('should call open if viewSubscription is called', () => {
    spyOn(service, 'open');
    service.viewSubscription({});
    expect(service.open).toHaveBeenCalled();
  });

  it('should call open if confirmDeleteSubscription is called', () => {
    spyOn(service, 'open');
    service.confirmDeleteSubscription({});
    expect(service.open).toHaveBeenCalled();
  });

  it('should call open if confirmGssnSync is called', () => {
    spyOn(service, 'open');
    service.confirmGssnSync();
    expect(service.open).toHaveBeenCalled();
  });

  it('should call open ', () => {
    service.open(AddSubscriptionComponent);
    expect(service.open.name).toEqual('open');
  });

});
