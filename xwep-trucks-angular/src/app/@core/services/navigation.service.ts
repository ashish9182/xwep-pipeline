import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RoutePaths } from '@core/constants';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private _router: Router) { }

  /**
   * Navigate to Subscriptions
   */

  goToSubscriptions() {
    this._router.navigate([RoutePaths.subscriptions])
  }

  /**
   * Navigate to All Subscriptions
   */
  goToAllSubscriptions() {
    this._router.navigate([RoutePaths.viewAllSubscriptions])
  }
}
