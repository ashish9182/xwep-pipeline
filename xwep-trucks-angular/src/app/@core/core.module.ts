import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { SharedModule } from '@shared/shared.module';

import {
  ApiService,
  AuthService,
  DataService,
  DialogService,
  HttpService,
  TranslationService,
  NavigationService,
  RoleService,
} from './services';
import { TokenInterceptor, ErrorInterceptor } from './interceptors';

import { HeaderComponent } from './components/header/header.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DatatableComponent } from './components/datatable/datatable.component';

// Dialog Components
import { AddSubscriptionComponent } from './components/dialog';
import { EditSubscriptionComponent } from './components/dialog/edit-subscription/edit-subscription.component';

import { PaginationComponent } from './components/pagination/pagination.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { SubscriptionsTableComponent } from './components/tables/subscriptions-table/subscriptions-table.component';
import { ViewSubscriptionComponent } from './components/dialog/view-subscription/view-subscription.component';
import { LoaderComponent } from './components/loader/loader.component';
import { DeleteSubscriptionConfirmComponent } from './components/dialog/delete-subscription-confirm/delete-subscription-confirm.component';
import { DivisionAggregateComponent } from './components/charts/division-aggregate/division-aggregate.component';
import { LanguageSelectComponent } from './components/language-select/language-select.component';
import { UserOptionsComponent } from './components/user-options/user-options.component';
import { GssnSyncConfirmComponent } from './components/dialog/gssn-sync-confirm/gssn-sync-confirm.component';
import { MpcCountrySelectComponent } from './components/mpc-country-select/mpc-country-select.component';


@NgModule({
  declarations: [
    HeaderComponent,
    NavbarComponent,
    DatatableComponent,
    AddSubscriptionComponent,
    PaginationComponent,
    StepperComponent,
    EditSubscriptionComponent,
    SubscriptionsTableComponent,
    ViewSubscriptionComponent,
    LoaderComponent,
    DeleteSubscriptionConfirmComponent,
    DivisionAggregateComponent,
    LanguageSelectComponent,
    UserOptionsComponent,
    GssnSyncConfirmComponent,
    MpcCountrySelectComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ApiService,
    AuthService,
    DataService,
    DialogService,
    HttpService,
    TranslationService,
    NavigationService,
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    DatatableComponent,
    AddSubscriptionComponent,
    EditSubscriptionComponent,
    PaginationComponent,
    StepperComponent,
    SubscriptionsTableComponent,
    LoaderComponent,
    DivisionAggregateComponent,
    UserOptionsComponent
  ]
})
export class CoreModule { }
