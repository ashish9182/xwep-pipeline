import { TestBed } from '@angular/core/testing';

import { ErrorInterceptor } from './error.interceptor';

import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiService } from '../services/api.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpService } from '../services/http.service';
import { throwError } from 'rxjs';
import { HttpEvent } from '@angular/common/http';
import { TranslationService } from '@core/services';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule,
} from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';


describe('ErrorInterceptor', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;
  let TranslationServiceSpy: TranslationService;
  let ToastrServiceSpy: jasmine.SpyObj<ToastrService>;


  beforeEach(() => {
    const spy = jasmine.createSpyObj('TranslationService', [
      'initTranslationService',
    ]);

    const ToastSpy = jasmine.createSpyObj('ToastrService', [
      'success',
      'error',
      'clear'
    ]);

    TestBed.configureTestingModule({
      providers: [
        ErrorInterceptor,
        { provide: ApiService, useValues: ApiService },
        { provide: HttpService, useValues: HttpService },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true,
        },
        {
          provide: TranslationService,
          useValue: TranslationService,
        },
        { provide: ToastrService, useValue: ToastSpy },
      ],
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        }),
      ],
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
    TranslationServiceSpy = TestBed.inject(TranslationService);
    ToastrServiceSpy = TestBed.inject(
      ToastrService
    ) as jasmine.SpyObj<ToastrService>;
  });

  it('should be created', () => {
    const interceptor: ErrorInterceptor = TestBed.inject(ErrorInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should add an Authorization header - ErrorEvent instance', async () => {
    service.getAllSubscriptionsData().subscribe((data) => {});

    const httpRequest = httpMock.expectOne(`/api/mcs`);
    httpRequest.error(new ErrorEvent('Unauthorized error'));
    expect(httpRequest.request.url).toEqual(`/api/mcs`);
  });

  it('should add an Authorization header - not ErrorEvent instance', () => {
    service.getAllSubscriptionsData().subscribe( data => fail('should have failed with the 401 error'),
    (error: HttpErrorResponse) => {
      expect(error.status).toEqual(401, 'status');
    });
    
    const httpRequest = httpMock.expectOne(`/api/mcs`);
    httpRequest.flush('deliberate 401 error', { status: 401, statusText: 'Unauthorized' })
    expect(httpRequest.request.url).toEqual(`/api/mcs`);
  });
});
