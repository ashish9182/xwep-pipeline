import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Injector } from '@angular/core';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next
      .handle(request)

      .pipe(
        // retry(1),

        catchError((error: HttpErrorResponse) => {
          let customError = {
            status: 101,
            message: 'Something went wrong',
          };

          const toster = this.injector.get(ToastrService);
          const translateService = this.injector.get(TranslateService);
          let errorHandled = false;

          if (error.error instanceof ErrorEvent) {
            // client-side error

            customError.message = `Error: ${error.error.message}`;
          } else {
            // server-side error
            customError.status = error.status;
            customError.message = `Error Code: ${error.status}\nMessage: ${error.message}`;

            if (error.status === 401 || error.status === 403) {
              const reloadMessage: any = translateService.instant(
                'subscriptions.PLEASE_RELOAD_PAGE'
              );
              toster.clear();
              toster.error(
                reloadMessage,
                translateService.instant('subscriptions.ERROR')
              );
              errorHandled = true;
            }
          }

          if (!errorHandled) {
            toster.clear();
            toster.error(translateService.instant('subscriptions.ERROR'));
          }

          console.error(customError);

          return throwError(customError);
        })
      );
  }
}
