import { TestBed } from '@angular/core/testing';

import { TokenInterceptor } from './token.interceptor';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiService } from '../services/api.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpService } from '../services/http.service';

describe('TokenInterceptor', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TokenInterceptor,
        {provide : ApiService , useValues : ApiService},
        {provide : HttpService , useValues : HttpService},

        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        },
        ],
      imports:[HttpClientTestingModule]
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });



  it('should be created', () => {
    const interceptor: TokenInterceptor = TestBed.inject(TokenInterceptor);
    expect(interceptor).toBeTruthy();
  });


  it('should add an Authorization header', async() => {
    service.getAllSubscriptionsData().subscribe((data)=>{
      
    });

    const httpRequest = httpMock.expectOne(`/api/mcs`);
    expect(httpRequest.request.headers.has('Authorization')).toEqual(false);
  });

});
