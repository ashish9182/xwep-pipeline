export const SUBSCRIPTION_OVERVIEW_CHART = {
    entries: [
        'fullyEquipped',
        'partiallyEquipped',
        'fullyEquippedWithoutSubscription',
        'partiallyEquippedWithoutSubscription',
        'noSubscription'
    ],
    labels: {
        // fullyEquipped: 'Fully Equipped',
        // partiallyEquipped: 'Partially Equipped',
        // fullyEquippedWithoutSubscription: 'Fully Equipped Without Subscription',
        // partiallyEquippedWithoutSubscription: 'Partially Equipped Without Subscription'
        fullyEquipped: 'subscriptions.equipmentType.FULL_EQUIPMENT',
        partiallyEquipped: 'subscriptions.equipmentType.PARTIAL_EQUIPMENT',
        fullyEquippedWithoutSubscription: 'subscriptions.equipmentType.FULL_EQUIPMENT_WITHOUT_SUBSCRIPTION',
        partiallyEquippedWithoutSubscription: 'subscriptions.equipmentType.PARTIAL_EQUIPMENT_WITHOUT_SUBSCRIPTION',
        noSubscription: 'subscriptions.equipmentType.NO_SUBSCRIPTION'
    },
    colors: {
        fullyEquipped: '#6ea046',
        partiallyEquipped: '#c8ddb4',
        fullyEquippedWithoutSubscription: '#e5923a',
        partiallyEquippedWithoutSubscription: '#fae6d2',
        noSubscription: '#ff5252'
    },
    backgroundColor: '#fff'
}