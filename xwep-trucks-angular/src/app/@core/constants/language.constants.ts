export const LANGUAGE_LIST = [
    'de',
    'en',
    'fr',
    'it',
    'es',
    'zh'
];
export const LANGUAGE_LABELS = {
    de: 'German',
    en: 'English',
    fr: 'French',
    it: 'Italian',
    es: 'Spanish',
    zh: 'Chinese'
};