export const API_PATHS = {
    manageSubscriptions: 'manage-subscription',
    manageSubscriptionsByNationality: 'manage-subscriptions',
    allSubscriptions: 'mcs',
    manageSubscriptionsInfo: 'mcs/info',
    subscriptionsAggregate: 'mcs/aggregate',
    downloadMPCDealers: 'download/mpc-dealers',
    downloadHQDealers: 'download/hq-dealers',
    dealerDataSynch: 'manage-subscription/synch',
    logoutUrl: '/logout',
    dealerMySubscription: 'manage-subscription/subscription-status'
}