export const RoutePaths = {
    dashboard: 'dashboard',
    subscriptions: 'subscriptions',
    decisionCommittee: 'decision-committee',
    marketSurvey: 'market-survey',
    campaigns: 'campaigns',
    tenantPreconfiguration: 'tenant-preconfiguration',
    viewAllSubscriptions: 'all-subscriptions',
    subscriptionType: 'subscription-type',
    mySubscriptions: 'my-subscriptions',
    informationSection: 'information-section',
    error: 'error'
}