import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
} from '@angular/core';

import { API_PATHS } from '@core/constants';

@Component({
  selector: 'app-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.scss'],
})
export class UserOptionsComponent implements OnInit {
  constructor(private _eRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this._eRef.nativeElement.contains(event.target)) {
      this.userMenuOpen = false;
    }
  }

  @Input() userName:string = 'username';

  userMenuOpen: boolean = false;

  ngOnInit(): void {}

  toggleUserMenu() {
    this.userMenuOpen = !this.userMenuOpen;
  }

  handleLogout() {
    window.location.replace(API_PATHS.logoutUrl)
  }
}
