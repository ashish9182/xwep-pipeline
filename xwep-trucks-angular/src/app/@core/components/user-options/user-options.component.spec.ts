import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOptionsComponent } from './user-options.component';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { Inject } from '@angular/core';

describe('UserOptionsComponent', () => {
  let component: UserOptionsComponent;
  let fixture: ComponentFixture<UserOptionsComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOptionsComponent ],
      imports: [
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ],
    })
    .compileComponents();
    
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toogle userMenuOpen if toggleUserMenu is called', () => {
    component.userMenuOpen = false;
    component.toggleUserMenu();
    expect(component.userMenuOpen).toBe(true);
  });

  it('should call handle logout - currently no action performed', () => {
    spyOn(component, 'handleLogout');
    component.handleLogout();
    expect(component.handleLogout).toHaveBeenCalled();
  });

});
