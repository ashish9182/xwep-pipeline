import { RoutePaths } from '@core/constants';
import has from 'lodash-es/has';

export const NAVBAR_TYPES = {
    dashboard: 'dashboard',
    manageSubscription: 'manageSubscription',
    decisionCommittee: 'decisionCommittee',
    marketSurvey: 'marketSurvey',
    campaigns: 'campaigns',
    tenantPreconfiguration: 'tenantPreconfiguration',
    subscriptions: 'subscriptions',
    subscriptionType: 'subscriptionType',
    viewAllSubscriptions: 'viewAllSubscriptions',
    mySubscriptions: 'mySubscriptions',
    informationSection: 'informationSection'
}


const NavbarConfig = {
    default: [
        // {
        //     type: NAVBAR_TYPES.dashboard,
        //     title: 'navbar.DASHBOARD',
        //     path: `/${RoutePaths.dashboard}`,
        //     icon: 'nav-dashboard.svg',
        //     activeIcon: 'nav-dashboard-active.svg',
        //     classList: ['nav-dashboard'],
        //     disabled: true
        // },
        {
            type: NAVBAR_TYPES.manageSubscription,
            title: 'navbar.MANAGE_SUBSCRIPTIONS',
            path: `/${RoutePaths.subscriptions}`,
            icon: 'nav-subscriptions.svg',
            activeIcon: 'nav-subscriptions-active.svg',
            classList: ['nav-subscriptions'],
            entitlementType  :"VIEW_OWN_SUBSCRIPTION"

        },
        // {
        //     type: NAVBAR_TYPES.decisionCommittee,
        //     title: 'navbar.DECISION_COMMITTEE',
        //     path: `/${RoutePaths.decisionCommittee}`,
        //     icon: 'nav-dc.svg',
        //     activeIcon: 'nav-dc-active.svg',
        //     classList: 'nav-dc',
        //     disabled: true
        // },
        // {
        //     type: NAVBAR_TYPES.marketSurvey,
        //     title: 'navbar.MARKET_SURVEY',
        //     path: `/${RoutePaths.marketSurvey}`,
        //     icon: 'nav-ms.svg',
        //     activeIcon: 'nav-ms-active.svg',
        //     classList: ['nav-ms'],
        //     disabled: true
        // },
    ],

    hq: [
        // {
        //     type: NAVBAR_TYPES.dashboard,
        //     title: 'navbar.DASHBOARD',
        //     path: `/${RoutePaths.dashboard}`,
        //     icon: 'nav-dashboard.svg',
        //     activeIcon: 'nav-dashboard-active.svg',
        //     classList: ['nav-dashboard'],
        //     disabled: true
        // },
        // {
        //     type: NAVBAR_TYPES.campaigns,
        //     title: 'navbar.CAMPAIGNS',
        //     path: `/${RoutePaths.campaigns}`,
        //     icon: 'nav-campaigns.svg',
        //     activeIcon: 'nav-campaigns-active.svg',
        //     classList: ['nav-campaigns'],
        //     disabled: true
        // },
        // {
        //     type: NAVBAR_TYPES.tenantPreconfiguration,
        //     title: 'navbar.TENANT_PRECONFIG',
        //     path: `/${RoutePaths.tenantPreconfiguration}`,
        //     icon: 'nav-tenant-preconfig.svg',
        //     activeIcon: 'nav-tenant-preconfig-active.svg',
        //     classList: ['nav-tenant-preconfig'],
        //     disabled: true
        // },
        // {
        //     type: NAVBAR_TYPES.subscriptions,
        //     title: 'navbar.SUBSCRIPTIONS',
        //     path: `/${RoutePaths.subscriptions}`,
        //     icon: 'nav-hq-subscriptions.svg',
        //     activeIcon: 'nav-hq-subscriptions-active.svg',
        //     classList: ['nav-hq-subscriptions'],
        //     hasChildren: true,
        //     children: [
        //         {
        //             type: NAVBAR_TYPES.subscriptionType,
        //             title: 'navbar.SUBSCRIPTION_TYPE',
        //             path: `/${RoutePaths.subscriptionType}`,
        //             icon: 'nav-subscription-type.svg',
        //             activeIcon: 'nav-subscription-type-active.svg',
        //             classList: ['nav-subscription-type'],
        //         }
        //     ]
        // },
        {
            type: NAVBAR_TYPES.viewAllSubscriptions,
            title: 'navbar.VIEW_ALL_SUBSCRIPTIONS',
            path: `/${RoutePaths.viewAllSubscriptions}`,
            icon: 'nav-all-subscriptions.svg',
            activeIcon: 'nav-all-subscriptions-active.svg',
            classList: ['nav-all-subscriptions'],
            relativePath: RoutePaths.subscriptions,
            entitlementType  : "VIEW_ALL_SUBSCRIPTION"
        }

    ],
    dealer: [
        // {
        //     type: NAVBAR_TYPES.dashboard,
        //     title: 'navbar.DASHBOARD',
        //     path: `/${RoutePaths.dashboard}`,
        //     icon: 'nav-dashboard.svg',
        //     activeIcon: 'nav-dashboard-active.svg',
        //     classList: ['nav-dashboard'],
        //     disabled: true

        // },
        // {
        //     type: NAVBAR_TYPES.marketSurvey,
        //     title: 'navbar.MARKET_SURVEY',
        //     path: `/${RoutePaths.marketSurvey}`,
        //     icon: 'nav-ms.svg',
        //     activeIcon: 'nav-ms-active.svg',
        //     classList: ['nav-ms'],
        //     disabled: true
        // },

        {
            type: NAVBAR_TYPES.mySubscriptions,
            title: 'navbar.MY_SUBSCRIPTIONS',
            path: `/${RoutePaths.mySubscriptions}`,
            icon: 'nav-my-subs.svg',
            activeIcon: 'nav-my-subs-active.svg',
            classList: ['nav-my-subs'],
            entitlementType  : ""
        },
        // {
        //     type: NAVBAR_TYPES.informationSection,
        //     title: 'navbar.INFO_SECTION',
        //     path: `/${RoutePaths.informationSection}`,
        //     icon: 'nav-info.svg',
        //     activeIcon: 'nav-info-active.svg',
        //     classList: ['nav-info'],
        //     disabled: true
        // },

    ]
}



export function getNavBarConfig(role?) {
    if (!role || !has(NavbarConfig, role))
        return NavbarConfig.default;
    else
        return NavbarConfig[role];
}
