import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, 
  // ChangeDetectorRef 
} from '@angular/core';
// import { NavigationService, RoleService } from '@core/services';
import { getNavBarConfig } from './navbar.config';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { RoleService } from '@core/services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {

  navConfig: Array<any>;
  currentRoute: string;

  @Input() open: boolean;
  // @Input() userRole: string;
  @Output() toggleNavMenu = new EventEmitter<boolean>();
  
  userRole: string;
  
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _roleService : RoleService,
    private cd: ChangeDetectorRef
  ) {
    this.currentRoute = this._router.url.split('?')[0];
    this.userRole = this._roleService.getUserRole();
    this.navConfig = getNavBarConfig(this.userRole);
  }

  authList = [];

  ngOnInit(): void {
    this.authList = this._roleService.getAuthorizationList();
    this._router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.currentRoute = event.url;
      this.cd.detectChanges();
    })
  }

  getIconPath(item) {
    const icon = this.currentRoute.includes(item.path) ? item.activeIcon : item.icon;
    return `/assets/icons/${icon}`;
  }

  toggleMenu() {
    this.toggleNavMenu.emit();
  }

  handleNavigation(navItem) {
    if (!navItem.disabled)
      this._router.navigate([navItem.path])
    // this._router.navigate([navItem.path], { relativeTo: navItem.relativePath || '' })
  }

}
