import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { RoleService } from '@core/services';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let RouterSpy: Router;

  class RouteStub {
    url = "test?role=mpc";
    navigate() { }
    events = of()
  }

  class RoleStub {
    _role = "test";
    getUserRole() { return this._role; }
    getUserData () { return {firstName : 'test'}}
    getAuthorizationList() {return []}
  }

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      providers: [
        { provide: Router, useClass: RouteStub },
        { provide: RoleService, useClass: RoleStub },
        {
          provide: ActivatedRoute, useValue: {
            snapshot: {
              queryParams: {
                role: ""
              }
            }
          }
        }
      ]
    })
      .compileComponents();
    RouterSpy = TestBed.inject(Router);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check if navigate method is called', () => {
    spyOn(RouterSpy, 'navigate');
    component.handleNavigation({ disabled: false })
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  it('should check if navigate method is called', () => {
    spyOn(RouterSpy, 'navigate');
    component.handleNavigation({ disabled: false })
    expect(RouterSpy.navigate).toHaveBeenCalled();
  });

  it('check if function to change the language is called', () => {
    spyOn(component.toggleNavMenu, 'emit');
    component.toggleMenu();
    expect(component.toggleNavMenu.emit).toHaveBeenCalled();
  });

});
