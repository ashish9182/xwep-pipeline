import { Component, Inject, OnInit, Injector } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SubscriptionData } from '@core/interfaces';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from '@core/services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-subscription',
  templateUrl: './edit-subscription.component.html',
  styleUrls: ['./edit-subscription.component.scss']
})
export class EditSubscriptionComponent<T> implements OnInit {

  dealerForm: FormGroup;
  subscriptionForm: FormGroup;
  activeTab: string = 'dealer';
  showValidationMsg: boolean = true;
  isTruckEnabled: boolean = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SubscriptionData,
    private _dialogRef: MatDialogRef<EditSubscriptionComponent<T>>,
    private _fb: FormBuilder,
    private _injector: Injector,
    private _toastr: ToastrService,

  ) {
    this.createForms(this.data);

  }

  ngOnInit(): void {
  }

  createForms(data: SubscriptionData) {
    this.dealerForm = this._fb.group({
      brandCode: [data.brandCode, Validators.required],
      legalName: [data.legalName, Validators.required],
      companyId: [data.companyId, Validators.required],
      email: [data.email],
      emailLKW: [data.emailLKW],
      nationality: [data.nationality],
      outletId: [data.outletId, Validators.required],
      place: [data.place, Validators.required],
      postCode: [data.postCode, Validators.required],
      road: [data.road, Validators.required],
    });
    
    const {
      truck,
    }: any = data.subscriptionStatus;

    this.subscriptionForm = this._fb.group({
      truckSubscribed: [truck.subscribed || false],
      truckEquipmentType: [truck.equipmentType],
      truckSubType: [''],
    });

    this.watchSubscriptionStatusChange();
  }

  watchSubscriptionStatusChange() {
    // Truck
    this.subscriptionForm.get('truckSubscribed').valueChanges.subscribe((subscribed) => {
      const truckEquipmentTypeField = this.subscriptionForm.get('truckEquipmentType');
      if (subscribed) {
        if (!truckEquipmentTypeField.value || truckEquipmentTypeField.value == 'NA')
          truckEquipmentTypeField.setValue('PARTIAL_EQUIPMENT')
      }
    })
  }


  validateForms() {
    // if (this.dealerForm.valid)
    this.saveChanges();
    // else
    //   this.activeTab = 'dealer';
  }

  saveChanges() {
    const { value: dealerData } = this.dealerForm;
    const { value: subData } = this.subscriptionForm;
    const subscriptions = [];
    const {truckSubscribed} = this.subscriptionForm.controls;

    subscriptions.push({
      id: this.data.subscriptionStatus['truck'].id,
      divisionName: 'TRUCK',
      equipmentType: this.subscriptionForm.controls.truckEquipmentType.value || 'NA',
      subscribed: truckSubscribed.value
    });

    this._dialogRef.close({
      type: 'edit',
      payload: {
        ...this.data,
        ...dealerData,
        subscriptionStatus: subscriptions
      }
    })

  }

  close() {
    this._dialogRef.close(false);
  }

  deleteSubscription() {
    const dialogService = this._injector.get(DialogService);
    dialogService.confirmDeleteSubscription(this.data).subscribe((res) => {
      if (res) {
        this._dialogRef.close({
          type: 'delete',
          payload: this.data.uuid
        })
      }
    })
  }
  changeActiveTab(tab) {
    this.activeTab = tab;
  }
}
