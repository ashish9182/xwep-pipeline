import { ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { EditSubscriptionComponent } from './edit-subscription.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogService } from '@core/services';
import { FormBuilder } from '@angular/forms';
import {
  MatDialogModule,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NO_ERRORS_SCHEMA } from '@angular/core';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


const mockDialogRef = {
  close: jasmine.createSpy('close')
};

let data = {
  brandCode: null,
  companyId: 'GC0000478',
  email: null,
  emailLKW: null,
  emailPassengerCars: null,
  emailSmartPassengerCars: null,
  emailTransporter: null,
  id: 4397,
  legalName: 'Beresa GmbH & Co. KG',
  nationality: 'DE',
  outletId: 'GS0000764',
  place: null,
  postCode: '48599',
  road: 'Ochtruper Straße',
  status: 'OLD',
  subscriptionStatus: {
    eq: { subscribed: true, equipmentType: 'FULL_EQUIPMENT' },
    passenger: { subscribed: true, equipmentType: 'PARTIAL_EQUIPMENT' },
    truck: { subscribed: true, equipmentType: 'FULL_EQUIPMENT' },
    van: { subscribed: true, equipmentType: 'PARTIAL_EQUIPMENT' },
  },
};

class DialogStub {
  close() { }
}

class DialogServiceStub {
  confirmDeleteSubscription() { return of() }
}

describe('EditSubscriptionComponent', () => {
  let component: EditSubscriptionComponent<any>;
  let fixture: ComponentFixture<EditSubscriptionComponent<any>>;
  let MatDialogSpy: MatDialogRef<EditSubscriptionComponent<any>>;
  let DialogServiceSpy: any;
  let ToastrServiceSpy: jasmine.SpyObj<ToastrService>;

  beforeEach(async () => {
    const TranslationSpy = jasmine.createSpyObj('TranslationService', [
      'initTranslationService',
    ]);

    const DialogSpy = jasmine.createSpyObj('DialogService', [
      'confirmDeleteSubscription',
    ]);

    const ToastSpy = jasmine.createSpyObj('ToastrService', [
      'success',
      'error',
      'clear'
    ]);

    await TestBed.configureTestingModule({
      declarations: [EditSubscriptionComponent],
      providers: [
        { provide: MatDialogRef, useClass: DialogStub },
        { provide: MAT_DIALOG_DATA, useValue: { ...data } },
        FormBuilder,
        { provide: Injector, useValue: Injector },
        { provide: DialogService, useValue: DialogSpy },
        { provide: ToastrService, useValue: ToastSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule
        , TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ],
    }).compileComponents();

    MatDialogSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<
      MatDialogRef<EditSubscriptionComponent<any>>
    >;

    DialogServiceSpy = TestBed.inject(DialogService) as jasmine.SpyObj<
      DialogService<EditSubscriptionComponent<any>>>;

      ToastrServiceSpy = TestBed.inject(
        ToastrService
      ) as jasmine.SpyObj<ToastrService>;
   

  });
  

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubscriptionComponent);
    component = fixture.componentInstance;
    component.watchSubscriptionStatusChange();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save and close dialog once save changes are done', () => {
    spyOn(MatDialogSpy, 'close');
    component.saveChanges();
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });


  it('should save and close dialog once close function is called', () => {
    spyOn(MatDialogSpy, 'close');
    component.close();
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });


  it('should change active tab once changeActiveTab is called', () => {
    component.changeActiveTab('Test');
    expect(component.activeTab).toBe('Test');
  });


  it('should call delete confirm subscription and close dialog', () => {
    DialogServiceSpy.confirmDeleteSubscription.and.callFake(() => of({}));
    component.deleteSubscription();
    expect(DialogServiceSpy.confirmDeleteSubscription).toHaveBeenCalled();
  });

  it('should change tab since the form is not valid', () => {
    component.validateForms();
    expect(component.activeTab).toBe('dealer');
  });


  it('should change tab since the form is not valid', () => {
    component.subscriptionForm.setValue({
      truckSubscribed: true,
      truckEquipmentType: 'PARTIAL_EQUIPMENT',
      truckSubType: [''],
    })
    expect(component.subscriptionForm.value.truckEquipmentType).toBe('PARTIAL_EQUIPMENT');

  });


});
