import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SubscriptionData } from '@core/interfaces';


@Component({
  selector: 'app-delete-subscription-confirm',
  templateUrl: './delete-subscription-confirm.component.html',
  styleUrls: ['./delete-subscription-confirm.component.scss']
})
export class DeleteSubscriptionConfirmComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SubscriptionData,
    private _dialogRef: MatDialogRef<DeleteSubscriptionConfirmComponent>
  ) { }

  ngOnInit(): void {
  }

  confirmDelete() {
    this.closeDialog(true);
  }

  cancelDelete() {
    this.closeDialog(false);
  }

  closeDialog(response?: boolean) {
    this._dialogRef.close(response)
  }

}
