import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSubscriptionConfirmComponent } from './delete-subscription-confirm.component';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';

class DialogStub{
  close() {}
}

describe('DeleteSubscriptionConfirmComponent', () => {
  let component: DeleteSubscriptionConfirmComponent;
  let fixture: ComponentFixture<DeleteSubscriptionConfirmComponent>;
  let MatDialogSpy: MatDialogRef<DeleteSubscriptionConfirmComponent>;

  

  beforeEach(async () => {
    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);
    await TestBed.configureTestingModule({
      declarations: [ DeleteSubscriptionConfirmComponent ],
      providers: [
        { provide: MatDialogRef, useClass: DialogStub },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: TranslationService, useValue: TranslationSpy },

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader,
        },
      })]
    })
    .compileComponents();
    MatDialogSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<DeleteSubscriptionConfirmComponent>>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSubscriptionConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check if dialog close is called', () => {
    spyOn(MatDialogSpy, 'close')
    component.closeDialog(true);
    fixture.detectChanges()
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });

  it('should check if dialog close is called if cancel delete is called', () => {
    spyOn(MatDialogSpy, 'close')
    component.cancelDelete();
    fixture.detectChanges()
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });

  
  it('should check if dialog close is called if confirm delete is called', () => {
    spyOn(MatDialogSpy, 'close')
    component.confirmDelete();
    fixture.detectChanges()
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });


});
