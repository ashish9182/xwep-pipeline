import { Component, OnInit } from '@angular/core';
// import { MatDialogRef } from '@angular/material/dialog';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';

const steps = [
  {
    id: 'dealer',
    title: 'Dealer Data',
    template: 'dealerForm'
  },
  {
    id: 'subscription',
    title: 'Subscription Data',
    template: 'subscriptionForm'
  },
];
@Component({
  selector: 'app-add-subscription',
  templateUrl: './add-subscription.component.html',
  styleUrls: ['./add-subscription.component.scss']
})
export class AddSubscriptionComponent implements OnInit {

  // isLastStep: boolean = false;
  // isFirstStep: boolean = true;
  // activeStep: any = steps[0];
  // activeStepIndex: number = 0;

  // dealerForm: FormGroup;
  // subscriptionForm: FormGroup;

  // stepperConfig: string[];
  // showValidationMsg: boolean = true;

  constructor(
  //   private _fb: FormBuilder,
  //   private _dialogRef: MatDialogRef<AddSubscriptionComponent>
  ) {
  //   this.stepperConfig = steps.map(el => el.title);
  //   this.createForms();
  }
  // createForms() {
  //   this.dealerForm = this._fb.group({
  //     brandCode: ['', Validators.required],
  //     legalName: ['', Validators.required],
  //     companyId: ['', Validators.required],
  //     email: [''],
  //     emailLKW: [''],
  //     emailPassengerCars: [''],
  //     emailSmartPassengerCars: [''],
  //     emailTransporter: [''],
  //     nationality: [''],
  //     outletId: ['', Validators.required],
  //     place: [''],
  //     postCode: ['', Validators.required],
  //     road: ['', Validators.required],
  //   });
  //   this.subscriptionForm = this._fb.group({
  //     eqSubscribed: [false],
  //     truckSubscribed: [false],
  //     vanSubscribed: [false],
  //     passengerSubscribed: [false],
  //     eqEquipmentType: [''],
  //     truckEquipmentType: [''],
  //     vanEquipmentType: [''],
  //     passengerEquipmentType: [''],
  //     eqSubType: [''],
  //     truckSubType: [''],
  //     vanSubType: [''],
  //     passengerSubType: ['']
  //   });

  //   this.watchSubscriptionStatusChange();
  // }

  ngOnInit(): void {

  }

  // watchSubscriptionStatusChange() {
  //   // EQ
  //   this.subscriptionForm.get('eqSubscribed').valueChanges.subscribe((subscribed) => {
  //     const eqEquipmentTypeField = this.subscriptionForm.get('eqEquipmentType');
  //     if (subscribed) {
  //       if (!eqEquipmentTypeField.value || eqEquipmentTypeField.value == 'NA')
  //         eqEquipmentTypeField.setValue('PARTIAL_EQUIPMENT')
  //     }
  //   })
  //   // Truck
  //   this.subscriptionForm.get('truckSubscribed').valueChanges.subscribe((subscribed) => {
  //     const truckEquipmentTypeField = this.subscriptionForm.get('truckEquipmentType');
  //     if (subscribed) {
  //       if (!truckEquipmentTypeField.value || truckEquipmentTypeField.value == 'NA')
  //         truckEquipmentTypeField.setValue('PARTIAL_EQUIPMENT')
  //     }
  //   })
  //   // Van
  //   this.subscriptionForm.get('vanSubscribed').valueChanges.subscribe((subscribed) => {
  //     const vanEquipmentTypeField = this.subscriptionForm.get('vanEquipmentType');
  //     if (subscribed) {
  //       if (!vanEquipmentTypeField.value || vanEquipmentTypeField.value == 'NA')
  //         vanEquipmentTypeField.setValue('PARTIAL_EQUIPMENT')
  //     }
  //   })
  //   // Passenger
  //   this.subscriptionForm.get('passengerSubscribed').valueChanges.subscribe((subscribed) => {
  //     const passengerEquipmentTypeField = this.subscriptionForm.get('passengerEquipmentType');
  //     if (subscribed) {
  //       if (!passengerEquipmentTypeField.value || passengerEquipmentTypeField.value == 'NA')
  //         passengerEquipmentTypeField.setValue('PARTIAL_EQUIPMENT')
  //     }
  //   })
  // }

  // handlePrev() {
  //   const { activeStepIndex } = this;
  //   const prevIndex = activeStepIndex - 1;
  //   if (!prevIndex) {
  //     this.isFirstStep = true;
  //   }
  //   this.isLastStep = false;
  //   this.activeStepIndex = prevIndex;
  //   this.activeStep = steps[prevIndex];
  // }

  // handleNext() {
  //   const { activeStepIndex, isLastStep } = this;
  //   const nextIndex = activeStepIndex + 1;
  //   if (isLastStep) {
  //     this.createDataObject();
  //   }
  //   else if (this.dealerForm.valid) {
  //     this.activeStepIndex = nextIndex;
  //     this.activeStep = steps[nextIndex];
  //     this.isLastStep = (nextIndex + 1) === steps.length
  //   } else {
  //     this.showValidationMsg = true;
  //   }
  // }
  // createDataObject() {
  //   const { value: dealerData } = this.dealerForm;
  //   const { value: subData } = this.subscriptionForm;
  //   const subscriptions = [];
  //   const { eqSubscribed, truckSubscribed, vanSubscribed, passengerSubscribed } = this.subscriptionForm.controls;
  //   subscriptions.push({
  //     divisionName: 'EQ',
  //     equipmentType: this.subscriptionForm.controls.eqEquipmentType.value || 'NA',
  //     subscribed: eqSubscribed.value
  //   });
  //   subscriptions.push({
  //     divisionName: 'PASSENGER',
  //     equipmentType: this.subscriptionForm.controls.passengerEquipmentType.value || 'NA',
  //     subscribed: passengerSubscribed.value
  //   });
  //   subscriptions.push({
  //     divisionName: 'TRUCK',
  //     equipmentType: this.subscriptionForm.controls.truckEquipmentType.value || 'NA',
  //     subscribed: truckSubscribed.value
  //   });
  //   subscriptions.push({
  //     divisionName: 'VAN',
  //     equipmentType: this.subscriptionForm.controls.vanEquipmentType.value || 'NA',
  //     subscribed: vanSubscribed.value
  //   });

  //   this._dialogRef.close({ ...dealerData, subscriptionStatus: subscriptions, nationality: 'D' })
  // }

  // close() {
  //   this._dialogRef.close(false);
  // }
}
