import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GssnSyncConfirmComponent } from './gssn-sync-confirm.component';
import {
  MatDialogModule,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
describe('GssnSyncConfirmComponent', () => {
  let component: GssnSyncConfirmComponent;
  let fixture: ComponentFixture<GssnSyncConfirmComponent>;
  let MatDialogSpy: MatDialogRef<GssnSyncConfirmComponent>;

  class DialogStub {
    close() { }
  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GssnSyncConfirmComponent ],
      providers :[ { provide: MatDialogRef, useClass: DialogStub },],
      imports: [
        MatDialogModule
        , TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ],
    })
    .compileComponents();

    MatDialogSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<
      MatDialogRef<GssnSyncConfirmComponent>
    >;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GssnSyncConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call close Dialog is comfirmsync is called', () => {
    spyOn(component, 'closeDialog');
    component.confirmSync();
    expect(component.closeDialog).toHaveBeenCalled();
  });


  it('should call close Dialog is comfirmsync is called', () => {
    spyOn(component, 'closeDialog');
    component.cancelSync();
    expect(component.closeDialog).toHaveBeenCalled();
  });

  
  it('should call close Dialog is closeDialog is called', () => {
    spyOn(MatDialogSpy, 'close');
    component.closeDialog();
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });

});
