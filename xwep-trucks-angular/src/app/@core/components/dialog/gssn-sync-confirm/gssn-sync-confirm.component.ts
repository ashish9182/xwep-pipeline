import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-gssn-sync-confirm',
  templateUrl: './gssn-sync-confirm.component.html',
  styleUrls: ['./gssn-sync-confirm.component.scss'],
})
export class GssnSyncConfirmComponent implements OnInit {
  constructor(
    private _dialogRef: MatDialogRef<GssnSyncConfirmComponent>
  ) {}

  ngOnInit(): void {}

  confirmSync() {
    this.closeDialog(true);
  }

  cancelSync() {
    this.closeDialog(false);
  }

  closeDialog(response?: boolean) {
    this._dialogRef.close(response)
  }
}
