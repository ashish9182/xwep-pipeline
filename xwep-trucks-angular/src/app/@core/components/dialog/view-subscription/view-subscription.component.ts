import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SubscriptionData } from '@core/interfaces';

@Component({
  selector: 'app-view-subscription',
  templateUrl: './view-subscription.component.html',
  styleUrls: ['./view-subscription.component.scss']
})

export class ViewSubscriptionComponent implements OnInit {

  activeTab: string = 'dealer';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SubscriptionData,
    private _dialogRef: MatDialogRef<ViewSubscriptionComponent>,
  ) { }

  ngOnInit(): void {
  }

  close() {
    this._dialogRef.close(false);
  }

  changeActiveTab(tab) {
    this.activeTab = tab;
  }

}
