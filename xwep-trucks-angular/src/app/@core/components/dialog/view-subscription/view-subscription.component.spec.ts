import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSubscriptionComponent } from './view-subscription.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
class DialogStub{
  close() {}
}
describe('ViewSubscriptionComponent', () => {
  let component: ViewSubscriptionComponent;
  let fixture: ComponentFixture<ViewSubscriptionComponent>;
  let MatDialogSpy: MatDialogRef<ViewSubscriptionComponent>;

  beforeEach(async () => {
    let MatDialogRefSpy: MatDialogRef<ViewSubscriptionComponent>;
    // const MAT_DIALOG_DATA = jasmine.createSpyObj('MAT_DIALOG_DATA', []);
    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['initTranslationService']);

    await TestBed.configureTestingModule({
      declarations: [ViewSubscriptionComponent],
      providers: [
        { provide: MatDialogRef, useClass: DialogStub },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: TranslationService, useValue: TranslationSpy },

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader,
        },
      })]
    })
      .compileComponents();

    MatDialogSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<ViewSubscriptionComponent>>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check if dialog close is called', () => {
    spyOn(MatDialogSpy, 'close')
    component.close();
    fixture.detectChanges()
    expect(MatDialogSpy.close).toHaveBeenCalled();
  });

  it('check if activation tab is changed', () => {
    component.changeActiveTab(2);
    fixture.detectChanges()
    expect(component.activeTab + '').toEqual('2');
  });

});
