import { Component, OnInit, Input, Output, HostListener, EventEmitter, ElementRef } from '@angular/core';
import {COUNTRY_NAMES} from '@core/constants/country-constants'
@Component({
  selector: 'app-mpc-country-select',
  templateUrl: './mpc-country-select.component.html',
  styleUrls: ['./mpc-country-select.component.scss']
})
export class MpcCountrySelectComponent implements OnInit {

  @Input() currentCountry: string;
  @Output() countryChange = new EventEmitter<string>();
  @Input() countryList: string[];

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this._eRef.nativeElement.contains(event.target)) 
      this.menuOpen = false;
  }

  countryListLabels = COUNTRY_NAMES;
  menuOpen: boolean = false;


  constructor(
    private _eRef: ElementRef
  ) { }

  ngOnInit(): void {
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }
  
  handleCountryChange(e,country) {
    e.stopPropagation();
    this.countryChange.emit(country);
    this.menuOpen = false;
  }

}
