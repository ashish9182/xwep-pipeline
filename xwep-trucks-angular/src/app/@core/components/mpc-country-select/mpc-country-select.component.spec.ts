import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcCountrySelectComponent } from './mpc-country-select.component';

describe('MpcCountrySelectComponent', () => {
  let component: MpcCountrySelectComponent;
  let fixture: ComponentFixture<MpcCountrySelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MpcCountrySelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcCountrySelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toogle toogleMenu field once clicked', () => {
    let toogleMenu = component.menuOpen;
    component.toggleMenu();
    expect(component.menuOpen).toBe(!toogleMenu);
   });
 
   it('should change country and menu should be false', () => {
     component.handleCountryChange({stopPropagation(){}}, 'IND');
     expect(component.menuOpen).toBe(false);
    });

});
