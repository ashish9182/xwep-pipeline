import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageSelectComponent } from './language-select.component';
import { of, Subject } from 'rxjs';
import { RoleService } from '@core/services';

describe('LanguageSelectComponent', () => {
  let component: LanguageSelectComponent;
  let fixture: ComponentFixture<LanguageSelectComponent>;
  let RoleServiceSpy: RoleService;


  class RoleStub {
    _role = "test";
    public countryEmmitter = new Subject();
    public languageEmmitter = new Subject();

    getUserRole() { return this._role; }
    getUserData() { return { firstName: 'test' } }
    getCurrentCountry() { return 'IND' }
    getCountryList() { return [] }
    updateCurrentCountry(data) {}
    getLanguageList() {return []}
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageSelectComponent ],
      providers:[
        { provide: RoleService, useClass: RoleStub }
      ]
    })
    .compileComponents();

    RoleServiceSpy = TestBed.inject(RoleService) as jasmine.SpyObj<RoleService>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageSelectComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    RoleServiceSpy.languageEmmitter.next([]);
    expect(component).toBeTruthy();
  });

  it('should toogle toogleMenu field once clicked', () => {
   let toogleMenu = component.menuOpen;
   component.toggleMenu();
   expect(component.menuOpen).toBe(!toogleMenu);
  });

  it('should change language and menu should be false', () => {
    component.handleLanguageChange({stopPropagation(){}}, 'en');
    expect(component.menuOpen).toBe(false);
   });
});
