import { Component, EventEmitter, Input, OnInit, Output, HostListener, ElementRef } from '@angular/core';
import { LANGUAGE_LABELS, LANGUAGE_LIST } from '@core/constants';
import { RoleService } from '@core/services';

@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html',
  styleUrls: ['./language-select.component.scss']
})
export class LanguageSelectComponent implements OnInit {

  @Input() currentLanguage: string = 'en';
  @Output() langchange = new EventEmitter<string>();

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this._eRef.nativeElement.contains(event.target)) 
      this.menuOpen = false;
  }

  menuOpen: boolean = false;

  languageList: string[] = LANGUAGE_LIST;
  languageLabels: any = LANGUAGE_LABELS;

  constructor(
    private _eRef: ElementRef,
    private roleService : RoleService
  ) { 
    this.languageList = roleService.getLanguageList();
  }

  ngOnInit(): void {
    this.roleService.languageEmmitter.subscribe((data: any) => {
      this.languageList = data?.length ? data : LANGUAGE_LIST;
    });
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }
  
  handleLanguageChange(e,lang) {
    e.stopPropagation();
    this.langchange.emit(lang);
    this.menuOpen = false;
  }
}
