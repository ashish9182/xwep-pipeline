import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { of, Subject } from 'rxjs';
import { RoleService } from '@core/services';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let TranslationServiceSpy: jasmine.SpyObj<TranslationService>;
  let RoleServiceSpy: any

  class RoleStub {
    _role = "test";
    public countryEmmitter = new Subject();

    getUserRole() { return this._role; }
    getUserData() { return { firstName: 'test' } }
    getCurrentCountry() { return 'IND' }
    getCountryList() { return [] }
    updateCurrentCountry(data) {}

  }

  beforeEach(async () => {
    const TranslationSpy = jasmine.createSpyObj('TranslationService', ['getTranslationObservable']);

    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [
        { provide: TranslationService, useValue: { getTranslationObservable() { return of() }, setCurrentLang() { } } },
        { provide: RoleService, useClass: RoleStub }
      ],
      imports: [
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ]
    })
      .compileComponents();
    TranslationServiceSpy = TestBed.inject(TranslationService) as jasmine.SpyObj<TranslationService>;
    RoleServiceSpy = TestBed.inject(RoleService) as jasmine.SpyObj<RoleService>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check if function to change the language is called', () => {
    spyOn(TranslationServiceSpy, 'setCurrentLang');
    component.handleLangChange("en");
    expect(TranslationServiceSpy.setCurrentLang).toHaveBeenCalled();
  });

  it('check if function to change the language is called', () => {
    spyOn(RoleServiceSpy, 'updateCurrentCountry');
    component.handleCountryChange("en");
    expect(RoleServiceSpy.updateCurrentCountry).toHaveBeenCalled();
  });


});
