import { Component, OnInit } from '@angular/core';
import { RoleService, TranslationService } from '@core/services';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  
  userName: string = 'Steffen Koelbel';
  currentCountry: string;
  countryList : string[];
  currentLang: string;
  userRole : string ;

  constructor(private _translationService: TranslationService, private roleService:RoleService) {
    this.userName = this.roleService.getUserData()['firstName']
    this.userRole = this.roleService.getUserRole();
  }


  ngOnInit(): void {
    this._translationService
      .getTranslationObservable()
      .subscribe((lang: string) => {
        this.currentLang = lang;
      });
      this.fetchCountryData();
  }

  fetchCountryData() {
    this.roleService.countryEmmitter.subscribe((data:any)=>{
      this.currentCountry = data;
    });
    this.countryList = this.roleService.getCountryList();
  }

  handleLangChange(lang) {
    this._translationService.setCurrentLang(lang);
  }

  handleCountryChange(country) {
    this.roleService.updateCurrentCountry(country);
  }
}
