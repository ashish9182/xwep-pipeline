export const TableConfig = {
    keys: {
        dealerName: 'dealerName',
        outletId: 'outletId',
        companyId: 'companyId',
        brandCode: 'brandCode',
        address: 'address',
        place: 'place',
        postCode: 'postCode',
        subscriptionStatus: 'subscriptionStatus',

    },
    columns: [
        [
            'dealerName',
            'outletId',
            'companyId',
            'brandCode',
            'address',
            'place',
            'postCode',
            'subscriptionStatus'
        ],

    ],
    columnConfig: {
        dealerName: {
            title: 'subscriptions.subscriptionData.DEALER_NAME',
            rowSpan: 2,
            colSpan: 1,
            key: 'dealerName'
        },
        outletId: {
            title: 'subscriptions.subscriptionData.OUTLET_ID',
            rowSpan: 2,
            colSpan: 1,
            key: 'outletId'
        },
        companyId: {
            title: 'subscriptions.subscriptionData.COMPANY_ID',
            rowSpan: 2,
            colSpan: 1,
            key: 'companyId'
        },
        brandCode: {
            title: 'subscriptions.subscriptionData.BRAND_CODE',
            rowSpan: 2,
            colSpan: 1,
            key: 'brandCode'
        },
        address: {
            title: 'subscriptions.subscriptionData.ROAD',
            rowSpan: 2,
            colSpan: 1,
            key: 'address'
        },
        place: {
            title: 'subscriptions.subscriptionData.PLACE',
            rowSpan: 2,
            colSpan: 1,
            key: 'place'
        },
        postCode: {
            title: 'subscriptions.subscriptionData.POSTCODE',
            rowSpan: 2,
            colSpan: 1,
            key: 'postCode'
        },
        subscriptionStatus: {
            title: 'subscriptions.subscriptionData.SUBSCRIPTION_STATUS',
            rowSpan: 1,
            colSpan: 1,
            key: 'truck'
        },

    },
    selectors: {
        dealerName: 'legalName',
        outletId: 'outletId',
        companyId: 'companyId',
        brandCode: 'brandCode',
        address: 'road',
        place: 'place',
        postCode: 'postCode',
        subscriptionStatus: {
            truck: 'subscriptionStatus.truck',
        }
    }
} 