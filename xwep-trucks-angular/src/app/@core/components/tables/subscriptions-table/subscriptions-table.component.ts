import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SubscriptionData } from '@core/interfaces';
import { TableConfig } from './table-config';
import { get } from '@core/utils';


@Component({
  selector: 'app-subscriptions-table',
  templateUrl: './subscriptions-table.component.html',
  styleUrls: ['./subscriptions-table.component.scss']
})
export class SubscriptionsTableComponent implements OnInit {

  constructor() { }

  displayedColumns: string[][] = TableConfig.columns;
  columnConfig: any = TableConfig.columnConfig;
  columnKeys: any = TableConfig.keys;
  cellSelectors: any = TableConfig.selectors;

  getValueIn: any = get;

  @Input() data: SubscriptionData[];
  @Output() rowClick = new EventEmitter<any>();


  ngOnInit(): void {

  }

  handleRowClick(item) {
    this.rowClick.emit(item);
  }
}
