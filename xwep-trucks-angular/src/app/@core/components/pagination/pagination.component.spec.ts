import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationComponent } from './pagination.component';
import { RoleService } from '@core/services';
import { of, Subject } from 'rxjs';

class RoleStub {
  _role = "test";
  public countryEmmitter = new Subject();

  getUserRole() { return this._role; }
  getUserData() { return { firstName: 'test' } }
  getCurrentCountry() { return 'IND' }
  getCountryList() { return [] }
  updateCurrentCountry(data) {}

}

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginationComponent ],
      providers: [
        { provide: RoleService, useClass: RoleStub }
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    component.pageLength = 10;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check goToPrevPage is working correclty', () => {
    component.goToNextPage();
    expect(component.activePageNumber).toBe(2);
  });

  it('check goToPrevPage is working correclty', () => {
    component.goToNextPage();
    component.goToPrevPage();
    expect(component.activePageNumber).toBe(1);
  });

  it('check goToPage is working correclty', () => {
    component.goToPage(3);
    expect(component.activePageNumber).toBe(3);
  });

  it('check createPaginationArray is working correclty if active page number goes below 1', () => {
    component.activePageNumber = 0;
    component.createPaginationArray();
    expect(component.activePageNumber).toBe(1);
  });

  it('check if end page is updated correclty if totalPages <= maxPages', () => {
    component.totalPages = 4;
    component.createPaginationArray();
    expect(component.paginationArray.length).toBe(4);
  });

});
