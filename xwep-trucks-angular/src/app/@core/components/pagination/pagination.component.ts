import { RoleService } from '@core/services';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  constructor(
    private RoleService: RoleService
  ) {}

  paginationArray = [];
  totalPages = 0;

  @Input()
  set pageLength(val: number) {
    this.totalPages = val;
    this.createPaginationArray();
  }
  @Output() paginationChange = new EventEmitter<any>();

  @Input() activePageNumber: number = 1;

  ngOnInit(): void {
    this.RoleService.countryEmmitter.subscribe(() => {
      this.updatePageNumber(1);
    })
  }

  createPaginationArray() {
    if (this.activePageNumber < 1) {
      this.activePageNumber = 1;
    } else if (this.activePageNumber > this.totalPages) {
      this.activePageNumber = this.totalPages;
    }
    let maxPages: number = 5;
    let startPage: number, endPage: number;
    if (this.totalPages <= maxPages) {
      // total pages less than max so show all pages
      startPage = 1;
      endPage = this.totalPages;
    } else {
      // total pages more than max so calculate start and end pages
      let maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
      let maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
      if (this.activePageNumber <= maxPagesBeforeCurrentPage) {
        // current page near the start
        startPage = 1;
        endPage = maxPages;
      } else if (
        this.activePageNumber + maxPagesAfterCurrentPage >=
        this.totalPages
      ) {
        // current page near the end
        startPage = this.totalPages - maxPages + 1;
        endPage = this.totalPages;
      } else {
        // current page somewhere in the middle
        startPage = this.activePageNumber - maxPagesBeforeCurrentPage;
        endPage = this.activePageNumber + maxPagesAfterCurrentPage;
      }
    }
    // create an array of pages to ng-repeat in the pager control
    this.paginationArray = Array.from(
      Array(endPage + 1 - startPage).keys()
    ).map((i) => startPage + i);

  }

  goToPrevPage() {
    if (this.activePageNumber > 1) {
      const prevPageNumber = this.activePageNumber - 1;
      this.updatePageNumber(prevPageNumber);
    }
    this.createPaginationArray();
  }

  goToNextPage() {
    const nextpageNumber = this.activePageNumber + 1;
    if (nextpageNumber <= this.totalPages) this.updatePageNumber(nextpageNumber);
    this.createPaginationArray();
  }

  goToPage(index) {
    this.updatePageNumber(index);
    this.createPaginationArray();
  }

  updatePageNumber(newPageNumber) {
    this.activePageNumber = newPageNumber;
    this.paginationChange.emit({ pageNumber: newPageNumber });
    // console.log(newPageNumber);
  }
}
