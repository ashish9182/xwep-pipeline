import {
  Component,
  OnInit,
  AfterContentInit,
  ContentChildren,
  ViewChild,
  QueryList,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

import { MatTable, MatColumnDef } from '@angular/material/table';
import { flatten, filter, chunk } from '@core/utils';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss'],
})
export class DatatableComponent<T> implements OnInit, AfterContentInit {
  @Input() columns: string[][];
  @Input() columnConfig: string[];
  @ViewChild('paginationRef') pagination;
  @Output() rowClick = new EventEmitter();
  @Input() showLegends: boolean;
  private _data: T[][];
  get data(): any {
    return this._data;
  }
  @Input()
  set data(val: any) {
    this._data = chunk(val, 10);
    this.maxPageNumber = this._data.length;
    if (this.currentPageNumber <= this.maxPageNumber) {
      this.dataToRender = this._data[this.currentPageNumber - 1];
    } else {
      this.dataToRender = this._data[0];
      this.currentPageNumber = 1;
    }

    // if (this.pagination)
    //   this.pagination.updatePageNumber(this.currentPageNumber);
  }

  dataToRender: T[];
  maxPageNumber: number = 0;

  @Input() dataLoading: boolean;

  bodyColumns: string[];
  hasMultiLineHeader: boolean;

  currentPageNumber: number = 1;

  constructor() {}

  @ViewChild(MatTable, { static: true }) table: MatTable<T>;
  @ContentChildren(MatColumnDef) columnDefs: QueryList<MatColumnDef>;

  ngAfterContentInit() {
    this.hasMultiLineHeader = this.columns.length > 1;
    this.columnDefs.forEach((columnDef) => this.table.addColumnDef(columnDef));
  }

  ngOnInit(): void {
    this.bodyColumns = filter(
      flatten(this.columns),
      (el) => this.columnConfig[el].colSpan === 1
    );
  }

  handlePaginationChange(paginationObj) {
    this.currentPageNumber = paginationObj.pageNumber;
    this.dataToRender = this.data[this.currentPageNumber - 1];
  }

  handleRowClick(row) {
    this.rowClick.emit(row);
  }
}
