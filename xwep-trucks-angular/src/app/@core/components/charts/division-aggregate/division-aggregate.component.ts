import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-division-aggregate',
  templateUrl: './division-aggregate.component.html',
  styleUrls: ['./division-aggregate.component.scss']
})
export class DivisionAggregateComponent implements OnInit {

  @Input() overviewData: any;

  @Output() showTooltip = new EventEmitter<any>();

  chartActiveEl: any;
  constructor() { }

  ngOnInit(): void {
    
  }

  handleActivationChange(el) {
    this.chartActiveEl = el;
  }

}
