import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionAggregateComponent } from './division-aggregate.component';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';

describe('DivisionAggregateComponent', () => {
  let component: DivisionAggregateComponent;
  let fixture: ComponentFixture<DivisionAggregateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionAggregateComponent ],
      imports: [
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        })
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionAggregateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the chart toop tip value once the activation changes are called', () => {
    component.handleActivationChange('Test');
    expect(component.chartActiveEl).toBe('Test');
  });
  
});
