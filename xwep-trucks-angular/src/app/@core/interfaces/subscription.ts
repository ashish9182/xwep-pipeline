export interface SubscriptionData {
    id: number,
    uuid: string,
    brandCode: string,
    legalName: string,
    companyId: string,
    email: string,
    emailLKW: string,
    nationality: string,
    outletId: string,
    place: string,
    postCode: string,
    road: string,
    subscriptionStatus: SubscriptionStatus[];
    status : string
}

export interface SubscriptionStatus {
    id: number,
    equipmentType: string,
    divisionName: string,
    subscribed: boolean
}

export interface CreateSubscription {
    uuid : string,
    brandCode: string,
    companyId: string,
    email: string,
    emailLKW: string,
    emailPassengerCars: string,
    emailSmartPassengerCars: string,
    emailTransporter: string,
    id: number,
    legalName: string,
    nationality: string,
    outletId: string,
    place: string,
    postCode: string,
    road: string,
    subscriptionStatus: Array<SubscriptionStatus>
}

export interface SubscriptionsInfo {
    mpc: string,
    dealerCount: number,
    countryName: string,
    subscriptionCount: {
        VAN: number,
        TRUCK: number,
        EQ: number,
        PASSENGER: number
    }
}

export interface SubscriptionsInfoNew {
    mpc: string,
    dealerCount: number,
    countryName: string,
    subscriptionCount: number
}

export interface SubscriptionsAggregate {
    division: string,
    total: number,
    fullyEquipped: number,
    partiallyEquipped: number,
    fullyEquippedWithoutSubscription: number,
    partiallyEquippedWithoutSubscription: number
}

export interface SubscriptionsAggregateOverview {
    dealerCount: number,
    divisionAggregateList: SubscriptionsAggregate[]
}

export interface SubscriptionsAggregateNew {
    total: number,
    fullyEquipped: number,
    partiallyEquipped: number,
    fullyEquippedWithoutSubscription: number,
    partiallyEquippedWithoutSubscription: number
}
