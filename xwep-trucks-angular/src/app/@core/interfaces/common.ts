export interface Response {
    data: any,
    error: any,
    errorCode: string,
    responseCode: 0,
    responseStatus: string

}