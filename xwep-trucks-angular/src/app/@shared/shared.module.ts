import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

/**
 * Angular Material
 */
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { ExpansionPanelComponent } from './components/expansion-panel/expansion-panel.component';
import {MatTooltipModule} from '@angular/material/tooltip';
/**
 * NGX Chart
 */
import { PieChartModule } from '@swimlane/ngx-charts';
import { DonutChartComponent } from './components/charts/donut-chart/donut-chart.component';
import { ExpandableDatatableComponent } from './components/expandable-datatable/expandable-datatable.component';


import { MdePopoverModule } from '@material-extended/mde';
import { UserDetailsNotFoundComponent } from './components/user-details-not-found/user-details-not-found.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';

@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    PieChartModule,
    MatTableModule,
    MdePopoverModule,
    MatTooltipModule
  ],
  declarations: [
    ExpansionPanelComponent,
    DonutChartComponent,
    ExpandableDatatableComponent,
    UserDetailsNotFoundComponent,
    ErrorPageComponent
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatTableModule,
    MatDialogModule,
    MatRadioModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    // Components
    ExpandableDatatableComponent,
    ExpansionPanelComponent,
    DonutChartComponent,
    MdePopoverModule,
    UserDetailsNotFoundComponent,
    ErrorPageComponent,
    MatTooltipModule
  ]
})
export class SharedModule { }
