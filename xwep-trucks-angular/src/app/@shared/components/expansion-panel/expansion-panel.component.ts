import { Component, OnInit, ContentChild, TemplateRef, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss']
})
export class ExpansionPanelComponent implements OnInit {

  @ContentChild('expansionHeader', { static: false }) expansionHeader: TemplateRef<ElementRef>;
  @ContentChild('expansionContent', { static: false }) expansionContent: TemplateRef<ElementRef>;

  @Input() fullWidth: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
