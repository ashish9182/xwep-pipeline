import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonutChartComponent } from './donut-chart.component';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslationService } from '@core/services/translation.service';
import { of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA, ElementRef, NO_ERRORS_SCHEMA, Renderer2, Type } from '@angular/core';

describe('DonutChartComponent', () => {
  let component: DonutChartComponent;
  let fixture: ComponentFixture<DonutChartComponent>;
  let renderer2: Renderer2;

  class TranslationServiceStub {
    getTranslationObservable() { return of() }
    instant() { return of() }
  }

  class RendererStub {
    setAttribute(){}
    createElement(){}
    appendChild(){}
    listen(){}
    addClass(){}
  }

  beforeEach(async () => {
    
    await TestBed.configureTestingModule({
      declarations: [DonutChartComponent],
      providers: [
        {
          provide: TranslationService, useClass: TranslationServiceStub
        },
        Renderer2,
        // { provide: Renderer2, useClass: RendererStub },
        { provide: ElementRef, useValue: ElementRef }
      ],
      imports: [
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
        }),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonutChartComponent);
    component = fixture.componentInstance;
    component.skipForTesting = true;
    component.data = [{ name: "subscriptions.equipmentType.FULL_EQUIPMENT", value: 11 },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT", value: 26 },
    { name: "subscriptions.equipmentType.FULL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: 20 },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: 20 }];
    component.colors = [{ name: "subscriptions.equipmentType.FULL_EQUIPMENT", value: "#6ea046" },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT", value: "#c8ddb4" },
    { name: "subscriptions.equipmentType.FULL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: "#e5923a" },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: "#fae6d2" }];
    component.total = 0;
    renderer2 = fixture.componentRef.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnChanges();
    expect(component).toBeTruthy();
  });

  it('should set default if total is 0', () => {
    component.data = [{ name: "subscriptions.equipmentType.FULL_EQUIPMENT", value: 0 },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT", value: 0 },
    { name: "subscriptions.equipmentType.FULL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: 0 },
    { name: "subscriptions.equipmentType.PARTIAL_EQUIPMENT_WITHOUT_SUBSCRIPTION", value: 0 }];
    component.initChart();
    expect(component.renderData[0].name).toBe('no_data');
    expect(component.showLegends ).toBe(false);

  });


  it('should handleElActivation be called // Currently no functionality performed', () => {
    // spyOn(component, 'handleElActivation');
    component.handleElActivation({});
    expect(component).toBeTruthy();
   

  });


  it('should handleElDeactivation be called // Currently no functionality performed', () => {
    // spyOn(component, 'handleElDeactivation');
    component.handleElDeactivation({});
    expect(component).toBeTruthy();
   

  });

  it('should handleArcHover be called // Currently no functionality performed', () => {
    // spyOn(component, 'handleArcHover');
    component.handleArcHover({});
    expect(component).toBeTruthy();
  });

});
