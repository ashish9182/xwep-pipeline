import {
  Component,
  Input,
  OnInit,
  OnChanges,
  ContentChild,
  TemplateRef,
  ElementRef,
  Output,
  EventEmitter,
  AfterViewInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { TranslationService } from '@core/services';

@Component({
  selector: 'app-donut-chart',
  templateUrl: './donut-chart.component.html',
  styleUrls: ['./donut-chart.component.scss']
})
export class DonutChartComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() data: any[];
  @Input() colors: any;
  @Input() title: string;
  @ContentChild('tooltipRef', { static: false }) tooltipRef: TemplateRef<ElementRef>;
  @Output() activationchange = new EventEmitter<any>()
  @ViewChild('container') container;


  total: number;
  skipForTesting = false;
  renderData: any;
  renderColorData: any;
  showLegends: boolean = true;

  constructor(
    private _el: ElementRef,
    private _renderer: Renderer2,
    private _translationService: TranslationService
  ) { }

  ngOnInit(): void {
    this.initChart();
    this.watchLangChange();
  }
  ngOnChanges() {
    this.initChart();
  }

  initChart() {
    const total = this.updateTotal();
    if (total) {
      this.processTranslation();
      this.showLegends = true;
    }
    else {
      this.renderData = [{
        name: 'no_data',
        value: 1
      }]
      this.renderColorData = [{
        name: 'no_data',
        value: '#c8c7c8'
      }];
      this.showLegends = false;
    }
  }

  watchLangChange() {
    this._translationService.getTranslationObservable().subscribe(lang => {
      this.processTranslation();
    })
  }

  processTranslation() {
    if (this.total) {
      this.renderData = this.data.map(el => ({ ...el, name: this._translationService.instant(el.name) }));
      this.renderColorData = this.colors.map(el => ({ ...el, name: this._translationService.instant(el.name) }));
    }
  }
  ngAfterViewInit() {

    const parent = this.container.nativeElement.querySelector('svg > g > g');
    const textEl = this._renderer.createElement('text', 'http://www.w3.org/2000/svg');
    this._renderer.setAttribute(textEl, 'text-anchor', 'middle');
    this._renderer.addClass(textEl, 'chart-total');
    this._renderer.setAttribute(textEl, 'dy', '5px');
    textEl.innerHTML = this.total;
    if(parent){
      this._renderer?.appendChild(parent, textEl);
      this._renderer?.listen(this.container.nativeElement.querySelector('svg .arc'), 'mouseover', this.handleArcHover)
    }
  }

  updateTotal() {
    this.total = this.data.reduce((acc, el) => acc + el.value, 0);
    const element = this._el.nativeElement.querySelector('.chart-total');
    if (element)
      this._renderer.setProperty(element, 'innerHTML', this.total);
    return this.total;
  }

  handleElActivation(el) {
    this.activationchange.emit({
      ...el.value,
      total: this.total
    })
  }

  handleElDeactivation(el) {
    // this.activationchange.emit(null)
  }

  handleArcHover(e) {
    // debugger;
    // console.log(e)
  }

}
