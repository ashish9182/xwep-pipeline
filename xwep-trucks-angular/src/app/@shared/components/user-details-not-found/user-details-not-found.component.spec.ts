import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsNotFoundComponent } from './user-details-not-found.component';

describe('UserDetailsNotFoundComponent', () => {
  let component: UserDetailsNotFoundComponent;
  let fixture: ComponentFixture<UserDetailsNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDetailsNotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
