import { Component, Input, OnInit } from '@angular/core';
import { API_PATHS } from '@core/constants';
@Component({
  selector: 'app-user-details-not-found',
  templateUrl: './user-details-not-found.component.html',
  styleUrls: ['./user-details-not-found.component.scss']
})
export class UserDetailsNotFoundComponent implements OnInit {

  @Input() errorMessage;
  
  constructor() { }

  ngOnInit(): void {
  }

  handleLogout() {
    window.location.replace(API_PATHS.logoutUrl)
  }
  

}
